<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'/third_party/icepay/icepay.php');

class payment_library
{
	private $icepay;
	
	function __construct()
	{
		$ci =& get_instance();
		$ci->config->load('icepay');
		$this->icepay = new ICEPAY( $ci->config->item('icepay_merchant'), $ci->config->item('icepay_secret') );
	}
	
	/*
	 * note that price is in cents
	 */
	function get_payment_url($payment_method, $performance_id, $user_id, $price)
	{
		$url = '';
		$reference = array('performance_id' => $performance_id, 'user_id' => $user_id);
		$this->icepay->SetReference(json_encode($reference));
		$url = $this->icepay->Pay('NL', 'NL', 'EUR', $price, 'on-stage.tv voorstelling' );
		
		return $url;
	}
	
	function get_postback_performance()
	{
		$performance_id = 0;
		if ( $this->icepay->OnPostback() )
		{
			$data = $this->icepay->GetPostback();
			$reference = json_decode($data->reference, TRUE);
			$performance_id = $reference['performance_id'];
		}
		return $performance_id;
	}

	function get_postback_user()
	{
		$user_id = 0;
		if ( $this->icepay->OnPostback() )
		{
			$data = $this->icepay->GetPostback();
			$reference = json_decode($data->reference, TRUE);
			$user_id = $reference['user_id'];
		}
		return $user_id;
	}

	function get_postback_status()
	{
		$status = 'error';
		
		if ( $this->icepay->OnPostback() )
		{
			$data = $this->icepay->GetPostback();
			switch ( strtoupper($data->status) )
			{
				case "OK": // Successful payment
					$status = 'paid';
					break;
				case "OPEN": // Payment is not yet completed
					$status = 'start';
					break;
				case "ERR": // Error happened
				case "REFUND": // Merchant did a refund
				case "CBACK": // Charge back by end-user
				default:
					$status = 'failed';
				}
		}
		
		return $status;
	}
	
	function get_success_performance()
	{
		$performance_id = 0;
		
		if ( $this->icepay->OnSuccess() )
		{
			$data = $this->icepay->GetData();
			if ( $data->status == "OK" )
			{
				$reference = json_decode($data->reference, TRUE);
				$performance_id = $reference['performance_id'];
			}
		}
		return $performance_id;
	}

	function get_failure_performance()
	{
		$performance_id = 0;
		
		if ( $this->icepay->OnError() )
		{
			$data = $this->icepay->GetData();
			$reference = json_decode($data->reference, TRUE);
			$performance_id = $reference['performance_id'];
		}
		return $performance_id;
	}
}