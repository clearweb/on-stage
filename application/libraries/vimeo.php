<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH.'/third_party/vimeo/vimeo.php');

class Vimeo
{
	private $vimeo = NULL;
	
	private function set_correct_preset($video_id = 0) {
		$ci =& get_instance();
		$preset_id = $ci->config->item('vimeo_preset_id', 0);

		if ( ! empty($video_id) && ! empty($preset_id)) {
			$response = $this->vimeo->call('vimeo.videos.embed.setPreset', array('preset_id' => $preset_id, 'video_id' => $video_id));
		}
	}

	
	
	public function __construct()
	{
		
		$consumer_key              = '71d3e8490d6c14b33b2b551b1da9825e4ee8446e';
		$consumer_secret           = 'b0019f6b012b816816903174972bf309c7cdc598';
		$oauth_access_token        = '623b15ed915a24946cf05c7cd58aa15a';
		$oauth_access_token_secret = '95f75cdc0e4b845d479e28e23fe04655af2c88d2';
		
		$this->vimeo = new phpVimeo($consumer_key, $consumer_secret);
		$this->vimeo->enableCache(phpVimeo::CACHE_FILE, './vimeo_cache', 300);
		
		$this->vimeo->setToken($oauth_access_token, $oauth_access_token_secret);
	}
	
	public function upload_video($filepath, $old_video_id = null)
	{
		error_reporting(E_ALL);
		ini_set('display_errors', '1');
		$video_id = $this->vimeo->upload($filepath, true, './vimeo_temp', 2097152, $old_video_id);
		$this->set_correct_preset($video_id);
		return $video_id;
	}
	
	public function get_videos()
	{
		// Do an authenticated call
        try {
            $videos = $this->vimeo->call('vimeo.videos.getUploaded', array('full_response'=>TRUE));
			return $videos;
        }
        catch (VimeoAPIException $e) {
            echo "Encountered an API error -- code {$e->getCode()} - {$e->getMessage()}";
			return array();
        }
	}
	
	public function is_ready($id)
	{
		$ready = FALSE;
		$video_info = $this->get_video_info($id);
		
		if ( ! empty ($video_info->video[0]) && isset ($video_info->video[0]->is_transcoding) && $video_info->video[0]->is_transcoding == 0)
		{
			$ready = TRUE;
		}
		
		return $ready;
	}
	
	public function get_thumbnail_url($video_id)
	{
		$thumbnail_url = null;
		
		$thumbnail_urls_object = $this->vimeo->call('vimeo.videos.getThumbnailUrls', array('video_id' => $video_id));
		$thumbnails_array = $thumbnail_urls_object->thumbnails->thumbnail;
		
		$thumbnail_url = $thumbnails_array[2]->_content;
		
		return $thumbnail_url;
	}
	
	public function get_video_info($id)
	{
		return $this->vimeo->call('vimeo.videos.getInfo', array('video_id' => $id));
	}
	
	
	/**
	 * Not used yet! this is for getting the token for first time!
	 */
	public function request_token($consumer_key, $consumer_secret)
	{

		$request_token = $vimeo->getRequestToken();
		return $request_token;
	}
	
	public function get_video_html($video_id, $width=679, $height=380) {
		/*
		$ci =& get_instance();
		$ci->load->helper('layout');
		
		layout_add_js("http://vjs.zencdn.net/c/video.js");
		layout_add_css("http://vjs.zencdn.net/c/video-js.css");
		
		$html = '<video id="example_video_1" class="video-js vjs-default-skin" controls preload="none" width="'.$width.'" height="'.$height.'" poster="'.$thumbnail_url.'" data-setup="{}">'.PHP_EOL;

		$html .= '<source src="'.$video_url.'" type="video/mp4" />'.PHP_EOL;
		$html .= '</video>'.PHP_EOL;
		return $html;
		*/

		//		$src = 'src="http://player.vimeo.com/video/'.$video_id.'?title=0&amp;byline=0&amp;portrait=0&color=F47660"';
		$src = 'src="https://player.vimeo.com/video/'.$video_id.'?title=0&amp;byline=1&amp;portrait=1&color=F47660"';
		$html = '<iframe '.$src.' width="'.$width.'" height="'.$height.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
		return $html;
	}
	
	public function _curl_page($url, $post = '', $cookie_val = '') {
		$header [] = "Accept:text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
		$header [] = "Accept-Charset:ISO-8859-1,utf-8;q=0.7,*;q=0.3";
		$header [] = "Accept-Encoding:gzip,deflate";
		$header [] = "Accept-Language:en-US,en;q=0.5";
		
		$cookie= realpath("./vimeo_temp/cookie.txt");
		
		/*
		echo 'cookie content: "';
		readfile($cookie);
		echo '"';

		if (!file_exists($cookie)) 
		{
			$ourFileHandle = fopen($cookie, 'w') or die("can't open file");
			fclose($ourFileHandle);
		}
		*/
		
		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, $url);
		//curl_setopt ($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_UNRESTRICTED_AUTH, TRUE);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, TRUE); 
		
		
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		
		curl_setopt($ch, CURLOPT_HEADER, TRUE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		
		curl_setopt($ch, CURLOPT_ENCODING, "gzip");
		
		curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6"); 
		curl_setopt ($ch, CURLOPT_TIMEOUT, 60); 
		curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, TRUE); 
		//curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, FALSE); 
		
		curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
		
		curl_setopt( $ch, CURLOPT_COOKIESESSION, TRUE);
		curl_setopt( $ch, CURLOPT_COOKIEJAR, $cookie );
		
		if ( ! empty($cookie_val)) {
			curl_setopt($ch, CURLOPT_COOKIE, $cookie_val);
			echo 'setting cookie: '.$cookie_val;
		}
		else
		{
			curl_setopt( $ch, CURLOPT_COOKIEFILE, $cookie );
		}
		
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, TRUE
);
		
		curl_setopt ($ch, CURLOPT_REFERER, $url); 

		if ( ! empty($post)) {
			curl_setopt ($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt ($ch, CURLOPT_POST, TRUE);
		}
		
		$result = curl_exec ($ch);

		curl_close($ch);
		return $result;
	}
	
	private function _visit_login_form() {
		return $this->_curl_page("http://vimeo.com/log_in");
	}

	private function _login($token) {
		$username="barendvanbalen@hotmail.com";
		$password="onstageTV1234";
		
		$postdata = "email=".$username."&password=".$password."&action=login&service=vimeo&token=".$token;
		
		return $this->_curl_page("http://vimeo.com/log_in", $postdata, 'token='.$token.';');
	}
	
	public function get_video_urls() {
		$login_form = $this->_visit_login_form();
		//echo $login_form;
		$token = preg_match("#xsrft: '([a-z0-9]{32})'#", $login_form, $matches);
		echo $this->_login($matches[1]);
		/*


		*/
		//return $result;
	}

}