<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Grid {
	var $title = '';
	var $filter_headers = array();
	var $header_names = array();
	var $use_pagination = FALSE;
	var $operations = array();
	var $controls = array();
	var $rows = array();
	var $sort_action = '';
	
	/**
	 * sets the title of the grid
	 */
	public function set_title($title = '') {
		$this->title = $title;
	}

	/**
	 * Set which headers will be filtered out.
	 */
    public function filter_headers(array $filter_headers = array())
    {
		$this->filter_headers = $filter_headers;
    }

	/**
	 * sets a 'translation' array for the headers
	 */
	public function set_header_names(array $header_names = array()) {
		$this->header_names = $header_names;
	}

	/**
	 * Toggles pagination on or off
	 */
	public function set_pagination($use_pagination = TRUE)
	{
		$this->use_pagination = $use_pagination;
	}

	/**
	 * Turns on sortability and adds an action to call when user drops row.
	 */
	public function set_sortable($action = '')
	{
		$this->sort_action = $action;
	}
	
	/**
	 * Add an operation button to the top of the grid.
	 */
	public function add_operation($action = 'add', $text = 'Toevoegen', array $classess = array()) {
		$this->operations[] = array(
									'text' => $text,
									'action' => $action,
									'classess' => $classess
									);
	}

	/**
	 * Adds a control button. These are buttons which are added for each row in the grid.
	 */
	public function add_control_button($action = 'edit/{id}', $text = 'Bewerken', array $classess = array())
	{
		$this->controls[] =  array(
								   'text' => $text,
								   'action' => $action,
								   'classess' => $classess
								   );
	}
	
	/**
	 * Adds data to grid. Make sure it is assosiative array and that it has same keys as data already inside.
	 * @TODO add check here to ensure it has same keys as the rows already inside.
	 */
	public function add_rows(array $rows = array())
	{
		$this->rows = array_merge($this->rows, $rows);
	}

	/**
	 * Adds data to grid. Make sure it is assosiative array and that it has same keys as data already inside.
	 * @TODO add check here to ensure it has same keys as the rows already inside.
	 */
	public function add_row(array $row = array())
	{
		$this->rows[] = $row;
	}
	
	public function clear()
	{
		$this->rows = array();
	}

	private function _replace_vars($string = '', $row_data = array())
	{
		
		foreach($row_data as $key => $value)
		{
			$string = preg_replace('#{'.$key.'}#', $value, $string);
		}
		
		return $string;
	}

	/**
	 * directly echos the grid.
	 */
	public function render(array $classes = array())
	{
		echo '<div class="grid-container">'.PHP_EOL;
		if ($this->title != '')
		{
			echo '<h1>'.$this->title.'</h1>'.PHP_EOL;
		}
		
		if (count($this->operations) > 0)
		{
			echo '<div class="operations">'.PHP_EOL;
			foreach ($this->operations as $operation)
			{
				echo "<a class='operation ".implode(' ', $operation['classess'])."' href='{$operation['action']}'>{$operation['text']}</a>".PHP_EOL;
			}
			echo '</div>'.PHP_EOL;
		}
		
		echo '<table class="grid ';
		implode(' ', $classes);
		echo '" >'.PHP_EOL;


		echo '<thead>'.PHP_EOL;
		echo '<tr>'.PHP_EOL;
		
		if (count($this->rows) > 0)
		{
			$first_row = reset($this->rows);

			foreach($first_row as $column_name => $value) {
				if ( ! in_array($column_name, $this->filter_headers))
					{
						echo '<th>';
						echo (isset($this->header_names[$column_name]))?$this->header_names[$column_name]:$column_name;
						echo '</th>';
					}
			}

		
			if (count($this->controls)) {
				echo '<th></th>';
			}
			
		}

		
		echo '</tr>'.PHP_EOL;
		echo '</thead>'.PHP_EOL;
		
		echo '<tbody>'.PHP_EOL;		
		foreach($this->rows as $row)
		{
			echo '<tr>'.PHP_EOL;
			foreach($row as $column_name => $value)
			{ 
			if ( ! in_array($column_name, $this->filter_headers))
				{
					echo "<td>$value</td>".PHP_EOL;
				}

			}
			
			if (count($this->controls) || ! empty($this->sort_action)) {
				echo '<td class="control-button-cell">';
				foreach($this->controls as $control_button) {
					$action = $this->_replace_vars($control_button['action'], $row);
					echo '<a href="'.$action.'" class="control-button ';
					echo implode(' ', $control_button['classess']);
					echo '">';
					echo $control_button['text'];
					echo '</a>';
				}
				if ( ! empty($this->sort_action))
				{
					echo '<a href="'.$this->sort_action.'/'.$row['id'].'/-1" class="control-button sort-button up" >up</a>';
					echo '<a href="'.$this->sort_action.'/'.$row['id'].'/1" class="control-button sort-button down" >down</a>';
				}
				echo '</td>';
			}
			
			echo '</tr>'.PHP_EOL;
		}
		echo '</tbody>'.PHP_EOL;
		echo '</table>'.PHP_EOL;
		echo '</div>'.PHP_EOL;
	}
}

/* End of file Grid.php */