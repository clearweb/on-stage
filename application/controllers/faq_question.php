<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq_question extends CI_Controller
{
	/**
	 * management for faq questions for a faq id.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/faq_question/overview/1
	 *
	 */
	public function overview($faq_id) {
		$this->load->helper('admin_user');
		admin_user_require();
		
		$this->load->model('Faq_model');
		if ($this->Faq_model->faq_id_exists($faq_id)) {
			$this->load->library('parser');
			$this->load->model('Faq_question_model');
			$faq_questions = $this->Faq_question_model->get_faq_questions($faq_id);
			$data = array(
						  'faq_questions' => $faq_questions,
						  'faq_id' => $faq_id
						  );
			$this->parser->parse('administration/faq/questions_overview', $data);
		} else {
			show_404();
		}
	}
	
	private function _load_form_validation()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'titel', 'required');
	}
	
	private function _process_and_show_form()
	{
		if ($this->form_validation->run())
		{
			$this->load->helper('url');
			
			$this->Faq_question_model->title = $this->input->post('title');
			$this->Faq_question_model->category = $this->input->post('category');
			$this->Faq_question_model->description = $this->input->post('description');
			
			$this->Faq_question_model->write_through();
			
			redirect('/faq_question/overview/'.$this->Faq_question_model->faq_id);
		}
		else
		{
			if ($this->input->post('title') !== FALSE)
			{
				$this->Faq_question_model->title = $this->input->post('title');
				$this->Faq_question_model->description = $this->input->post('description');
			}
			
			$this->load->view('administration/faq/question_edit');
		}
	}

	public function edit($faq_question_id) {
		$this->load->helper('admin_user');
		admin_user_require();
		
		$this->load->library('parser');
		$this->load->model('Faq_question_model');
		
		if ($this->Faq_question_model->id_exists($faq_question_id)) {
			$this->Faq_question_model->load($faq_question_id);
			$this->_load_form_validation();
			
			$this->_process_and_show_form();
			
		} else {
			show_404();
		}
	}
	
	public function sort($faq_question_id = 0, $rank_change = 0)
	{
		$this->load->helper('admin_user');
		$this->load->helper('url');
		admin_user_require();
		
		$this->load->model('Faq_question_model');
		
		if ($this->Faq_question_model->id_exists($faq_question_id))
		{
			$this->Faq_question_model->load($faq_question_id);
			$this->Faq_question_model->move($rank_change);
			redirect('/faq_question/overview/'.$this->Faq_question_model->faq_id);
		} else {
			show_404();
		}
	}
	
	public function delete($faq_question_id) {
		$this->load->helper('admin_user');
		admin_user_require();
		
		$this->load->model('Faq_question_model');
		
		if ($this->Faq_question_model->id_exists($faq_question_id)) {
			$this->load->helper('url');
			$this->Faq_question_model->load($faq_question_id);
			
			$this->Faq_question_model->delete();
			redirect('/faq_question/overview/'.$this->Faq_question_model->faq_id);
		} else {
			show_404();
		}
	}

	public function add($faq_id) {
		$this->load->helper('admin_user');
		admin_user_require();
		
		$this->load->model('Faq_question_model');

		$this->Faq_question_model->faq_id = $faq_id;
		
		$this->_load_form_validation();
		
		$this->_process_and_show_form();
	}
	
}

/* End of file faq_question.php */
/* Location: ./application/controllers/faq_question.php */