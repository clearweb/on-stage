<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Crud_controller extends ON_Controller
{
	var $crud_model = '';
	var $edit_view  = '';
	var $overview_url = '';
	
	protected function _load_form_validation()
	{
		$this->load->library('form_validation');
	}
	
	protected function _populate_model_from_input() {}
	
	protected function _after_edit_redirect()
	{
		$this->load->helper('url');
		redirect($this->overview_url);
	}

	protected function _after_delete_redirect()
	{
		$this->load->helper('url');
		redirect($this->overview_url);
	}

	protected function _process_form() {
		$this->_populate_model_from_input();
		$this->{$this->crud_model}->save_to_db();
	}
	
	protected function _show_form()
	{
		$this->load->view($this->edit_view);
	}
	
	protected function _process_and_show_form()
	{
		if ($this->form_validation->run())
		{
			$this->_process_form();
			$this->_after_edit_redirect();
		}
		else
		{
			if ($this->input->post('submit') !== FALSE)
			{
				$this->_populate_model_from_input();
			}

			$this->_show_form();
		}
	}

	protected function _load_model($id) {
		$this->{$this->crud_model}->load($id);
	}
	
	public function edit($id)
	{
		$this->_require_write_rights();
		
		$this->load->model($this->crud_model);
		if ($this->{$this->crud_model}->exists($id))
		{
			$this->_load_model($id);
			$this->_load_form_validation();
			$this->_process_and_show_form();
		}
		else
		{
			show_404();
		}
	}
	
	protected function _require_write_rights() {
		$this->load->helper('admin_user');
		admin_user_require();
	}
	
	public function add() {
		$this->_require_write_rights();
		
		$this->load->model($this->crud_model);
		
		$this->_load_form_validation();
		
		$this->_process_and_show_form();
	}
	
	public function delete($id) {
		$this->load->helper('admin_user');
		admin_user_require();
		
		$this->load->model($this->crud_model);
		
		if ($this->{$this->crud_model}->exists($id)) {
			$this->_load_model($id);
			$this->{$this->crud_model}->delete();
			$this->_after_delete_redirect();
		} else {
			show_404();
		}
	}
}


/* End of file Crud_controller.php */
/* Location: ./application/controllers/Crud_controller.php */