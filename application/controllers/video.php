<?php

class Video extends Crud_controller
{
	var $crud_model    = 'Video_model';
	var $edit_view     = 'administration/video/edit';
	var $overview_url  = '/video/overview';
	
	function overview()
	{
		$this->load->helper('admin_user');
		admin_user_require();
		
		$this->load->model('Video_model');
		$this->load->model('Performance_model');
		
		$videos = $this->Video_model->get_all();
		
		foreach($videos as $key => $video)
		{
			if ( ! empty($videos[$key]['performance']) && $this->Performance_model->exists($videos[$key]['performance'])) {
				$this->Performance_model->load($videos[$key]['performance']);
				$videos[$key]['performance_name'] = $this->Performance_model->getProperty('title');
			} else {
				$videos[$key]['performance_name'] = '';
			}
		}
		
		$this->load->view('administration/video/overview', array('videos'=>$videos));
	}

	function blueimp()
	{
		$param_name = 'video_jquery_upload';
		$options = array();
		$options['param_name'] = $param_name;
		$options['accept_file_types'] = '/\.(mp4|mpeg|mov|avi|wmv)$/i';
		$this->load->library('blueimp', $options);
	}
	
	protected function _load_form_validation()
	{
		parent::_load_form_validation();
		$this->form_validation->set_rules('performance', 'Voorstelling', 'required');
		$this->form_validation->set_rules('type', 'Type', 'required|callback_has_other_paid_video_validation');
	}
	
	public function has_other_paid_video_validation($type) {
		$id = $this->input->post('id');
		
		$video = new Video_model();
		
		if ($type == 'paid') {
			$video->load_from_performance($this->input->post('performance'), $type);
			if ( $video->get_id() != $id && $video->loaded) {
				$this->form_validation->set_message('has_other_paid_video_validation', 'A paid video already exists for this performance');
				return FALSE;
			}
		}
	}
	
	protected function _show_form()
	{
		$this->load->model('Performance_model');
		
		$data = array();
		
		$this->Performance_model->set_return_only_available(false);
		
		$data['performances'] = $this->Performance_model->get_all();
		
		$this->load->view($this->edit_view, $data);
	}
	
	protected function _populate_model_from_input()
	{
		$this->Video_model->setProperty('type', $this->input->post('type'));
		$this->Video_model->setProperty('performance', $this->input->post('performance'));
	}
	
	protected function _process_form()
	{
		$this->load->library('vimeo');
		$this->load->model('Performance_model');
		
		$this->Video_model->set_video_api($this->vimeo);
		
		$new_video = $this->input->post('video');
		if ( ! empty($new_video))
		{
			$this->Video_model->change_video('files/'.$new_video);
		}
		
		parent::_process_form();
		
		if ( ! is_null($this->Video_model->getProperty('performance')) )
		{
			$this->Performance_model->load($this->Video_model->getProperty('performance'));
		}
	}
	
	function _load_model($id) {
		$this->load->library('vimeo');
		$this->Video_model->set_video_api($this->vimeo);
		parent::_load_model($id);
	}
}