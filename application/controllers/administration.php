<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administration extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/administration
	 *	- or -  
	 * 		http://example.com/index.php/administration/index
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/administration/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('Performance_model');
		$this->load->model('Performance_rental');
		$this->load->model('Performance_view');
		$this->load->model('User_model');
		$this->load->helper('admin_user');
		admin_user_require();
		
		$period_filter = FALSE;
		$total_newsletters = 0;
		
		/**
		// Get period data
		*/
		$period_from = $this->input->post('period_from');
		$period_till = $this->input->post('period_till');
		
		$date_from = '0000-00-00';
		$date_till = '0000-00-00';
		
		if( ! empty($period_from) ){
			$data['period']['from'] = $period_from;	
			
			$chunks = explode('-', $period_from);
			$date_from = $chunks[2];
			$date_from .= '-'.$chunks[1];
			$date_from .= '-'.$chunks[0];
			
			$period_filter = TRUE;
		}
			
		if( ! empty($period_till) ){
			$data['period']['till'] = $period_till;
			$chunks = explode('-', $period_till);
			$date_till = $chunks[2];
			$date_till .= '-'.$chunks[1];
			$date_till .= '-'.$chunks[0];
			$date_till .= ' 23:59:59';
			
			$period_filter = TRUE;
		}
			
	;
		
		
		/**
		// Gather data for performance statistics.
		*/
		$performances = $this->Performance_model->get_all();
		$total_performances = $this->Performance_model->get_count();
		$filter = array();
		
		foreach( $performances as $performance )
		{
			if( ! empty($period_from) ){
				$this->Performance_rental->set_restriction('payment_datetime', $date_from, '>=');
				$this->Performance_view->set_restriction('datetime', $date_from, '>=');
			}
			
			if( ! empty($period_till) ){
				$this->Performance_rental->set_restriction('payment_datetime', $date_till, '<=');
				$this->Performance_view->set_restriction('datetime', $date_till, '<=');
			}	
		
			$id = $performance['id'];
			$price = $performance['rental_price'];
			
			$this->Performance_rental->set_restriction('performance', $id);
			$rents = $this->Performance_rental->get_count();	
			
			$this->Performance_view->set_restriction('performance', $id);
			$views = $this->Performance_view->get_count();	
			
			$title = $performance['title'];
			$artist = $performance['performer'];
			
			$filter['performance'] = $id;

			if( ! empty($period_from) ){

				$filter['payment_datetime >='] = $date_from;
			}
				
			if( ! empty($period_till) ){
			
				$filter['payment_datetime <='] = $date_till;
			}
				$earnings = $this->Performance_rental->get_sum($filter);
		
			$data['performance_stats'][] = array( 
									'id' => $id,
									'title' => $title,
									'artist' => $artist,
									'rents' => $rents,
									'views' => $views,
									'price' => number_format($price, 2, ',', '.'),
									'earnings' => number_format($earnings['payment'], 2, ',', '.')
								);
		}
		
		$sorting_field = 'earnings';
		$sort_type = SORT_REGULAR;
		
		if( $sorting_field == 'earnings' || $sorting_field == 'price' ){
			
			$sort_type = SORT_NUMERIC;
			
		}

		foreach ($data['performance_stats'] as $key => $row) {
			$colmn[$key]  = $row[$sorting_field];
		}

		array_multisort($colmn, $sort_type, SORT_DESC, $data['performance_stats']);
		
		if($period_filter == TRUE){
			foreach($data['performance_stats'] as $key => $performance_stat) {
				if ($performance_stat['rents'] == 0 && $performance_stat['views'] == 0) {
					unset($data['performance_stats'][$key]);
					unset($data['performance_stats_totals'][$key]);
				}
			}
		}
		
		/**
		// Gather data for user statistics.
		*/
		$this->load->model('Newsletter_model');
		
		$users = $this->User_model->get_all();
		$total_users = $this->User_model->get_count();
		
		foreach( $users as $user )
		{
			$id = $user['id'];
			$name = $user['username'];
			
			$this->Performance_rental->clear_restrictions();
			$this->Performance_view->clear_restrictions();
			
			if( ! empty($period_from) ){
				$this->Performance_rental->set_restriction('payment_datetime', $date_from, '>=');
				$this->Performance_view->set_restriction('datetime', $date_from, '>=');
			}
			
			if( ! empty($period_till) ){
				$this->Performance_rental->set_restriction('payment_datetime', $date_till, '<=');
				$this->Performance_view->set_restriction('datetime', $date_till, '<=');
			}		
	
	
			$this->Performance_rental->set_restriction('user', $id);
			$user_rents = $this->Performance_rental->get_count();
			
			$this->Performance_view->set_restriction('user', $id);
			$user_views = $this->Performance_view->get_count();	

			$this->Newsletter_model->set_restriction('email', $name);
			$user_email = $this->Newsletter_model->get_count();
			
			$user_newsletter = 'nee';
			if( $user_email > 0 ){
				$user_newsletter = 'ja';
				$total_newsletters++;
			}
			
			$data['user_stats'][] = array( 
									'id' => $id,
									'name' => $name,
									'rents' => $user_rents,
									'views' => $user_views,
									'newsletter' => $user_newsletter,
								);
		}
		
		$sorting_field = 'rents';

		foreach ($data['user_stats'] as $key => $row) {
			$colmn[$key]  = $row[$sorting_field];
		}

		array_multisort($colmn, SORT_DESC, $data['user_stats']);
		
		if($period_filter == TRUE){
			foreach($data['user_stats'] as $key => $user_stat) {
				if ($user_stat['rents'] == 0 && $user_stat['views'] == 0) {
					unset($data['user_stats'][$key]);
				}
			}
		}
		
		/**
		// Get statistic totals
		*/
		
		$this->Performance_rental->clear_restrictions();
		$this->Performance_view->clear_restrictions();
		
		if( ! empty($period_from) ){
			$this->Performance_rental->set_restriction('payment_datetime', $date_from, '>=');
			$this->Performance_view->set_restriction('datetime', $date_from, '>=');
		}
			
		if( ! empty($period_till) ){
			$this->Performance_rental->set_restriction('payment_datetime', $date_till, '<=');
			$this->Performance_view->set_restriction('datetime', $date_till, '<=');
		}
		
		$rentals = $this->Performance_rental->get_all();
		$total_rentals = $this->Performance_rental->get_count();
		
		$filter = array();
		if( ! empty($period_from) ){ $filter['payment_datetime >='] = $date_from; }
		if( ! empty($period_till) ){ $filter['payment_datetime <='] = $date_till; }
		
		$total_earnings = $this->Performance_rental->get_sum($filter);
		
		$average_earnings = $total_earnings['payment']/$total_performances;

		$views = $this->Performance_view->get_all();
		$total_views = $this->Performance_view->get_count();
				
		$data['performance_stats_totals'] = array(
									'total_gebruikers' => $total_users,
									'total_newsletter' => $total_newsletters,
									'total_voorstellingen' => $total_performances,
									'total_rents' => $total_rentals,
									'total_views' => $total_views,
									'total_earnings' => number_format($total_earnings['payment'], 2, ',', '.'),
									'gemiddeld' => number_format($average_earnings, 2, ',', '.')
								);
	
		$this->load->view('administration/dashboard', $data);
	}

	public function login() {
		if ($this->input->post('username') !== FALSE)
		{
			$this->load->helper('admin_user');
			if (admin_user_login($this->input->post('username'), $this->input->post('password')))
			{
				$this->load->helper('url');
				redirect('/administration');
			}
			else
			{
				$this->load->view('administration/login', array('validation_error'=>TRUE));
			}
		}
		else
		{
			$this->load->view('administration/login', array('validation_error'=>FALSE));
		}
	}
	
	public function logout() {
		$this->load->helper('admin_user');
		$this->load->helper('url');
		
		admin_user_logout();
		redirect('/administration/login');
	}
}

/* End of file performance.php */
/* Location: ./application/controllers/administration.php */