<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administration extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/administration
	 *	- or -  
	 * 		http://example.com/index.php/administration/index
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/administration/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('performance/overview');
	}
}

/* End of file performance.php */
/* Location: ./application/controllers/administration.php */