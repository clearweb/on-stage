<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsletter extends Crud_controller
{

	var $crud_model 	= 'Newsletter_model';
	var $edit_view  	= '/newsletter/failed';
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/newsletter/added
	 * 
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/text/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function overview()
	{
		$this->load->helper('admin_user');
		admin_user_require();
		$this->load->model('Newsletter_model');
		
		$news = $this->Newsletter_model->get_all_emails();
		$data = array(
					  'newsletter' => $news
					  );
		
		$this->load->view('administration/newsletter/overview', $data);
	}
	 
	public function success($news_id)
	{
		$this->load->model('Newsletter_model');
		$this->load->library('form_validation');
		$this->Newsletter_model->load($news_id);
		
		$data['email'] = $this->Newsletter_model->getProperty('email');
		
		$this->load->view('/newsletter/success',$data);
	}
	
	protected function _require_write_rights()
	{
	
	}
	
	protected function _load_form_validation()
	{
		$this->load->library('form_validation');
		$this->load->database();
		$this->form_validation->set_rules('email', 'E-mail', 'valid_email|is_unique[newsletter.email]');
		
		$this->form_validation->set_message('is_unique', 'Dit e-mailadress is al geregistreerd.<br />Vul een ander e-mailadress in.');
		$this->form_validation->set_message('valid_email', 'Dit is geen geldig e-mailadress.<br />Controleer het e-mailadress en probeer het opnieuw.');
		//$this->form_validation->set_rules('email', 'E-mail', 'required');
	}
	
	protected function _after_edit_redirect()
	{
		$this->load->helper('url');
		redirect('/newsletter/success/'.$this->Newsletter_model->get_id());
	}
	
	protected function _populate_model_from_input()
	{
		$this->Newsletter_model->setProperty('email', $this->input->post('email'));
	}
}

/* End of file text.php */
/* Location: ./application/controllers/text.php */