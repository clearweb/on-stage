<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Performance extends Crud_controller
{

	var $crud_model    = 'Performance_model';
	var $edit_view     = 'administration/performance/edit';
	var $overview_url  = '/performance/overview';

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/performance
	 *	- or -  
	 * 		http://example.com/index.php/performance/index
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/performance/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($page = 1)
	{
		$this->load->model('Text_model');
		$this->load->model('Performance_model');
		
		$this->load->library('parser');
		
		$data = array();
		
		$this->Text_model->load(6);
		
		$data['text_bottom'] = $this->Text_model->getProperty('text');
		$data['text_bottom_title'] = $this->Text_model->getProperty('title');
		
		$items_per_page = 12;
		
		$uri_array = array_map('urldecode', $this->uri->uri_to_assoc(4));
		foreach($uri_array as $property => $value)
		{
			if (in_array($property, array('title','performer','genre')))
				$this->Performance_model->set_restriction($property, $value);
		}
		
		$data['search'] = '';
		if (isset($uri_array['search']))
		{
			$data['search'] = $uri_array['search'];
			$this->Performance_model->set_search_key($uri_array['search']);
		}
		
		$data['total_hits'] = $this->Performance_model->get_count();
		$last_page = ceil($data['total_hits']/$items_per_page);
		$data['performances']   = $this->Performance_model->get_all($items_per_page, min($last_page, $page), 'id', 'asc');
		
		$data['total_hits'] = $this->Performance_model->get_count();
		
		$data['items_per_page'] = $items_per_page;
		
		$this->parser->parse('performance/overview', $data);
	}
	
	/**
	 * handles the search form!
	 */
	public function search()
	{
		$this->load->helper('url');
		$url = '/performance/index/';
		$page = '1';
		$url_params = array();
		if ($this->input->get('controller') == 'performance' && $this->input->get('action') == 'index')
		{
			$url_params = json_decode($this->input->get('assoc_params'), true);
		}
		
		$url_params['search'] = $this->input->get('search');
		//Vorige code om te filteren.
		//$url_params['search'] = preg_replace('/[^'.$this->config->item('permitted_uri_chars').']/', '', $url_params['search']);
		$url_params['search'] = urlencode($url_params['search']);

		if (empty($url_params['search']) OR $url_params['search'] == 'zoek op voorstelling artiest...')
		{
			unset($url_params['search']);
		}
		$url_params['search'] = urldecode($url_params['search']);		
		redirect($url.$page.'/'.$this->uri->assoc_to_uri($url_params));
	}

	/**
	 * administration overview of performances
	 */
	public function overview() {
		$this->load->helper('admin_user');
		
		admin_user_require();
		
		$this->load->model('Performance_model');
		$this->Performance_model->set_return_only_available(false);
		$data = array(
					  'performances' => $this->Performance_model->get_all()
					  );
		$this->load->view('administration/performance/overview', $data);
	}
	
	/**
	 * show trailer page for a performance
	 */
	public function trailer($performance_id) {
		$this->load->model('Performance_model');
		$this->load->model('Performance_view');
		
		$this->load->library('vimeo');
		
		$this->Performance_model->load($performance_id);
		
		$this->require_nonblocked_country();
		
		$related_performances = $this->Performance_view->get_related_performances($performance_id);

		$video = $this->Performance_model->get_free_video();
		
		$video->set_video_api($this->vimeo);
		
		$this->load->model('Faq_model');
		$this->load->model('Faq_question_model');

		$this->Faq_model->id = 1;
			
		$faq_questions = $this->Faq_question_model->get_faq_questions($this->Faq_model->id);

		
		$this->load->view('performance/trailer', array('related_performances'=>$related_performances,
													   'video' => $video, 'faq_questions' => $faq_questions));
	}

	/**
	 * if the visitor comes from a blocked country for the performance, redirect him to correct page!
	 * @precondition $this->Performance_model is loaded!
	 */
	private function require_nonblocked_country() {
		$this->load->helper('country_filter');
		
		$ip = $_SERVER['REMOTE_ADDR'];
		
		$country = country_filter_get_ip_country($ip);
		
		if (in_array($country, $this->Performance_model->getProperty('filter_countries')))
		{
			show_error('Deze voorstelling is niet toegestaan in uw land');
		}
	}
	
	/**
	 * show video page for a performance
	 */
	public function video($performance_id) {
		$this->load->library('vimeo');
		
		$this->load->helper('url');
		$this->load->helper('user');
		
		$this->load->model('Performance_model');
		$this->load->model('Performance_view');
		$this->load->model('Performance_rental');
		$this->load->model('User_model');
		
		$this->Performance_model->load($performance_id);
		
		$this->require_nonblocked_country();
		
		user_require();
		
		$this->User_model->load_from_username(user_logged_in_username());
		
		if ( ! $this->Performance_rental->user_has_right_for_performance($this->User_model->get_id(), $performance_id))
		{
			redirect('/payment/hire_performance/'.$performance_id);
		}
		
		
		$this->Performance_view->setProperties( 
											   array( 
													 'performance'=>$performance_id,
													 'user'=>$this->User_model->get_id(),
													 'datetime'=>date('Y-m-d H:i:s')
													  )
												);
		$this->Performance_view->save_to_db();
		
		$related_performances = $this->Performance_view->get_related_performances($performance_id);
		
		$video = $this->Performance_model->get_paid_video();
		
		$video->set_video_api($this->vimeo);
		
		$this->load->view('performance/video', array('related_performances'=>$related_performances,
													 'video' => $video)
						  );			
	}

	/**
	 * overload the parent show_form
	 */
	protected function _show_form()
	{
		$this->load->helper('country_filter');
		$data['countries'] = array();
		$raw_countries = country_filter_get_countries();
		foreach($raw_countries as $country) {
			$data['countries'][$country['code2']] = ucfirst(strtolower($country['name']));
		}
		$this->load->view($this->edit_view, $data);
	}

	/**
	 * shows the rotated image of the performance (for the small-slider)
	 */
	public function slider_image($performance_id) {
		$this->load->library('Vimeo');
		$this->load->model('Performance_model');
		$this->Performance_model->set_video_api($this->vimeo);
		
		$this->Performance_model->load($performance_id);
		$image_file = substr($this->Performance_model->getProperty('image_url'), 16);
		$file_path  = './images/uploads/'.$image_file;
		
		$new_width  = 340;
		$new_height = 230;
		
		header("Cache-Control: private, max-age=10800, pre-check=10800");
		header("Pragma: private");
		header("Expires: " . date(DATE_RFC822,strtotime(" 2 day")));
		
		if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) 
			&& 
			(strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == filemtime($file_path))) {
			// send the last mod time of the file back
		  
			header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($file_path)).' GMT', true, 304);
			exit;
		}
		
		$extension = substr($file_path, -3);
		switch ( strtolower($extension) )
		{
		case 'png':
			$im = imagecreatefrompng($file_path);
			break;
		case 'jpg':
		case 'jpeg':
			$im = imagecreatefromjpeg($file_path);
			break;
		case 'gif':
			$im = imagecreatefromgif($file_path);
			break;
		}
		
		$trans_color    = imagecolorallocate($im, 255, 255, 255);
		$rotation_angle = 10;
		$clockwise = FALSE;
		
		if (strlen($this->Performance_model->getProperty('performer'))<=17)
		{
			$clockwise = TRUE;
		}
		
		$rotation = ($clockwise)?360-$rotation_angle:$rotation_angle;
		
		$rotated = imagerotate($im, $rotation, $trans_color);
		
		list($width, $height, $type, $attr) = getimagesize($file_path);
		$alpha = deg2rad($rotation);
		
		$right_top_corner_y    = $width * sin($alpha);
		$left_bottom_corner_y  = $height * cos($alpha);
		
		$left_bottom_corner_x  = -$height * sin($alpha);
		$right_top_corner_x    = $width * cos($alpha);
		
		$cropped_height = min($left_bottom_corner_y - abs($right_top_corner_y), $left_bottom_corner_y);
		$cropped_width  = max($right_top_corner_x - abs($left_bottom_corner_x), $right_top_corner_x);
		
		$cropped = imagecreatetruecolor($cropped_width, $cropped_height);
		
		$resized = imagecreatetruecolor($new_width, $new_height);
		
		imagecopy($cropped, $rotated, 0, 0, abs($left_bottom_corner_x), abs($right_top_corner_y), $cropped_width, $cropped_height);
		
		imagecopyresampled($resized, $cropped, 0, 0, 0, 0, $new_width, $new_height, $cropped_width, $cropped_height);
		
		header("Content-Type: image/png");
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($file_path)) . ' GMT');

		imagepng($resized);
		
		imagedestroy($im);
		imagedestroy($rotated);
		imagedestroy($cropped);
		imagedestroy($resized);
	}
	
	
	protected function _load_form_validation()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Titel', 'required');
		$this->form_validation->set_rules('performer', 'Uitvoerende', 'required');
		$this->form_validation->set_rules('genre', 'Genre', 'required');
		$this->form_validation->set_rules('description', 'Beschrijving', 'required');
		$this->form_validation->set_rules('available_from', 'Beschikbaar vanaf', 'callback_valid_date');
		$this->form_validation->set_rules('rental_period', 'Huurperiode', 'required|is_natural_no_zero');
		$this->form_validation->set_rules('rental_price', 'Huurprijs', 'required|is_numeric|greater_than[0]');
	}

	function valid_date($str)
	{
		if ( ! preg_match('#^[0-9]{2}-[0-9]{2}-[0-9]{4}$#', $str))
			return FALSE;
	} 
	
	protected function _populate_model_from_input()
	{
		$this->Performance_model->setProperty('title', $this->input->post('title'));
		$this->Performance_model->setProperty('performer', $this->input->post('performer'));
		$this->Performance_model->setProperty('genre', $this->input->post('genre'));
		$this->Performance_model->setProperty('duration', $this->input->post('duration'));
		$crew        = $this->input->post('crew');
		$year        = $this->input->post('year');
		$regission   = $this->input->post('regission');
		$production  = $this->input->post('production');
		$texts       = $this->input->post('texts');
		$music       = $this->input->post('music');
		$description = $this->input->post('description');
		$title_color = $this->input->post('title_color');
		$summary     = $this->input->post('summary');
		$video_id    = $this->input->post('video_id');
		$video_url   = $this->input->post('video_url');
		
		if (strlen($title_color) < 7)
			$this->Performance_model->setProperty('title_color', '#333333');
		else
			$this->Performance_model->setProperty('title_color', $title_color);
		
		
		$config['upload_path'] = './images/uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']= '20048';

		$this->load->library('upload', $config);
		
		if ( $this->upload->do_upload('image'))
		{
			$upload_data = $this->upload->data();
			$this->Performance_model->setProperty('image_url', '/images/uploads/'.$upload_data['file_name']);
		}
		else
		{
			$this->Performance_model->setProperty('image_url', $this->Performance_model->getProperty('image_url', NULL));
		}
		
		
		if ($crew)
			$this->Performance_model->setProperty('crew', $crew);
		if ($year)
			$this->Performance_model->setProperty('year', $year);
		if ($regission)
			$this->Performance_model->setProperty('regission', $regission);
		if ($production)
			$this->Performance_model->setProperty('production', $this->input->post('production'));
		if ($texts)
			$this->Performance_model->setProperty('texts', $this->input->post('texts'));
		if ($music)
			$this->Performance_model->setProperty('music', $this->input->post('music'));
		if ($description)
			$this->Performance_model->setProperty('description', $this->input->post('description'));
		if ($summary)
			$this->Performance_model->setProperty('summary', $this->input->post('summary'));
		if ($video_id)
			$this->Performance_model->setProperty('video_id', $this->input->post('video_id'));
		if ($video_url)
			$this->Performance_model->setProperty('video_url', $this->input->post('video_url'));

		$available_date = '0000-00-00';

		if (preg_match( '/^([0-9]{2})-([0-9]{2})-([0-9]{4})$/',$this->input->post('available_from'), $date_attr))
		{
			$available_date = "{$date_attr[3]}-{$date_attr[2]}-{$date_attr[1]}";
		}
		
		$this->Performance_model->setProperty('available_from', $available_date);
		$this->Performance_model->setProperty('rental_period', $this->input->post('rental_period'));
		$this->Performance_model->setProperty('rental_price', $this->input->post('rental_price'));
		$this->Performance_model->setProperty('filter_countries', $this->input->post('filter_countries'));
		
	}
	
	function load_related_videos($page=2) {
		$this->load->model('Performance_model'); 

		$items_per_page = 5;
		
		$items_loaded = $this->Performance_model->get_all($items_per_page, $page, 'id', 'asc');

		foreach( $items_loaded as $content )
		{
			echo '<li>';
			echo '<a href="/performance/trailer/'. $content['id'] .'"><img src="'. $content['thumbnail_url'].'" height="83" width="134" /></a>';
			echo '<div class="carousel-item-title"><a href="/performance/trailer/'. $content['id'] .'">'. $content['title'] .'</a></div>';
			echo '<div class="carousel-item-category">'. $content['genre'] .'</div>';
			echo '</li>';
			
			if( $this->__check_for_more_content($items_per_page))
			{
				echo '<script>remove_related_content_button();</script>';
			}

		}
	}
	
	private function __check_for_more_content($current)
	{
		$this->load->model('Performance_model');
		
		$more_content = FALSE;
		
		$total_content = $this->Performance_model->get_count();
		if( $total_content >= $current){
			$more_content = TRUE;
		}
		return( $more_content );
	}
	
	function check_for_more_content($current)
	{
		echo ($this->__check_for_more_content($current)?'true':'false');
	}
}

/* End of file performance.php */
/* Location: ./application/controllers/performance.php */