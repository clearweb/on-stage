<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Text extends Crud_controller
{

	var $crud_model = 'Text_model';
	var $edit_view  = 'administration/text/edit';
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/text/overview
	 * 
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/text/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function overview()
	{
		$this->load->helper('admin_user');
		admin_user_require();
		$this->load->model('Text_model');
		
		$texts = $this->Text_model->get_all();
		$data = array(
					  'texts' => $texts
					  );
		
		$this->load->view('administration/text/overview', $data);
	}

	protected function _after_edit_redirect()
	{
		$this->load->helper('url');
		redirect('/text/overview');
	}
	
	protected function _load_form_validation()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'titel', 'required');
	}
	
	protected function _populate_model_from_input()
	{
		$this->Text_model->setProperty('title', $this->input->post('title'));
		$this->Text_model->setProperty('text', $this->input->post('text'));
	}
}

/* End of file text.php */
/* Location: ./application/controllers/text.php */