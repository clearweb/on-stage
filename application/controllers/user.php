<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Controller
{	
	function register()
	{
		$this->load->library('form_validation');
		$this->load->database();
		
		$this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique[user.username]');
		$this->form_validation->set_rules('password2', 'Wachtwoord herhalen', 'required|matches[password]');
		$this->form_validation->set_rules('password', 'Wachtwoord', 'required');
		$this->form_validation->set_rules('policy', 'Algemene Voorwaarden', 'required');
		$this->form_validation->set_rules('double_opt_in', 'Dubbele Opt In', 'trim');
		
		$this->form_validation->set_message('is_unique', 'Bezet');
		$this->form_validation->set_message('valid_email', 'Ongeldig');
		$this->form_validation->set_message('matches', 'Komt niet overeen');
		$this->form_validation->set_message('required', 'Vereist');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		if ($this->input->get('ajax', FALSE))
		{
			$this->form_validation->set_error_delimiters('', '');

			$this->form_validation->run();
			$error = form_error($this->input->get('name'));
			
			if ( empty($error))
			{
				echo 'valid';
			}
			else
			{
				echo $error;
			}
		}
		else
		{
			if ($this->form_validation->run() == FALSE )
			{
				$data['error'] = '';
			
				if( $this->input->post('register') ){
					$data['error'] = '<div class="error"><p>U dient de algemene voorwaarden te accepteren!</p></div>';
				}
				$this->load->view('/user/registration',$data);
				
			}
			else
			{
				$this->load->model('User_model');
				$this->load->model('Newsletter_model');
				
				if($this->Newsletter_model->lookup_newsletter($this->input->post('email'))){
				
					$double_opt_in = TRUE;
					
				}elseif($this->input->post('double_opt_in')){
					
					$this->Newsletter_model->setProperty('email', $this->input->post('email'));
					$this->Newsletter_model->save_to_db();
					
					$double_opt_in = TRUE;
										
				}else{	
				
					$double_opt_in = FALSE;
					
				}
				
				$this->User_model->setProperty('username', $this->input->post('email'));
				$this->User_model->setProperty('password', $this->input->post('password'));
				$this->User_model->setProperty('active', FALSE);
				$this->User_model->save_to_db();
				
				$this->User_model->send_activation_mail();
				
				$this->load->view('/user/activation_needed');
			}
		}
	}

	function activate($email, $key)
	{
		$this->load->model('User_model');
		$this->load->helper('user');
		
		$email = urldecode($email);
		
		$this->User_model->load_from_username($email);
		
		if ($this->User_model->activate($key))
		{
			user_login_without_authentication($this->User_model->getProperty('username'));
			$this->load->view('/user/activation');
		}
		else
		{
			show_404();
		}
	}
	
	function remove($action='')
	{
		$this->load->helper('user');
		$this->load->library('form_validation');
		$this->load->model('User_model');
		$this->load->model('Performance_view');
		$this->load->model('Performance_rental');
		
		$this->User_model->load_from_username(user_logged_in_username());
		
		user_require();
					
		$data = array(
					  'changed'=> FALSE,
					  'recent_performances' => $this->Performance_view->get_recent_performance_videos($this->User_model->get_id(), 5),
					  'credit_performances' => $this->Performance_rental->get_credit_performances($this->User_model->get_id())
					  );
		
		$data['credits'] = FALSE;
		if(  $this->Performance_rental->get_credit_performances($this->User_model->get_id()) ){
			$data['credits'] = TRUE;
		}
				
		if($action == 'delete'){
		
			user_logout();
			
			$this->User_model->delete($this->User_model->get_id());
			$this->load->view('/user/removed');
			
		}else{
		
			$this->load->view('/user/remove', $data);
			
		}
	}

	function reset_password()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'email', 'required|valid_email');
		
		$this->form_validation->set_message('valid_email', 'Ongeldig');
		$this->form_validation->set_message('required', 'Leeg');
		
		if ($this->input->get('ajax', FALSE))
		{
			$this->form_validation->set_error_delimiters('', '');

			$this->form_validation->run();
			$error = form_error($this->input->get('name'));
			
			if ( empty($error))
			{
				echo 'valid';
			}
			else
			{
				echo $error;
			}
		}
		elseif($this->input->post('email') && $this->form_validation->run())
		{
			$this->load->model('User_model');
			$this->User_model->load_from_username($this->input->post('email'));
			
			if ($this->User_model->get_id() > 0)
			{
				$this->User_model->set_password_reset();
				$this->User_model->send_password_reset_mail();
			}
			
			$this->load->view('/user/change_password_needed');
		}
		else 
		{
			$this->load->view('/user/reset_password');
		}
	}
	
	function change_password($email='', $key='')
	{
		$this->load->library('form_validation');
		$this->load->helper('user');
		
		$email = urldecode($email);
		
		$this->form_validation->set_rules('password2', 'Wachtwoord herhalen', 'required|matches[password]');
		$this->form_validation->set_rules('password', 'Wachtwoord', 'required');
		
		$this->form_validation->set_message('matches', 'Komt niet overeen');
		$this->form_validation->set_message('required', 'Leeg');
		
		if ($this->input->get('ajax', FALSE))
		{
			$this->form_validation->set_error_delimiters('', '');
			
			$this->form_validation->run();
			$error = form_error($this->input->get('name'));
			
			if ( empty($error))
			{
				echo 'valid';
			}
			else
			{
				echo $error;
			}
		}
		elseif($this->form_validation->run())
		{       
			$this->load->model('User_model');
			$this->User_model->load_from_username($email);
			
			$expire_date = $this->User_model->getProperty('forgot_password_key_expire', '0000-00-00');
			
			if ($key == $this->User_model->getProperty('forgot_password_key') && strtotime($expire_date) >= time())
			{
				$this->User_model->setProperty('password', $this->input->post('password'));
				$this->User_model->setProperty('forgot_password_key', null);
				$this->User_model->setProperty('forgot_password_key_expire', null);
				
				$this->User_model->save_to_db();

				user_login_without_authentication($this->User_model->getProperty('username'));
				
				$this->load->view('/user/password_changed');
			}
			else
			{
				show_404();
			}
		}
		else
		{
			$email = urldecode($email);
			
			$this->load->model('User_model');
			$this->User_model->load_from_username($email);
                
			$expire_date = $this->User_model->getProperty('forgot_password_key_expire', '0000-00-00');
			
			if ($key == $this->User_model->getProperty('forgot_password_key') && strtotime($expire_date) >= time())
			{
				$this->load->view('/user/change_password', array('email'=>$email));
			}
			else
			{
				show_404();
			}
		}
	}
	
	function email_change_password($email='', $key='')
	{
		$this->load->helper('user');
		$this->load->library('form_validation');
		$this->load->model('User_model');
		$this->load->model('Performance_view');
		$this->load->model('Performance_rental');
		
		$email = urldecode($email);
			
		$this->User_model->load_from_username($email);
		$expire_date = $this->User_model->getProperty('forgot_password_key_expire', '0000-00-00');
			
			
		user_require();
			
		$data = array(
					  'changed'=> FALSE,
					  'recent_performances' => $this->Performance_view->get_recent_performances($this->User_model->get_id()),
					  'credit_performances' => $this->Performance_rental->get_credit_performances($this->User_model->get_id())
		 );
						  
		$data['credits'] = FALSE;
		
		if ($key == $this->User_model->getProperty('forgot_password_key') && strtotime($expire_date) >= time())
		{
			$this->load->view('/user/account', $data);
		}else{
			show_404();
		}
	}

	function login($referer = null) {
		$this->load->model('User_model');
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->helper('user');
		
		if (user_logged_in())
		{
			show_error('U bent al ingelogd');
		}
		else
		{
			
			if ($this->input->post('referer') !== FALSE)
			{
				$referer = $this->input->post('referer');
			}
			
			if ($this->session->userdata('current_url') !== FALSE)
			{
				$referer = $this->session->userdata('current_url');
				$this->session->unset_userdata('current_url');
			}
			
			if (user_login($this->input->post('email'), $this->input->post('password')))
			{
				if (empty($referer)){
					redirect('/user/account');
				}
				else
				{
					
					redirect(urldecode($referer));
				}
			}
			elseif($this->input->post('email') == FALSE && $this->input->post('password', null) == FALSE)
			{
				$this->load->view('/user/login_required', array('referer'=>$referer));
			}
			else 
			{
				$this->load->view('/user/password_incorrect', array('referer'=>$referer));
			}
		}
	}

	function logout()
	{
		$this->load->helper('user');
		$this->load->helper('url');
		
		if (user_logged_in())
		{
			user_logout();
			redirect('/');
		}
		else
		{
			show_error('U bent al ingelogd');
		}
	}
	
	function account_password2_check($str)
	{
		return ($this->input->post('password') == '' || $this->input->post('password') == $str);
	}
	
	function account()
	{
		$this->load->helper('user');
		$this->load->library('form_validation');
		$this->load->model('User_model');
		$this->load->model('Performance_view');
		$this->load->model('Performance_rental');
		
		$this->User_model->load_from_username(user_logged_in_username());
		
		user_require();
		
		$data = array(
					  'changed'=> FALSE,
					  'recent_performances' => $this->Performance_view->get_recent_performances($this->User_model->get_id()),
					  'credit_performances' => $this->Performance_rental->get_credit_performances($this->User_model->get_id())
					  );
					  
		$data['credits'] = FALSE;
		if(  $this->Performance_rental->get_credit_performances($this->User_model->get_id()) ){
			$data['credits'] = TRUE;
		}
		
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('password2', 'Herhaal wachtwoord', 'callback_account_password2_check');
		$this->form_validation->set_rules('double_opt_in', 'Dubbele Opt In', 'trim');
		
		$this->form_validation->set_message('valid_email', 'Ongeldig');
		$this->form_validation->set_message('matches', 'Komt niet overeen');
		$this->form_validation->set_message('required', 'Leeg');
		
		if ($this->input->get('ajax', FALSE))
		{
			$this->form_validation->set_error_delimiters('', '');
			
			$this->form_validation->run();
			$error = form_error($this->input->get('name'));
			
			if ( empty($error))
			{
				echo 'valid';
			}
			else
			{
				echo $error;
			}
			
		}
		elseif ($this->form_validation->run())
		{
			$this->load->model('Newsletter_model');
			
			$password_check = $this->input->post('password', '');
			
			if ( ! empty($password_check))
			{			
				$this->User_model->setProperty('password', $this->input->post('password'));
				$data['changed'] = TRUE;
				$this->User_model->set_password_reset();
				$this->User_model->send_password_change_mail();
			}
			
			$this->User_model->setProperty('username', $this->input->post('email'));
			$this->User_model->save_to_db();
			
			if($this->input->post('double_opt_in')){
				$this->Newsletter_model->setProperty('email',$this->input->post('email'));
				$this->Newsletter_model->save_to_db();
			}else{
				$this->Newsletter_model->remove_newsletter($this->User_model->getProperty('username'));
			}
		
			$this->load->view('/user/account', $data);
		}
		else
		{
			$this->load->view('/user/account', $data);
		}
		
	}

	/**
	 * Admin user overview!
	 */
	function overview()
	{
		$this->load->helper('admin_user');
		
		admin_user_require();
		$this->load->model('User_model');
		
		$data = array(
					  'users' => $this->User_model->get_all()
					  );
		$this->load->view('administration/user/overview', $data);
	}

	/**
	 * Lets admin login as any webuser
	 */
	function login_as($id)
	{
		$this->load->helper('admin_user');
		$this->load->helper('user');
		$this->load->helper('url');
		
		$this->load->model('User_model');
		
		$this->User_model->load($id);
		
		if ($this->User_model->loaded)
		{
			user_login_without_authentication($this->User_model->getProperty('username'));
			
			redirect('/user/account');
		}

		admin_user_require();
		
	}
}