<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq extends CI_Controller
{
	/**
	 * show faq for site.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/faq/questions
	 *
	 */
	 
	public function questions($faq_name)
	{
		$this->load->model('Faq_model');
		$this->load->model('Faq_question_model');

		if ($this->Faq_model->faq_name_exists($faq_name)) {
			$this->Faq_model->load_from_name($faq_name);
			
			$faq_questions = $this->Faq_question_model->get_faq_questions($this->Faq_model->id);
			
			$data= array(
						 'faq_title' => $this->Faq_model->title,
						 'faq_description' => $this->Faq_model->description,
						 'top_image' => $faq_name,
						 'page_url' => $faq_name,
						 'faq_questions' => $faq_questions,
						 'faq_questions2' => $faq_questions
						 );
			$this->load->view('faq/questions', $data);
		} else {
			show_404();
		}
	}
	
	public function overview() {
		$this->load->helper('admin_user');
		admin_user_require();
		
		$this->load->library('parser');
		$this->load->model('Faq_model');
		$faqs = $this->Faq_model->get_faqs();
		$data = array( 'faqs' => $faqs );
		$this->parser->parse('administration/faq/overview', $data);
	}
	
	public function edit($faq_id) {
		$this->load->helper('admin_user');
		admin_user_require();
		
		$this->load->library('parser');
		$this->load->model('Faq_model');
		
		if ($this->Faq_model->faq_id_exists($faq_id)) {
			
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules('title', 'titel', 'required');
			
			if ($this->form_validation->run()) {
				$this->load->helper('url');
				
				$this->Faq_model->load_from_id($faq_id);
				
				$this->Faq_model->id = $faq_id;
				$this->Faq_model->title = $this->input->post('title');
				$this->Faq_model->description = $this->input->post('description');
				
				$this->Faq_model->write_through();
				
				redirect('/faq/overview');
			} else {
				if ($this->input->post('title') === FALSE) {
					$this->Faq_model->load_from_id($faq_id);
				} else {
					$this->Faq_model->id = $faq_id;
					$this->Faq_model->title = $this->input->post('title');
					$this->Faq_model->description = $this->input->post('description');
				}
				
				$this->load->view('administration/faq/edit');
			}
			
		} else {
			show_404();
		}
	}
	
}

/* End of file faq.php */
/* Location: ./application/controllers/faq.php */