<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends CI_Controller
{
	function data()
	{
		$this->load->library('email');
		
		log_message('debug', '-------POSTBACK START');
		
		$this->load->library('payment_library');
		$this->load->model('Performance_model');
		$this->load->model('Performance_rental');
		$this->load->model('User_model');
		
		$performance_id = $this->payment_library->get_postback_performance();
		$user_id        = $this->payment_library->get_postback_user();

		log_message('debug', '-------POSTBACK PERFORMANCE ID = '.$performance_id);
		log_message('debug', '-------POSTBACK USER ID = '.$user_id);
		
		if ($this->Performance_model->exists($performance_id))
		{
			log_message('debug', '-------POSTBACK PERFORMANCE EXISTS');
			if($this->payment_library->get_postback_status() == 'paid')
			{
				if ( ! $this->Performance_rental->user_has_right_for_performance($this->User_model->get_id(), $performance_id))
				{
					log_message('debug', '-------POSTBACK PERFORMANCE WILL BE RENT');
					$this->Performance_model->load($performance_id);
					$this->Performance_rental->setProperties( array(
																	'performance' => $performance_id,
																	'user'        => $user_id,
																	'payment'     => $this->Performance_model->getProperty('rental_price')
																	)
															  );
			
					$this->Performance_rental->set_rental_period($this->Performance_model->getProperty('rental_period'));
												
					$this->Performance_rental->save_to_db();
				}
			}
			else
			{
				if ( $this->Performance_rental->user_has_right_for_performance($this->User_model->get_id(), $performance_id))
				{
					// here the performance will be disabled for user!
					$this->Performance_rental->stop_user_performance_rental();
				}
			}
		}
	}

	function success()
	{
		$this->load->model('Performance_rental');
		$this->load->model('Performance_model');
		$this->load->model('User_model');
		
		$this->load->library('payment_library');
		$this->load->helper('user');
		
		user_require();
		
		$this->User_model->load_from_username(user_logged_in_username());
		
		$performance_id = $this->payment_library->get_success_performance();
		if ($this->Performance_model->exists($performance_id))
		{
			$this->load->view('/payment/success', array('performance_id'=>$performance_id));
		}
		else
		{
			show_error('Er is een fout opgetreden, probeer het nogmaals');
		}
	}
	
	function failure()
	{
		$this->load->library('payment_library');
		$performance_id = $this->payment_library->get_failure_performance();
		$this->load->view('/payment/failure', array('performance_id'=>$performance_id));
	}
	
	function hire_performance($performance_id)
	{
		$this->load->helper('user');
		$this->load->helper('url');
		$this->load->model('Performance_model');
		$this->load->model('Performance_rental');
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->load->library('payment_library');
		
		user_require();
		
		$this->User_model->load_from_username(user_logged_in_username());
		
		if ( $this->Performance_rental->user_has_right_for_performance($this->User_model->get_id(), $performance_id))
		{
			redirect('/performance/video/'.$performance_id);
		}
		
		$this->form_validation->set_rules('policy', 'Algemene voorwaarden', 'required');
		
		if ($this->Performance_model->exists($performance_id))
		{
			$this->Performance_model->load($performance_id);
			
			if ($this->form_validation->run())
			{
				$url = $this->payment_library->get_payment_url($this->input->post('payment_method'), $performance_id, $this->User_model->get_id(), $this->Performance_model->getProperty('rental_price') * 100);
				redirect($url);
			}
			else
			{
				$payment_methods = array(
										 'ideal'=>'iDeal',
										 'paypal'=>'PayPal'
										 );
				$this->load->view('/payment/payment_form', array('payment_methods'=>$payment_methods));
			}
		}
		else
		{
			show_404();
		}
		
	}
}