<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/home
	 *	- or -  
	 * 		http://example.com/index.php/home/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/home/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->library('parser');
		$this->load->model('Text_model');
		$this->load->model('Performance_model');
		
		$data = array();
		$this->Text_model->load(1);
		$data['text_top'] = $this->Text_model->getProperty('text');
		$data['text_top_title'] = $this->Text_model->getProperty('title');

		$this->Text_model->load(2);
		$data['text_bottom'] = $this->Text_model->getProperty('text');
		$data['text_bottom_title'] = $this->Text_model->getProperty('title');

		$this->Text_model->load(3);
		$data['text_prefooter1'] = $this->Text_model->getProperty('text');
		$data['text_prefooter1_title'] = $this->Text_model->getProperty('title');
		
		$this->Text_model->load(4);
		$data['text_prefooter2'] = $this->Text_model->getProperty('text');
		$data['text_prefooter2_title'] = $this->Text_model->getProperty('title');
		
		$this->Text_model->load(5);
		$data['text_prefooter3'] = $this->Text_model->getProperty('text');
		$data['text_prefooter3_title'] = $this->Text_model->getProperty('title');

		$this->Performance_model->set_return_only_available();
		
		$data['trailers'] = $this->Performance_model->get_all(12, 1, 'title');
		$data['artists'] = $this->Performance_model->get_all(12, 1, 'performer');

		$data['new_performances'] = $this->Performance_model->get_new();
		$data['popular_performances'] = $this->Performance_model->get_popular();
		
		$this->parser->parse('home', $data);
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */