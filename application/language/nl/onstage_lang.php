<?php
$lang['has_other_paid_video_validation'] = 'Er bestaat al een betaalde video voor deze voorstelling.';
$lang['greater_than'] = 'Het "%s" veld moet groter zijn dan %s.';
$lang['valid_date'] = 'Het "%s" veld moet een geldige datum zijn.';