<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('layout_header'))
{
	function layout_header($menu_item = 'home', $additional_css = array(), $additional_js = array(), $meta_taggs = array())
	{
		$ci=& get_instance();
		$ci->load->helper('html');
		$ci->load->helper('url');
		
		$header  = doctype('html5').PHP_EOL;
		$header .= '<html>'.PHP_EOL;
		
		$header .= '<head>'.PHP_EOL;
		$header .= '<meta name="viewport" content="width=1024">'.PHP_EOL;
		
		foreach( $meta_taggs as $tagg )
		{
			$header .= '<meta property="og:title" content="'.$tagg['title'].'" />'.PHP_EOL;
			$header .= '<meta property="og:url" content="'.$tagg['url'].'" />'.PHP_EOL;
			$header .= '<meta property="og:description" content="'.$tagg['description'].'" />'.PHP_EOL;
			$header .= '<meta property="og:image" content="'.$tagg['image'].'" />'.PHP_EOL;
		}
		//$header .= '<meta property="og:image" content="'.$additional_code.'" />'.PHP_EOL;
		
		$header .= '<title>On-Stage.tv - Altijd op de eerste rij</title>';
		
		$link = array(
          'href' => 'css/main.css',
          'rel' => 'stylesheet',
          'type' => 'text/css',
          'media' => 'all'
		);
		
		$header .= link_tag($link).PHP_EOL;
		
		$additional_css = layout_get_additional_css($additional_css);
		$additional_js  = layout_get_additional_js($additional_js);
		
		foreach($additional_css as $css)
		{
			if (substr($css, 0, 7) == 'https://')
			{
				$header .= link_tag($css).PHP_EOL;
			}
			else
			{
				$header .= link_tag('css/'.$css.'.css').PHP_EOL;
			}

			
		}
		
		$header .= '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>'.PHP_EOL;
		$header .= '<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>'.PHP_EOL;
		$header .= '<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700" rel="stylesheet" type="text/css">'.PHP_EOL;
    
		foreach($additional_js as $js)
		{
			if (substr($js, 0, 7) == 'https://')
			{
				$header .= '<script type="text/javascript" src="'.$js.'"></script>'.PHP_EOL;
			}
			else
			{
				$header .= '<script type="text/javascript" src="'.base_url('js/'.$js.'.js').'"></script>'.PHP_EOL;
			}
		}
		
		$header .= '<script type="text/javascript" src="'.base_url('js/main.js').'"></script>'.PHP_EOL;
		
		$header .= 
		'
		<script>
		  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');
		 
		  ga(\'create\', \'UA-40899188-1\', \'on-stage.tv\');
		  ga(\'send\', \'pageview\');
		 
		</script>
		';
		
		$header .= '</head>'.PHP_EOL;
		
		$header .= '<body>'.PHP_EOL;
		$header .= $ci->parser->parse('layout/header', array('current_menu'=>$menu_item), true);
		return $header;
	}
}

if ( ! function_exists('layout_get_additional_css'))
{
	function layout_get_additional_css(array $additional_css = array())
	{
		$ci=& get_instance();
		$css_array = $ci->config->item('layout_css');
		
		if ($css_array === FALSE) {
			$css_array = array();
		}
		
		$css_array = array_merge($css_array, $additional_css);
		return $css_array;
	}
}

if ( ! function_exists('layout_get_additional_js'))
{
	function layout_get_additional_js(array $additional_js = array())
	{
		$ci=& get_instance();
		$js_array = $ci->config->item('layout_js');
		
		if ($js_array === FALSE) {
			$js_array = array();
		}

		$js_array = array_merge($js_array, $additional_js);
		return $js_array;
	}
}

if ( ! function_exists('layout_add_css'))
{
	function layout_add_css($css)
	{
		$ci=& get_instance();
		$css_array = $ci->config->item('layout_css');
		if ($css_array === FALSE) {
			$css_array = array();
		}

		$css_array[] = $css;
		$ci->config->set_item('layout_css', $css_array);
	}
}

if ( ! function_exists('layout_add_js'))
{
	function layout_add_js($js)
	{
		$ci=& get_instance();
		$js_array = $ci->config->item('layout_js');
		
		if ($js_array === FALSE) {
			$js_array = array();
		}
		
		$js_array[] = $js;
		
		$ci->config->set_item('layout_js', $js_array);
	}
}

if ( ! function_exists('layout_footer'))
{
	function layout_footer()
	{
		$ci=& get_instance();
		
		$ci->load->model('Faq_model');
		$ci->load->model('Faq_question_model');

		$data = array(
					  'about'         => $ci->Faq_question_model->get_by_category('about'),
					  'watch_and_pay' => $ci->Faq_question_model->get_by_category('watch-and-pay'),
					  'support'       => $ci->Faq_question_model->get_by_category('support'),
					  'other'         => $ci->Faq_question_model->get_by_category('other')
					  );
		
		foreach($data as $category_key => $faq_category)
		{
			foreach($faq_category as $question_key => $faq_question)
			{
				$ci->Faq_model->load_from_id($faq_question['faq_id']);
				$data[$category_key][$question_key]['faq_title'] = $ci->Faq_model->name;
			}
		}

		$footer  = $ci->load->view('layout/footer', $data, true);
		$footer .= '</body></html>';
		return $footer;
	}
}


if ( ! function_exists('layout_sidefilters'))
{	
	function layout_sidefilters($artist_letter = 'a', $performance_letter = 'a')
	{
		$ci=& get_instance();
		$ci->load->model('Performance_model');
		
		$performance_model = new Performance_model;
				
		$uri_array = array_map('urldecode', $ci->uri->uri_to_assoc(4));
		$base_uri  = '/performance/index/'.$ci->uri->segment(3, 1).'/';
		
		$selected_performer   = FALSE;
		$selected_performance = FALSE;
		$selected_genre       = FALSE;
		
		if (!empty($uri_array['performer']))
		{
			$selected_performer = $uri_array['performer'];
		}
		elseif (!empty($uri_array['title']))
		{
			$selected_performance = $uri_array['title'];
		}
		elseif (!empty($uri_array['genre']))
		{
			$selected_genre = $uri_array['genre'];
		}
		
		// Make sure only performances with a starting date lower or equal to the current date are selected.
		$performance_model->set_restriction('available_from', date('Y-m-d',time()), '<=');	
		
		$performers   = $performance_model->get_distinct_column('performer');
		$performances = $performance_model->get_distinct_column('title');
		$raw_genres   = $performance_model->get_distinct_column('genre');
		
		$performance_alphabetical = array();
		$performers_alphabetical = array();
		
		$genres = array();
		
		$letters = range('a', 'z');
		
		foreach($letters as $letter)
		{
			$performance_alphabetical[$letter] = array();
			$performers_alphabetical[$letter] = array();
		}
		
		foreach($performers as $performer)
		{
			$performer_uri_array = $uri_array;
			$performer_uri_array['performer'] = $performer;
			unset($performer_uri_array['genre']);
			unset($performer_uri_array['title']);
			
			$performers_alphabetical[strtolower(substr($performer, 0, 1))][] = array(
																					 'name' => $performer,
																					 'url' => $base_uri.$ci->uri->assoc_to_uri($performer_uri_array)
																					 );
		}
		
		foreach($performances as $performance)
		{
			$performance_uri_array = $uri_array;
			$performance_uri_array['title'] = $performance;
			unset($performance_uri_array['genre']);
			unset($performance_uri_array['performer']);
			
			$performance_alphabetical[strtolower(substr($performance, 0, 1))][] = array(
																					 'name' => $performance,
																					 'url' => $base_uri.$ci->uri->assoc_to_uri($performance_uri_array)
																					 );
		}
		
		foreach($raw_genres as $raw_genre)
		{
			$genre_uri_array = $uri_array;
			$genre_uri_array['genre'] = $raw_genre;
			unset($genre_uri_array['title']);
			unset($genre_uri_array['performer']);
		
			$genres[] = array(
							  'name' => $raw_genre,
							  'url' => $base_uri.$ci->uri->assoc_to_uri($genre_uri_array)
							  );
		}
				
		$all_performers_uri_array   = $uri_array;
		$all_performances_uri_array = $uri_array;
		$all_genres_uri_array       = $uri_array;

		unset($all_performers_uri_array['performer']);
		unset($all_performances_uri_array['title']);
		unset($all_genres_uri_array['genre']);
		
		$data = array(
					  'performers' => $performers_alphabetical,
					  'performance' => $performance_alphabetical,
					  'genres' => $genres,
					  'selected_performer' => $selected_performer,
					  'selected_performance' => $selected_performance,
					  'selected_genre' => $selected_genre,
					  'all_performers_uri' => $base_uri.$ci->uri->assoc_to_uri($all_performers_uri_array),
					  'all_performances_uri' => $base_uri.$ci->uri->assoc_to_uri($all_performances_uri_array),
					  'all_genres_uri' => $base_uri.$ci->uri->assoc_to_uri($all_genres_uri_array)
					  );

		
		$sidefilters = $ci->load->view('layout/sidefilter', $data, true);
		
		return $sidefilters;
	}
}

if ( ! function_exists('layout_search'))
{
	function layout_search($search_term = '') {
		$ci   =& get_instance();
		$data = array(
					  'controller' => $ci->uri->segment(1),
					  'action' => $ci->uri->segment(2),
					  'first_param' => $ci->uri->segment(3),
					  'assoc_params' => array_map('urldecode',$ci->uri->uri_to_assoc(4)),
					  'search_term' => $search_term
					  );
		$search = $ci->load->view('layout/search', $data, true);
		return $search;
	}
}

if ( ! function_exists('layout_facebook') )
{
	function layout_facebook($faces=false, $width=140, $height=278, $border='white') {
	
		return '<div class="fb-like-box" data-href="https://www.facebook.com/pages/on-stagetv/489706887739978" data-width="'.$width.'" data-height="'.$height.'" data-show-faces="'.($faces?'true':'false').'" data-stream="false" data-border-color="'.$border.'" data-show-border="false" data-header="false"></div>';
		
	}
}

if ( ! function_exists('layout_social') )
{
	function layout_social($link='', $account='', $tagg='')
	{
	
		// Facebook "Like" button.
		$social_button = '<div class="fb-like" data-href="'.$link.'" data-send="false" data-layout="button_count" data-show-faces="false"></div>';
		
		// Twitter "Tweet" button.
		$social_button .= '<a href="https://twitter.com/share" class="twitter-share-button" data-via="'.$account.'" data-count="none" data-hashtags="'.$tagg.'">Tweet</a>
							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\'://platform.twitter.com/widgets.js\';fjs.parentNode.insertBefore(js,fjs);}}(document, \'script\', \'twitter-wjs\');</script>';
		
		return ( $social_button);	
	}
}

if ( ! function_exists('layout_get_subjects'))
{
	function layout_get_subjects($faq_name='how-it-works')
	{
		$ci=& get_instance();
		$ci->load->library('parser');

		$ci->load->model('Faq_model');		
		$ci->load->model('Faq_question_model');

		$html = '';
		
		$faq_model = new Faq_model;
		$faq_question_model = new Faq_question_model;
	
		if ($faq_model->faq_name_exists($faq_name)) {
			$faq_model->load_from_name($faq_name);
		
			$faq_questions = $faq_question_model->get_faq_questions($faq_model->id);
		
			$html = $ci->parser->parse('faq/subjects', array('faq_questions'=>$faq_questions, 'base_url'=>'/faq/questions/'.$faq_name), true);
		}
		
		return $html;
	}
}

if ( ! function_exists('layout_admin_header'))
{
	function layout_admin_header($menu_item = 'home', $additional_css = array(), $additional_js = array())
	{
		$ci=& get_instance();
		$ci->load->helper('html');
		$ci->load->helper('url');
		
		$header  = doctype('html5').PHP_EOL;
		$header .= '<html>'.PHP_EOL;
		
		$header .= '<head>'.PHP_EOL;
		$header .= '<meta name="viewport" content="width=1024, initial-scale=1">'.PHP_EOL;
		$header .= '<title>On-Stage.tv - Altijd op de eerste rij</title>';
		
		$header .= link_tag('css/main.css').PHP_EOL;

		$additional_css = layout_get_additional_css($additional_css);
		$additional_js  = layout_get_additional_js($additional_js);
		
		foreach($additional_css as $css)
		{
			if (substr($css, 0, 7) == 'https://')
			{
				$header .= link_tag($css).PHP_EOL;
			}
			else
			{
				$header .= link_tag('css/'.$css.'.css').PHP_EOL;
			}
		}
		
		$header .= '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>'.PHP_EOL;
		$header .= '<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>'.PHP_EOL;
		
		foreach($additional_js as $js)
		{
			if (substr($js, 0, 7) == 'https://')
			{
				$header .= '<script type="text/javascript" src="'.$js.'"></script>'.PHP_EOL;
			}
			else
			{
				$header .= '<script type="text/javascript" src="'.base_url('js/'.$js.'.js').'"></script>'.PHP_EOL;
			}
		}
    
		$header .= '<script type="text/javascript" src="'.base_url('js/main.js').'"></script>'.PHP_EOL;
		
		$header .= '</head>'.PHP_EOL;
		
		$header .= '<body>'.PHP_EOL;
		$header .= $ci->parser->parse('administration/layout/header', array('current_menu'=>$menu_item), true);
		return $header;
	}
}

if ( ! function_exists('layout_admin_footer'))
{
	function layout_admin_footer()
	{
		$ci=& get_instance();
		$header = $ci->load->view('administration/layout/footer', array(), true);
		return $header;
	}
}