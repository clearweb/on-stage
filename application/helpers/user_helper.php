<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('user_logged_in'))
{
	function user_logged_in()
	{
			$ci=& get_instance();
			$ci->load->library('session');
			return $ci->session->userdata('logged_in');
	}
}

if ( ! function_exists('user_login'))
{
	function user_login($username = '', $password = '')
	{
		$ci=& get_instance();
		$ci->load->model('User_model');
		
		if (
			$ci->User_model->authenticate($username, $password) && 
			$ci->User_model->is_active($username) == TRUE
			)
		{
			user_login_without_authentication($username);
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}

if ( ! function_exists('user_login_without_authentication'))
{
	function user_login_without_authentication($username)
	{
		$ci=& get_instance();
		$ci->load->library('session');
		
		$ci->session->set_userdata( array(
										  'logged_in'=>'true',
										  'username'=>$username
										  ) );
	}
}

if ( ! function_exists('user_logged_in_username'))
{
	function user_logged_in_username()
	{
		$ci=& get_instance();
		$ci->load->library('session');
		$username = $ci->session->userdata('username');
		return $username;
	}
}

if ( ! function_exists('user_require'))
{
	function user_require()
	{
		if (!user_logged_in()) {
			$ci=& get_instance();
			$ci->load->library('session');
			$ci->load->helper('url');
			$ci->session->set_userdata('current_url', current_url());
			redirect('/user/login/', 'location');
			//redirect('/user/login/'.$ci->session->userdata('current_url'), 'location');
		}
	}
}

if ( ! function_exists('user_logout'))
{
	function user_logout()
	{
		$ci=& get_instance();
		$ci->load->library('session');
		$ci->session->unset_userdata('logged_in');
		$ci->session->unset_userdata('username');
	}
}