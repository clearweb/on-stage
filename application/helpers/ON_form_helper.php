<?php
// single upload!
function form_jquery_upload($name = '', $url = '', $value = '') {

$html = '
<div id="fileupload-'.$name.'" class="fileupload">
        <div class="fileupload-buttonbar">
        <input type="hidden" name="'.$name.'" />
        <input type="file" name="'.$name.'_jquery_upload" />
        </div>
	<div class="fileupload-content">
        <table class="files"></table>
        <div class="fileupload-progressbar"></div>
	<div class="progress">
		<div class="bar"></div>
	</div>
    </div>
</div>

<script src="/js/blueimp/js/vendor/jquery.ui.widget.js"></script>
<script src="/js/blueimp/js/jquery.iframe-transport.js"></script>
<script src="/js/blueimp/js/jquery.fileupload.js"></script>
';

$html .= "
<script>
	$('.fileupload').fileupload({
	    dataType: 'json',
	    url: '".$url."',
	    done: function (e, data) {
		if (typeof(data.result.".$name."_jquery_upload) != 'undefined') {
			$.each(data.result.".$name."_jquery_upload, function (index, file) {
          		$(\"input[name='".$name."']\").val(file.name);
            });
        } else { alert(JSON.stringify(data.result)); }
	    },
	    progressall: function (e, data) {
		var progress = parseInt(data.loaded / data.total * 100, 10);
		$(e.target).find('.progress .bar').css(
		    'width',
		    progress + '%'
		);
	    }
	});
</script>
";
    return $html;
}

function form_datepicker($name, $value='')
{
	$html = form_input($name, $value);
	$html .= '<script type="text/javascript">';
	$html .= "jQuery.datepicker.regional['nl'] = {
    closeText: 'Sluiten',
    prevText: '<',
    nextText: '>',
    currentText: 'Vandaag',
    monthNames: ['januari', 'februari', 'maart', 'april', 'mei', 'juni',
 'juli', 'augustus', 'september', 'oktober', 'november', 'december'],
    monthNamesShort: ['jan', 'feb', 'maa', 'apr', 'mei', 'jun',
      'jul', 'aug', 'sep', 'okt', 'nov', 'dec'],
    dayNames: ['zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag'],
    dayNamesShort: ['zon', 'maa', 'din', 'woe', 'don', 'vri', 'zat'],
    dayNamesMin: ['zo', 'ma', 'di', 'wo', 'do', 'vr', 'za'],
    weekHeader: 'Wk',
    dateFormat: 'dd-mm-yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};

    jQuery.datepicker.setDefaults( jQuery.datepicker.regional[ 'nl' ] );
";
	$html .= '$(\'input[name="'.$name.'"]\').datepicker();';
	$html .= '</script>';
	return $html;
}

?>