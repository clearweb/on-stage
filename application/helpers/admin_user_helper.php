<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('admin_user_logged_in'))
{
	function admin_user_logged_in()
	{
			$ci=& get_instance();
			$ci->load->library('session');
			return $ci->session->userdata('admin_logged_in');
	}
}


if ( ! function_exists('admin_user_require'))
{
	function admin_user_require()
	{
		if (!admin_user_logged_in()) {
			$ci=& get_instance();
			$ci->load->helper('url');
			redirect('/administration/login/', 'location');
		}
	}
}

if ( ! function_exists('admin_user_login'))
{
	function admin_user_login($username = '', $password = '')
	{
		$ci=& get_instance();
		$ci->load->library('session');
		$ci->load->model('Admin_user_model');
		if ($ci->Admin_user_model->authenticate($username, $password))
		{
			$ci->session->set_userdata( array('admin_logged_in'=>'true') );
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}

if ( ! function_exists('admin_user_logout'))
{
	function admin_user_logout()
	{
		$ci=& get_instance();
		$ci->load->library('session');
		$ci->session->unset_userdata('admin_logged_in');
	}
}