<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('country_filter_get_countries'))
{
	function country_filter_get_countries()
	{
		$ci=& get_instance();
		$ci->load->database();
		return $ci->db->get('country')->result_array();
	}
}

if ( ! function_exists('country_filter_get_ip_country'))
{
	function country_filter_get_ip_country($ip)
	{
		$ci=& get_instance();
		$ci->load->database();
		
		$ip_number = ip2long($ip);
		
		$row = $ci->db->from('country_ip_range')
			->where('ipfrom <', $ip_number)
			->where('ipto   >', $ip_number)
			->get()->row_array();
		
		return $row['code2'];
	}
}

