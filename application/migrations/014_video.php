<?php defined("BASEPATH") or exit("No direct script access allowed");

class Migration_Video extends CI_Migration {
  public function up() {
    $this->dbforge->add_field('id');
    $this->dbforge->add_field(array(
									'performance' => array(
														   'type' => 'INT',
														   'null' => TRUE,
														   ),
									'video_id' => array(
													'type' => 'INT',
													'null' => FALSE,
													),
									'thumbnail_url' => array(
														'type' => 'VARCHAR',
														'constraint' => 255,
														'null' => TRUE,
														),
									'sd_url' => array(
														'type' => 'VARCHAR',
														'constraint' => 255,
														'null' => TRUE,
														),
									'hd_url' => array(
														'type' => 'VARCHAR',
														'constraint' => 255,
														'null' => TRUE,
														),
									));
    $this->dbforge->create_table('video');
    
  }
  
  public function down() {
    $this->dbforge->drop_table('video');
  }
  
}