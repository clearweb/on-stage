<?php defined("BASEPATH") or exit("No direct script access allowed");

class Migration_Newsletter extends CI_Migration {
	function up() {
		$this->dbforge->add_field('id');
		$this->dbforge->add_field(array(
										'userid' => array(
														'type' => 'VARCHAR',
														'constraint' => '255',
														'null' => FALSE,
														),
										'email' => array(
														 'type' => 'VARCHAR',
														 'constraint' => '255',
														 'null' => FALSE,
														 ),
										'double_opt_in' => array(
														 'type' => 'INTEGER',
														 'constraint' => '1',
														 'null' => TRUE,
														 ),
										));
		$this->dbforge->create_table('newsletter');
	}

	function down() {
		$this->dbforge->drop_table('newsletter');
	}
}