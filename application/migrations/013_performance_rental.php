<?php defined("BASEPATH") or exit("No direct script access allowed");

class Migration_Performance_rental extends CI_Migration {
  public function up() {
    $this->dbforge->add_field('id');
    $this->dbforge->add_field(array(
									'performance' => array(
														   'type' => 'INT',
														   'null' => FALSE,
														   ),
									'user' => array(
													'type' => 'INT',
													'null' => FALSE,
													),
									'payment_datetime' => array(
														'type' => 'DATETIME',
														'null' => TRUE,
														),
									'expiry_datetime' => array(
														'type' => 'DATETIME',
														'null' => TRUE,
														),
									));
    $this->dbforge->create_table('performance_rental');
    
  }
  
  public function down() {
    $this->dbforge->drop_table('performance_rental');
  }
  
}