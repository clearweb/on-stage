<?php defined("BASEPATH") or exit("No direct script access allowed");

class Migration_Ip_Filter extends CI_Migration {
	function up() {
		$this->dbforge->add_field(array(
										'ipfrom' => array(
														'type' => 'VARCHAR',
														'constraint' => '255',
														'null' => FALSE,
														),
										'ipto' => array(
														 'type' => 'VARCHAR',
														 'constraint' => '255',
														 'null' => FALSE,
														 ),
										'whois' => array(
														 'type' => 'VARCHAR',
														 'constraint' => '255',
														 'null' => TRUE,
														 ),
										'assigned' => array(
														 'type' => 'VARCHAR',
														 'constraint' => '255',
														 'null' => TRUE,
														 ),
										'code2' => array(
														 'type' => 'VARCHAR',
														 'constraint' => '255',
														 'null' => FALSE,
														 ),
										'code3' => array(
														 'type' => 'VARCHAR',
														 'constraint' => '255',
														 'null' => TRUE,
														 ),
										'name' => array(
														 'type' => 'VARCHAR',
														 'constraint' => '255',
														 'null' => TRUE,
														 ),
										));
		$this->dbforge->create_table('country_ip_range');
	}

	function down() {
		$this->dbforge->drop_table('country_ip_range');
	}
}