<?php defined("BASEPATH") or exit("No direct script access allowed");

class Migration_Performance extends CI_Migration {
	public function up()
	{
		$this->dbforge->add_column('performance',  array (
													'title_color' => array(
																		   'type' => 'VARCHAR',
																		   'constraint' => '7',
																		   'null' => FALSE,
																		   )
													)
								   );
		$this->dbforge->add_column('performance',  array (
														  'image_url' => array(
																			   'type' => 'VARCHAR',
																			   'constraint' => '255',
																			   'null' => TRUE,
																			   )
														  )
								   );
		$this->dbforge->add_column('performance',  array (
														  'thumbnail_url' => array(
																				 'type' => 'VARCHAR',
																				 'constraint' => '255',
																				 'null' => TRUE,
																				 )
													)
								   );
		
		$this->dbforge->add_column('performance',  array (
														  'video_id' => array(
																			  'type' => 'INT',
																			  'constraint' => '11',
																			  'null' => TRUE,
																			  )
														  )
								   );
		$this->dbforge->add_column('performance',  array (
														  'video_url' => array(
																			   'type' => 'VARCHAR',
																			   'constraint' => '255',
																			   'null' => TRUE,
																			   )
														  )
								   );
	}

	public function down()
	{
		$this->dbforge->drop_column('performance', 'title_color');
		$this->dbforge->drop_column('performance', 'image_url');
		$this->dbforge->drop_column('performance', 'thumbnail_url');
		$this->dbforge->drop_column('performance', 'video_id');
		$this->dbforge->drop_column('performance', 'video_url');
	}
}