<?php defined("BASEPATH") or exit("No direct script access allowed");

class Migration_Newsletter extends CI_Migration {
	function up() {
		$this->dbforge->drop_column('newsletter', 'double_opt_in' );
	}
}