<?php defined("BASEPATH") or exit("No direct script access allowed");

class Migration_Performance extends CI_Migration {
	function up() {
		$this->dbforge->drop_column('performance', 'video_id');
		$this->dbforge->drop_column('performance', 'video_url');
		
		$this->dbforge->add_column('performance',  array (
														  'available_from' => array(
																			  'type' => 'DATE',
																			  'null' => FALSE,
																			  )
														  )
								   );

		$this->dbforge->add_column('performance',  array (
														  'rental_price' => array(
																			  'type' => 'FLOAT',
																			  'null' => FALSE,
																			  )
														  )
								   );
		
		$this->dbforge->add_column('performance', array(
														'rental_period' => array(
																			   'type' => 'INT',
																			   'constraints' => 11,
																			   'null' => FALSE,
																			   )
														)
								   );
		
	}
	
	function down() {
		$this->dbforge->add_column('performance',  array (
														  'video_id' => array(
																			  'type' => 'INT',
																			  'constraint' => '11',
																			  'null' => TRUE,
																			  )
														  )
								   );
		
		$this->dbforge->add_column('performance',  array (
														  'video_url' => array(
																			   'type' => 'VARCHAR',
																			   'constraint' => '255',
																			   'null' => TRUE,
																			   )
														  )
								   );

		$this->dbforge->drop_column('performance', 'available_from');
		$this->dbforge->drop_column('performance', 'rental_price');
		$this->dbforge->drop_column('performance', 'rental_period');
		
	}
}