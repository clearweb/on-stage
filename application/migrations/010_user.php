<?php defined("BASEPATH") or exit("No direct script access allowed");

class Migration_User extends CI_Migration {
	
  public function up() {
	  $this->dbforge->add_column('user', array(
											   'forgot_password_key_expire' => array(
																					 'type' => 'DATETIME',
																					 'null' => TRUE
																			  )
											   )
								 );
  }
  
  public function down() {
	  $this->dbforge->drop_column('user', 'forgot_password_key_expire');
  }
  
}