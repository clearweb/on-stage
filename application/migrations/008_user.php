<?php defined("BASEPATH") or exit("No direct script access allowed");

class Migration_User extends CI_Migration {
	
  public function up() {
	  $this->dbforge->add_field('id');
	  $this->dbforge->add_field(array(
									  'username' => array(
														  'type' => 'VARCHAR',
														  'constraint' => '255',
														  'null' => FALSE,
														  ),
									  'password' => array(
														  'type' => 'VARCHAR',
														  'constraint' => '255',
														  'null' => TRUE,
														  ),
									  'active' => array(
														'type' => 'TINYINT',
														'null' => FALSE
														)
									  ));
	  $this->dbforge->create_table('user');
  }
  
  public function down() {
	  $this->dbforge->drop_table('user');
  }
  
}