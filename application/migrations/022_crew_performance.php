<?php defined("BASEPATH") or exit("No direct script access allowed");

class Migration_Crew_Performance extends CI_Migration {
	function up() {
		$this->dbforge->add_column('performance',  array (
														  'crew' => array(
																			  'type' => 'VARCHAR',
																			  'constraint' => '255',
																			  'null' => TRUE,
																			  )
														  ));
	}
	
	function down() {

		$this->dbforge->drop_column('performance', 'crew');
		
	}
}