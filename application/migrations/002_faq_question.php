<?php defined("BASEPATH") or exit("No direct script access allowed");

class Migration_Faq_Question extends CI_Migration {
  public function up() {
    $this->dbforge->add_field('id');
    $this->dbforge->add_field(array(
				    'faq_id' => array(
						      'type' => 'INT',
						      'constraint' => '9',
						      ),
				    'title' => array(
						    'type' => 'VARCHAR',
						    'constraint' => '255',
						    'null' => FALSE,
						    ),
				    'description' => array(
							   'type' => 'TEXT',
							   'null' => TRUE,
							   ),
				    ));
    $this->dbforge->create_table('faq_question');
    
  }
  
  public function down() {
    $this->dbforge->drop_table('faq_question');
  }
  
}