<?php defined("BASEPATH") or exit("No direct script access allowed");

class Migration_Country extends CI_Migration {
	function up() {
		$this->dbforge->add_field(array(
										'name' => array(
														'type' => 'VARCHAR',
														'constraint' => '255',
														'null' => FALSE,
														),
										'code2' => array(
														 'type' => 'VARCHAR',
														 'constraint' => '255',
														 'null' => FALSE,
														 ),
										'code3' => array(
														 'type' => 'VARCHAR',
														 'constraint' => '255',
														 'null' => TRUE,
														 ),
										'whois' => array(
														 'type' => 'VARCHAR',
														 'constraint' => '255',
														 'null' => TRUE,
														 ),
										));
		$this->dbforge->create_table('country');
	}

	function down() {
		$this->dbforge->drop_table('country');
	}
}