<?php defined("BASEPATH") or exit("No direct script access allowed");

class Migration_Performance extends CI_Migration {
	public function up()
	{
		$this->dbforge->add_field('id');
		$this->dbforge->add_field(array(
										'title' => array(
														'type' => 'VARCHAR',
														'constraint' => '255',
														'null' => FALSE,
														),
										'performer' => array(
														'type' => 'VARCHAR',
														'constraint' => '255',
														'null' => FALSE,
														),
										'summary' => array(
														'type' => 'VARCHAR',
														'constraint' => '255',
														'null' => FALSE,
														),
										'description' => array(
														   'type' => 'TEXT',
														   'null' => FALSE,
														   ),
										'genre' => array(
														   'type' => 'VARCHAR',
														   'constraint' => '255',
														   'null' => FALSE,
														   ),
										'duration' => array(
														 'type' => 'INT',
														 'constraint' => '5',
														 'null' => FALSE,
														 ),
										'year' => array(
														 'type' => 'INT',
														 'constraint' => '4',
														 'null' => FALSE,
														 ),
										'regission' => array(
														   'type' => 'VARCHAR',
														   'constraint' => '255',
														   'null' => FALSE,
														   ),
										'production' => array(
														 'type' => 'VARCHAR',
														 'constraint' => '255',
														 'null' => FALSE,
														 ),
										'texts' => array(
														 'type' => 'VARCHAR',
														 'constraint' => '255',
														 'null' => FALSE,
														 ),
										'music' => array(
														 'type' => 'VARCHAR',
														 'constraint' => '255',
														 'null' => FALSE,
														 ),
										));
		$this->dbforge->create_table('performance');
		
	}
	
	public function down()
	{
		$this->dbforge->drop_table('performance');
	}
	
}