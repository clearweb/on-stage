<?php defined("BASEPATH") or exit("No direct script access allowed");

class Migration_Rental_Payment extends CI_Migration {
	function up() {
		$this->dbforge->add_column('performance_rental',  array (
														  'payment' => array(
																			  'type' => 'FLOAT',
																			  'null' => FALSE
																			  )
														  ));
	}
	
	function down() {

		$this->dbforge->drop_column('performance_rental', 'payment');
		
	}
}