<?php defined("BASEPATH") or exit("No direct script access allowed");

class Migration_Text extends CI_Migration {
	public function up()
	{
		$this->dbforge->add_field('id');
		$this->dbforge->add_field(array(
										'page' => array(
														'type' => 'VARCHAR',
														'constraint' => '255',
														'null' => FALSE,
														),
										'page_url' => array(
														'type' => 'VARCHAR',
														'constraint' => '255',
														'null' => FALSE,
														),
										'position' => array(
														   'type' => 'VARCHAR',
														   'constraint' => '255',
														   'null' => FALSE,
														   ),
										'title' => array(
														 'type' => 'VARCHAR',
														 'constraint' => '255',
														 'null' => FALSE,
														 ),
										'text' => array(
														'type' => 'TEXT',
														'null' => TRUE,
														),
										));
		$this->dbforge->create_table('text');
		
	}
	
	public function down()
	{
		$this->dbforge->drop_table('text');
	}
	
}