<?php defined("BASEPATH") or exit("No direct script access allowed");

class Migration_Performance_view extends CI_Migration {
  public function up() {
    $this->dbforge->add_field('id');
    $this->dbforge->add_field(array(
									'performance' => array(
														   'type' => 'INT',
														   'null' => FALSE,
														   ),
									'user' => array(
													'type' => 'INT',
													'null' => FALSE,
													),
									'datetime' => array(
														'type' => 'DATETIME',
														'null' => FALSE,
														),
									));
    $this->dbforge->create_table('performance_view');
    
  }
  
  public function down() {
    $this->dbforge->drop_table('performance_view');
  }
  
}