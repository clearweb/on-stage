<?php defined("BASEPATH") or exit("No direct script access allowed");

class Migration_Faq extends CI_Migration {
  public function up() {
    $this->dbforge->add_column('faq', array(
					    'name' => array(
							   'type' => 'VARCHAR',
							   'constraint' => '255'
							   )
					    )
			       );
  }
  
  public function down() {
    $this->dbforge->drop_column('faq', 'name');
  }
  
}