<?php defined("BASEPATH") or exit("No direct script access allowed");

class Migration_Video extends CI_Migration {
  public function up() {
	  $this->dbforge->drop_column('video', 'sd_url');
	  $this->dbforge->drop_column('video', 'hd_url');
	  
	  $this->dbforge->add_column('video', array( 'type' => array(
																 'type' => 'ENUM',
																 'constraint' => "'free', 'paid'",
																 'null' => TRUE,
																 )
												 )
								 );
  }

  public function down() {
	  $this->dbforge->add_column('video', array( 'sd_url' => array(
														'type' => 'VARCHAR',
														'constraint' => 255,
														'null' => TRUE,
														)
												 )
								 );
	  
	  $this->dbforge->add_column('video', array( 'hd_url' => array(
														'type' => 'VARCHAR',
														'constraint' => 255,
														'null' => TRUE,
														)
												 )
								 );

	  $this->dbforge->drop_column('video', 'type');
  }
}