<?php defined("BASEPATH") or exit("No direct script access allowed");

class Migration_User extends CI_Migration {
	
  public function up() {
	  $this->dbforge->add_column('user', array(
											   'double_opt_in' => array(
																		'type' => 'TINYINT',
																		'null' => FALSE
																		)
											   )
								 );
  }
  
  public function down() {
	  $this->dbforge->drop_column('user', 'double_opt_in');
  }
  
}