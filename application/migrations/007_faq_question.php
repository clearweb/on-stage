<?php defined("BASEPATH") or exit("No direct script access allowed");

class Migration_Faq_Question extends CI_Migration {
	
  public function up() {
	  $this->dbforge->add_column('faq_question', array(
													   'category' => array(
																		   'type' => 'VARCHAR',
																		   'constraint' => '255'
																		   )
													   )
								 );
	  $this->dbforge->add_column('faq_question', array(
													   'rank' => array(
																	   'type' => 'INT',
																	   'constraint' => '11'
																	   )
													   )
								 );
  }
  
  public function down() {
	  $this->dbforge->drop_column('faq_question', 'category');
	  $this->dbforge->drop_column('faq_question', 'rank');
  }
  
}