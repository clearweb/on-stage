<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Performance_rental extends Crud_model
{
	var $sql_table   = 'performance_rental';
	
	/*
	 * standard rental_period
	 */
	private $rental_period = 48;
	
	protected $allowed_properties = array(
										  'performance',
										  'user',
										  'payment_datetime',
										  'expiry_datetime',
										  'payment'
										  );
	
	
	function set_rental_period($period)
	{
		if ( ! empty($period))
		{
			$this->rental_period = $period;
		}
	}
	
	function user_has_right_for_performance($user_id, $performance_id)
	{
		$this->load->database();

		$has_right = FALSE;
		
		$this->db->from($this->sql_table)
			->where('user', $user_id)
			->where('performance', $performance_id)
			->order_by('expiry_datetime', 'desc');
		
		$credit = $this->db->get()->row_array();
		
		if (isset($credit['expiry_datetime']) && strtotime($credit['expiry_datetime']) > time())
			$has_right = TRUE;
		
		return $has_right;
	}

	/**
	 * The user's rental for the performance is stopped immediately 
	 */
	function stop_user_performance_rental($user_id, $performance_id)
	{
		$this->load->database();
		
		$this->db->from($this->sql_table)
			->where('user', $user_id)
			->where('performance', $performance_id)
			->order_by('expiry_datetime', 'desc');
		
		$credit = $this->db->get()->row_array();
		
		if (isset($credit['expiry_datetime']) && strtotime($credit['expiry_datetime']) > time())
		{
			$rental = new Performance_rental;
			$rental->load($credit['id']);
			$rental->setProperty('expiry_datetime', date('Y-m-d H:i:s'));
			$rental->save_to_db();
		}
		
	}
	
	function get_credit_performances($user_id) {
		$this->load->database();
		
		$this->db->from($this->sql_table)
			->where('user', $user_id)
			->where('expiry_datetime >', date('Y-m-d H:i:s'))
			->join('performance', 'performance=performance.id')
			->order_by('payment_datetime', 'desc');
		
		$credits = $this->db->get()->result_array();
		foreach($credits as $i => $credit) {
			$credits[$i]['hours_left'] = (strtotime($credit['expiry_datetime']) - time()) / 3600;
		}
		
		return $credits;
	}

	function save_to_db() {
		$expiry_timestamp = time() + ($this->rental_period * 3600);
		
		$this->setProperty('payment_datetime', date('Y-m-d H:i:s'));
		$this->setProperty('expiry_datetime', date('Y-m-d H:i:s', $expiry_timestamp));
		
		parent::save_to_db();
	}
	
	function get_sum($filters=NULL)
	{			
	
		$this->db->from($this->sql_table);
		
		if( ! empty($filters) ){
			foreach( $filters as $key => $value )
			{
				$this->db->where($key, $value);
			}
		}
 		
		$this->db->select_sum('payment');
		
		$result = $this->db->get()->row_array();

		return $result; 
	}
}