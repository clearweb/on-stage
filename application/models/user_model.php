<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CRUD_model
{
	var $sql_table = 'user';
	protected $allowed_properties = array('password', 'username', 'active', 'forgot_password_key', 'forgot_password_key_expire');
	
	public static function authenticate($username = '', $password = '')
	{
		$user = new User_model;
		$user->load_from_username($username);
		return ($user->getProperty('password') == self::encrypt_password($password));
	}  
	
	public static function is_active($username)
	{
		$user = new User_model;
		$user->load_from_username($username);
		return ($user->getProperty('active'));
	}
	
	private function encrypt_password($password)
	{
		return md5('0y7u3 907h342rbhyor4egbcfwe '.$password.'asdhgasdysdgf74ywh9fj9382');
	}
	
	function load_from_username($username = '')
	{
		$this->load->database();
		$this->db->from($this->sql_table)->where('username', $username);
		$row = $this->db->get()->row_array();
		$this->setProperties($row);
		if (isset($row['id']))
			$this->id = $row['id'];
		$this->loaded = TRUE;
	}
	
	private function remove_user($username = '')
	{
		
		$this->delete();
	}

	public function setProperty($key, $value)
	{
		if ($key == 'password')
		{
			$value = $this->encrypt_password($value);
		}
		parent::setProperty($key, $value);
	}

	public function get_activation_key()
	{
		return md5($this->getProperty('password').$this->getProperty('username'));
	}
	
	public function activate($key) {
		if ( ! $this->getProperty('active', FALSE) && $key == $this->get_activation_key())
		{
			$this->setProperty('active', TRUE);
			$this->save_to_db();
			return true;
		}
		return false;
	}
	
	public function check_for_email($email)
	{
		$found_email = FALSE;
	
		$email_lookup = $this->db->select('*')
		->from('newsletter')
		->where('email', $email)
		->limit(1)
		->get();
		
		if( ! empty($email_lookup) ){
			$found_email = TRUE;
		}
		
		return $found_email;
	}

	public function send_activation_mail()
	{
		$this->load->library('email');
		$this->load->helper('url');
		
		$activation_url = site_url('user/activate/'.urlencode($this->getProperty('username')).'/'.$this->get_activation_key());
		
		$config['mailtype'] = 'html';
		
		$this->email->initialize($config);
		
		$this->email->from('no-reply@on-stage.tv', 'On-stage.tv');
		
		$this->email->to($this->getProperty('username'));
		
		$this->email->subject('Uw account activeren (On-stage.tv)');
		$this->email->message('Beste klant,<br /><br />
U hebt zojuist een account aangemaakt voor <a href="http://on-stage.tv">On-stage.tv</a>.<br />
U moet echter u account activeren. Dit kan door de onderstaande link te volgen.<br />
<a href="'.$activation_url.'">'.$activation_url.'</a><br />
Bij de volgende keer kunt u uw opgegeven wachtwoord gebruiken om in te loggen.
<br /><br />
Met vriendelijke groet,
<br /><br />
Het on-stage.tv team.
');
		
		$this->email->send();
	}
	
	function set_password_reset()
	{
		$key = uniqid();
		$this->setProperty('forgot_password_key', $key);
		$this->setProperty('forgot_password_key_expire', date('Y-m-d H:i:s', strtotime('next week')));
		$this->save_to_db();
		return true;		
	}

	function send_password_reset_mail()
	{
		$this->load->library('email');
		$this->load->helper('url');
		
		$reset_url = site_url('user/change_password/'.urlencode($this->getProperty('username')).'/'.$this->getProperty('forgot_password_key'));
		
		$config['mailtype'] = 'html';
		
		$this->email->initialize($config);
		
		$this->email->from('no-reply@on-stage.tv', 'On-stage.tv');
		
		$this->email->to($this->getProperty('username'));
		
		$this->email->subject('Uw wachtwoord herstellen (On-stage.tv)');
		$this->email->message('Beste klant,<br /><br />
				U hebt zojuist een aanvraag gedaan om uw wachtwoord voor <a href="http://on-stage.tv">On-stage.tv</a> opnieuw in te stellen.<br />
				Dit kan door de onderstaande link te volgen.<br />
				Let op dat deze link een beperkte geldigheidsdatum heeft.
				<a href="'.$reset_url.'">'.$reset_url.'</a>
				<br /><br />
				Met vriendelijke groet,
				<br /><br />
				Het on-stage.tv team.
		');
						
		$this->email->send();
	}
	
	public function send_password_change_mail()
	{
		$this->load->library('email');
		$this->load->helper('url');
		
		$reset_url = site_url('user/change_password/'.urlencode($this->getProperty('username')).'/'.$this->getProperty('forgot_password_key'));
		
		$config['mailtype'] = 'html';
		
		$this->email->initialize($config);
		
		$this->email->from('no-reply@on-stage.tv', 'On-stage.tv');
		
		$this->email->to($this->getProperty('username'));
		
		$this->email->subject('Uw wachtwoord gewijzigd (On-stage.tv)');
		$this->email->message('Beste klant,<br /><br />
U hebt zojuist uw wachtwoord voor <a href="http://on-stage.tv">On-stage.tv</a> gewijzigd.<br />
Heeft u dit niet gedaan maar toch deze mail otnvangen, dan raden wij u aan direct uw wachtwoord opnieuw in te stellen.<br />
Dit kan door de onderstaande link te volgen.<br />
<a href="'.$reset_url.'">'.$reset_url.'</a>
<br /><br />
Met vriendelijke groet,
<br /><br />
Het on-stage.tv team.
');
				
		$this->email->send();
	}
	
}