<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class performance_model extends Crud_model
{
	var $sql_table = 'performance';
	protected $allowed_properties = array(
										  'title',
										  'performer',
										  'crew',
										  'summary',
										  'description',
										  'genre',
										  'duration',
										  'year',
										  'regission',
										  'production',
										  'texts',
										  'music',
										  'title_color',
										  'image_url',
										  'thumbnail_url',
										  'available_from',
										  'rental_price',
										  'rental_period',
										  'filter_countries'
										  );

	// standard returns only performances which are available!
	protected $return_only_available = true;
	
	protected $search_key = '';
	
	function load($id)
	{
		parent::load($id);
		
		$countries = unserialize($this->getProperty('filter_countries'));
		
		if (is_array($countries))
		{
			$this->setProperty('filter_countries', $countries);
		}
		else
		{
			$this->setProperty('filter_countries', array());
		}
		
		$this->init_cached_properties();
	}
	
	public function init_cached_properties() {
		$thumbnail_url = $this->getProperty('thumbnail_url', NULL);
		if (empty($thumbnail_url) && $this->loaded)
		{
			$this->update_cached_properties();
		}
	}
	
	public function update_cached_properties() {
		$video = $this->get_paid_video();
		if ( ! empty($video) )
		{
			$video->load();
			$this->setProperty('thumbnail_url', $video->getProperty('thumbnail_url'));
			
			$this->save_to_db();
		}
	}

	function save_to_db() {
		$countries = serialize($this->getProperty('filter_countries'));
		$this->setProperty('filter_countries', $countries);
		parent::save_to_db();
		$this->setProperty('filter_countries', unserialize($countries));
	}
	
	function set_search_key($search_key = '')
	{
		$this->search_key = urldecode($search_key);
	}
	
	function get_all($items_per_page = 0, $page = 1, $sort_column = 'id', $sort_type = 'asc')
	{
		if ( ! empty($this->search_key))
		{
			$this->db->where("(`title` LIKE '%{$this->search_key}%' OR `summary` LIKE '%{$this->search_key}%' OR `description` LIKE '%{$this->search_key}%')");
		}
		
		if ($this->return_only_available)
		{
			$this->set_restriction('available_from', date('Y-m-d H:i:s'), '<');
		}
		
		return parent::get_all($items_per_page, $page, $sort_column, $sort_type);
	}
	
	function get_num_matches($field, $key, $table)
	{
		$this->db->select();
		$this->db->from($table);
		$this->db->where($field, $key);
		$result = $this->db->count_all_results();
		
		return $result;
	}

	function get_all_in(array $in_array)
	{
		$results = array();
		if ( ! empty($in_array)) {
			$this->load->database();
			$this->db->from($this->sql_table);
			$this->db->where_in('id', $in_array);
			$results = $this->db->get()->result_array();
		}
		return $results;
	}

	function get_count()
	{
		if ( ! empty($this->search_key))
		{
			$this->db->where("(`title` LIKE '%{$this->search_key}%' OR `summary` LIKE '%{$this->search_key}%' OR `description` LIKE '%{$this->search_key}%')");
		}
		
		return parent::get_count();
	}
	
	function get_new() {
		return $this->get_all('6','','available_from', 'desc');
	}
	
	function get_popular() {
	
		$this->load->database();
		
		$this->db->select('*')
		->from('performance')
		->where('available_from  <=', date('Y-m-d H:i:s'))
		->join('performance_view', 'performance_view.id = performance.id')
		->order_by('performance_view.performance', 'desc')
		->limit('6');
		
		$query = $this->db->get();
			
		return $query->result_array();
	}
	
	function set_return_only_available($modifier = true)
	{
		$this->return_only_available = $modifier;
	}

	function is_available()
	{
		return time() > strtotime($this->getProperty('available_from'));
	}

	function delete() {
		// TODO make it decouple the video's!
		parent::delete();
	}
	
	function get_free_video() {
		$ci =& get_instance();
		$ci->load->model('Video_model');
		$ci->Video_model->load_from_performance($this->get_id(), 'free');
		return $ci->Video_model;
	}
	
	function get_paid_video() {
		$ci =& get_instance();
		$ci->load->model('Video_model');
		$ci->Video_model->load_from_performance($this->get_id(), 'paid');
		return $ci->Video_model;
	}
	
	function set_video_api($video_api = NULL) {
		$this->video_api = $video_api;
	}

}