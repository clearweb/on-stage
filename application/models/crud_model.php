<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud_model extends CI_Model
{
	var $id                       = 0;
	var $sql_table                = '';
	var $loaded                   = FALSE;
	var $restrictions             = array();
	protected $properties         = array();
	protected $allowed_properties = array();
	
	function load($id = 0)
	{
		if ( ! empty($id) && $this->exists($id)) {
			$this->load->database();
			$this->db->from($this->sql_table)->where('id', $id);
			$this->setProperties($this->db->get()->row_array());
			$this->id = $id;
			$this->loaded = TRUE;
		}
	}
	
	function getProperty($key = '', $default = NULL)
	{
		return (isset($this->properties[$key]))?$this->properties[$key]: $default;
	}
	
	function getProperties()
	{
		$properties = array();
		
		foreach($this->allowed_properties as $key)
		{
			if (isset($this->properties[$key]))
			{
				$properties[$key] = $this->properties[$key];
			}
			else
			{
				$properties[$key] = null;
			}
		}
		
		return $properties;
	}
	
	function setProperty($key, $value)
	{
		if (in_array($key, $this->allowed_properties)) {
			$this->properties[$key] = $value;
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function get_id()
	{
		return $this->id;
	}

	function setProperties($properties)
	{
		$this->load->helper('array');
		$this->properties = array_merge($this->properties, elements($this->allowed_properties, $properties));
	}
	
	function exists($id)
	{
		$this->load->database();
		$this->db->from($this->sql_table)->where('id', $id);
		return (count($this->db->get()->row_array()) > 0);
	}

	/**
	 * Sets a restriction to the get_all function
	 */
	function set_restriction($property, $value, $operator = null)
	{
		$restriction = $property;
		if (in_array($property, $this->allowed_properties)) {
			if ( ! is_null($operator)) {
				$restriction .= ' '.$operator;
			}
			
			$this->restrictions[$restriction] = $value;
		}
	}
	
	function clear_restrictions()
	{
		$this->restrictions = array();
	}

	function get_distinct_column($column = '', $limit = 0, $sort_column = 'id', $sort_type = 'asc')
	{
		$this->load->database();
		$this->db->distinct();
		$this->db->select($column);
		$this->db->from($this->sql_table);

		foreach($this->restrictions as $key => $value)
		{
			$this->db->where($key, $value);
		}
		
		$this->db->order_by($sort_column, $sort_type);

		if ($limit)
			$this->db->limit($limit);

		$data = array();
		foreach($this->db->get()->result_array() as $row) {
			$data[] = element($column, $row);
		}
		return $data;
	}

	function get_all($items_per_page = 0, $page = 1, $sort_column = 'id', $sort_type = 'asc')
	{
		$this->load->database();
		
		$this->db->from($this->sql_table);

		foreach($this->restrictions as $key => $value)
		{
			$this->db->where($key, $value);
		}
		
		$this->db->order_by($sort_column, $sort_type);
		if ($items_per_page > 0 && $page > 0)
		{
			$this->db->limit($items_per_page, ($page - 1) * $items_per_page);
		}
		return $this->db->get()->result_array();
	}

	
	function get_count()
	{
		$this->load->database();
		$this->db->from($this->sql_table);

		foreach($this->restrictions as $key => $value)
		{
			$this->db->where($key, $value);
		}
		
		return $this->db->count_all_results();
	}

	function delete()
	{
		if ($this->loaded) {
			$this->load->database();
			$this->db->where('id', $this->id);
			$this->db->delete($this->sql_table);
		}
	}
	
	function save_to_db()
	{
		$this->load->helper('array');
		
		$data = elements( $this->allowed_properties, $this->properties );
		if ($this->exists($this->id)) {
			$this->db->where('id', $this->id)->update($this->sql_table, $data);
		} else {
			$this->db->insert($this->sql_table, $data);
			$this->id = $this->db->insert_id();
		}
	}
	
}