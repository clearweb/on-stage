<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq_model extends Crud_model
{
	var $id          = 0;
	var $title       = '';
    var $description = '';
    var $name        = '';
	
	function load_from_name($name = '')
	{
		$this->load->database();
		$this->db->from('faq')->where('name', $name);
		foreach($this->db->get()->row_array() as $key => $value)
		{
			$this->$key = $value;
		}
		$this->loaded = TRUE;
	}

	function load_from_id($id = 0)
	{
		$this->load->database();
		$this->db->from('faq')->where('id', $id);
		foreach($this->db->get()->row_array() as $key => $value)
		{
			$this->$key = $value;
		}
		$this->loaded = TRUE;
	}

	function faq_id_exists($faq_id) {
		$this->load->database();
		$this->db->from('faq')->where('id', $faq_id);
		return (count($this->db->get()->row_array()) > 0);
	}

	function faq_name_exists($faq_name) {
		$this->load->database();
		$this->db->from('faq')->where('name', $faq_name);
		return (count($this->db->get()->row_array()) > 0);
	}
	
	function get_faqs() {
		$this->load->database();
		$this->db->from('faq');
		return $this->db->get()->result_array();
	}

	function write_through() {
		$this->load->helper('array');
		
		$data = elements( array('title', 'name', 'description'), get_object_vars($this) );
		if ($this->faq_id_exists($this->id)) {
			$this->db->where('id', $this->id)->update('faq', $data);
		} else {
			$this->db->insert('faq', $data);
		}
	}
}