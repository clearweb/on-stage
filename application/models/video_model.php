<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Video_model extends Crud_model
{
	var $sql_table = 'video';
	protected $allowed_properties = array(
										  'thumbnail_url',
										  'video_id',
										  'type',
										  'performance'
										  );
	
	protected $video_api = NULL;
	
	function load($id = 0)
	{
		parent::load($id);
		
		$thumbnail_url = $this->getProperty('thumbnail_url', NULL);
		$video_id      = $this->getProperty('video_id', NULL);
		
		if (empty($thumbnail_url) && !empty($video_id))
		{
			if ($this->video_api->is_ready($video_id)) {
				$this->setProperty('thumbnail_url', $this->video_api->get_thumbnail_url($video_id));
				$this->save_to_db();
			}
		}
	}

	public function load_from_performance( $performance_id = 0, $type = 'free') {
		if ( ! empty($performance_id) )
		{
			$this->load->database();
			$this->db->from($this->sql_table)
				->where('performance', $performance_id)
				->where('type', $type);
			
			$row = $this->db->get()->row_array();

			if ( ! empty($row['id'])) {
				$this->id = $row['id'];			
				$this->setProperties($row);
				
				$this->loaded = TRUE;
			}
		}
	}
	
	function set_video_api($video_api = NULL)
	{
		$this->video_api = $video_api;
	}
	
	function get_video_html($width=679, $height=380)
	{
		return $this->video_api->get_video_html($this->getProperty('video_id'), $width, $height);
	}
	
	function change_video($video_path)
	{		
		$video_id = $this->video_api->upload_video($video_path, $this->getProperty('video_id', null));
		$this->setProperty('video_id', $video_id);
		$this->setProperty('thumbnail_url', '');
		$this->setProperty('sd_url', '');
		$this->setProperty('hd_url', '');
	}
}