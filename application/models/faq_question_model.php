<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq_question_model extends CI_Model
{
	var $id          = 0;
    var $faq_id      = 0;
	var $title       = '';
	var $category    = 'none';
    var $description = '';
	var $loaded      = FALSE;
	
	function load($id = 0)
	{
		$this->load->database();
		$this->db->from('faq_question')->where('id', $id);
		foreach($this->db->get()->row_array() as $key => $value)
		{
			$this->$key = $value;
		}
		$this->loaded = TRUE;
	}

	function id_exists($id) {
		$this->load->database();
		$this->db->from('faq_question')->where('id', $id);
		return (count($this->db->get()->row_array()) > 0);
	}

	function get_faq_questions($faq_id) {
		$this->load->database();
		$this->db->from('faq_question')->where('faq_id', $faq_id);
		$this->db->order_by('rank', 'asc');
		return $this->db->get()->result_array();
	}

	private function get_max_rank()
	{
		$this->load->database();
		$this->db->from('faq_question');
		$this->db->select_max('rank');
		$max_rank = $this->db->get()->row();
		return (empty($max_rank)) ? 1 : max($max_rank->rank, 1);
	}
	
	private function repair_ranks()
	{
		$this->load->database();
		$max_rank = $this->get_max_rank();
		
		$this->db->from('faq_question');
		$this->db->where('rank', 0);
		$rankless_rows = $this->db->get()->result_array();
		foreach($rankless_rows as $rankless_row)
		{
			$this->db->set('rank', $max_rank);
			$this->db->where('id', $rankless_row['id']);
			$this->db->update('faq_question');
			$max_rank++;
		}
	}

	function move($rank_change)
	{
		if ($this->loaded)
		{
			$this->repair_ranks();
			
			$max_rank = $this->get_max_rank();
			$destination_rank = max(min($max_rank, $this->rank + $rank_change), 1);
			
			$this->load->database();
			
			if ($rank_change < 0)
			{
				$sql = 'UPDATE `faq_question` SET `rank` = (`rank` + 1) WHERE rank <'.$this->rank.' AND rank >='.$destination_rank;
			}
			else
			{
				$sql = 'UPDATE `faq_question` SET `rank` = (`rank` - 1) WHERE rank >'.$this->rank.' AND rank <='.$destination_rank;
			}
			
			$query = $this->db->query($sql);
			
			$this->db->from('faq_question')->where('id', $this->id)->set('rank', $destination_rank);
			$this->db->update();
		}
	}
	
	function get_by_category($category)
	{
		$this->load->database();
		$this->db->from('faq_question')->where('category', $category);
		$this->db->order_by('rank', 'asc');
		return $this->db->get()->result_array();
	}
	
	function get_categories()
	{
		return array(
					 'none' => 'Niet in footer',
					 'about' => 'Over on-stage.tv',
					 'watch-and-pay' => 'Kijken en betalen',
					 'support' => 'Support',
					 'other' => 'Overig'
					 );
	}
	
	function write_through() {
		$this->load->database();
		$this->load->helper('array');
		
		$data = elements( array('faq_id', 'title', 'category', 'description'), get_object_vars($this) );
		if ($this->id_exists($this->id)) {
			$this->db->where('id', $this->id)->update('faq_question', $data);
		} else {
			$this->db->insert('faq_question', $data);
		}
	}

	function delete() {
		if ($this->loaded) {
			$this->load->database();
			$this->db->where('id', $this->id);
			$this->db->delete('faq_question');
		}
	}
}