<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Performance_view extends Crud_model
{
	var $sql_table = 'performance_view';
	protected $allowed_properties = array(
										  'performance',
										  'user',
										  'datetime'
										  );
	

	function get_related_performances($performance_id) {
		$user_ids       		 = array();
		$related_performance_ids = array();
		
		$this->load->database();
		
		$this->load->model('Performance_model');
		
		$users = $this->db->select('user')
			->from($this->sql_table)
			->where('performance', $performance_id)
			->get()
			->result_array();
		
		foreach($users as $user) 
		{
			$user_ids[] = $user['user'];
		}
		
		$related_performances = array();
		if( ! empty($user_ids)){
			$related_performances = $this->db->select('performance, COUNT(performance)', FALSE)
				->from($this->sql_table)
				->where_in('user', $user_ids)
				->group_by('performance')
				->order_by('COUNT(performance)', 'desc')
				->limit(5)
				->get()
				->result_array();
		}
		
		foreach($related_performances as $related_performance)
		{
			$related_performance_ids[] = $related_performance['performance'];
		}
		
		
		return $this->Performance_model->get_all_in($related_performance_ids);
	}
	
	function get_recent_performances($user_id, $page=1) {
		$performances = array();
		
		$performance_views = $this->get_distinct_column('performance', 5, 'datetime', 'desc');
		
		$this->load->model('Performance_model');
		
		$this->set_restriction('user', $user_id);
		

		foreach($performance_views as $view)
		{
			$performance = new Performance_model;
			$performance->load($view['performance']);
			
			$performance_array = $performance->getProperties();
			$performance_array['id'] = $performance->get_id();
			
			$performances[] = $performance_array;
		}
		
		return $performances;
	}
	
	function get_recent_performance_videos($user_id, $limit=5)
	{
		$recent_videos = $this->db->select('*')
		->from('performance')
		->where('user', $user_id)
		->join('performance_view', 'performance_view.id = performance.id')
		->order_by('performance_view.performance', 'desc')
		->limit($limit)
		->get();
		
		return $recent_videos->result_array();
	}
	
}