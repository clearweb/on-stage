<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Text_model extends Crud_model
{
	var $sql_table = 'text';
	protected $allowed_properties = array('page', 'page_url', 'position', 'title', 'text');
	
}