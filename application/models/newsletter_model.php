<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsletter_model extends Crud_model
{
	var $sql_table = 'newsletter';
	protected $allowed_properties = array('userid', 'email');
	
	public function get_all_emails()
	{
		$this->load->database();
	
		$newsletter_emails = $this->db->select('*')
		->from($this->sql_table)
		->get();
		
		//$user_email = $this->db->select('username')
		//->from('user')->get();
			
		return $newsletter_emails->result_array();
	}
	
	public function lookup_newsletter($email)
	{
		$newsletter_object = $this->db->select('email')
		->where('email', $email)
		->from($this->sql_table)
		->get();
		
		$found_newsletter_result = FALSE;
		$news_check = $newsletter_object->result_array();
		if( ! empty($news_check)){
			$found_newsletter_result = TRUE;
		}
		
		return $found_newsletter_result;
	}
	
	public function remove_newsletter($email)
	{
		$this->db->delete($this->sql_table, array('email' => $email)); 
	}
	
}