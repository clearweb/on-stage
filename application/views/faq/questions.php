<?php
$this->load->helper('layout');
echo layout_header($page_url);
?>

    <div class="wrapper">
      <?php echo layout_search(); ?>
	  <h1 class="page-title"><?php echo $faq_title; ?></h1>
    </div>
    <img id="top-image" src="/images/<?php echo $top_image; ?>-top.png" />
    
    <div id="main">
      <div id="main-content">


	<div id="faq-left">
	  <div>
		  <?php echo $faq_description; ?>
	  </div>
	  <div id="faq-accordion">

	  <?php foreach($faq_questions as $question): ?>
	<h3 id="<?php echo $question['title']; ?>"><a class="accordion-choice" href="#<?php echo $question['title']; ?>"><?php echo $question['title']; ?></a></h3>
	    <div>
		<?php echo $question['description']; ?>
	      <div class="faq-arrow-up-container">
		<a href="#top" class="faq-arrow-up">
		  <img src="/images/faq_arrow_up.png" />Terug naar overzicht
		</a>
	      </div>
	    </div>
	  <?php endforeach;?>
	  </div>
	</div>



	<div id="faq-right">
	  <h3>Kies uw onderwerp</h3>
	  <ul>
		<?php
			$i = 0;
			foreach($faq_questions2 as $question2): 
		?>
	    <li>
	
			<a href="#<?php echo $question2['title'];?>" class="question" onClick="open_question(<?php echo $i; ?>)"><?php echo $question2['title'];?></a>
	    </li>
		<?php 
			$i++;
			endforeach; 
		?>
	  </ul>
	</div>  


      </div>
    </div>



<?php
echo layout_footer();