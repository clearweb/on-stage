<?php
$this->load->helper('layout');
$this->load->helper('form');

echo layout_header('home', array(), array('account_form'));
?>

	<div class="wrapper">
		<?php echo layout_search(); ?>
		<h1 class="page-title">Verwijderen</h1>
    </div>
<?php
	// if(tegoeden)
?>
    <div id="main">
      <div id="main-content">
	  	<div id="account-form-left">
	      <div>
	        <p>
		      U heeft aangegeven dat u uw account wilt verwijderen.
		      Hier onder staan uw eventuele tegoeden weergegeven.
	        </p>
	      </div>
	      <div class="account-form-block registration-block">
<?php
	echo form_open('user/register');
?>
	        <h3>Weet u zeker dat u uw account wilt verwijderen?</h3>
<!--
	        <div class="input-boxes">
	          <div>
	            <label for="input-email">WAT IS DE REDEN</label>
	            <input id="input-email" class="validate" name="email" type="textarea" value="<?php echo set_value('email', 'EMAIL');?>" onfocus="if (this.value=='EMAIL')this.value=''" onblur="if (this.value=='')this.value='EMAIL'" />
	          </div>

	          <div>
	            <label for="input-password">UW WACHTWOORD</label>
	            <input type="text" id="input-fake-password" value="WACHTWOORD" />
	            <input type="password"  class="validate" id="input-password" name="password" style="display:none" />
	          </div>
	          <div>
	            <label for="input-password2">HERHAAL UW WACHTWOORD</label>
	            <input id="input-fake-password2" type="text" value="HERHAAL UW WACHTWOORD"/>
	            <input id="input-password2"  class="validate" type="password" name="password2" style="display:none"/>
	          </div>
	        </div>
-->

	        <div class="submit-wrapper">
	          <input type="submit" class="call-to-action" value="Ja, mijn account verwijderen">
	          <!--input type="submit" class="call-to-action" value="Nee, annuleren">-->
	        </div>

	
	        <div class="account-form-block-bottom"></div>
<?php
	echo form_close();
?>
	      </div>
	
	    </div>
		<div id="account-form-right">
		  <h3>Kies uw onderwerp</h3>
		  <div class="account-form-block">
		    <?php
		      echo layout_get_subjects();
		    ?>
		  </div>
		</div>
	  </div>
	</div>

<?php
echo layout_footer();