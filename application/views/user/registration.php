<?php
$this->load->helper('layout');
$this->load->helper('form');

echo layout_header('home', array(), array('account_form'));
?>

	<div class="wrapper">
		<?php echo layout_search(); ?>
		<h1 class="page-title">Registreren</h1>
    </div>
    <img id="top-image" src="/images/register-top.png" />
	
    <div id="main">
      <div id="main-content">
	  	<div id="account-form-left">
	      <div>
	        <p>
		      Registreer via het onderstaande formulier jouw persoonlijke on-stage.tv account.
		      Met dit account bekijk je jouw favoriete voorstellingen zonder wachttijden en pauzes.
	        </p>
		<?php echo $error; ?>
	      </div>
	      <div class="account-form-block registration-block">
<?php
	echo form_open('user/register');
?>
	        <h3>Gratis account bij on-stage.tv</h3>
	        <p>
	        Uw gegevens worden nooit gedeeld met andere partijen.
	        </p>
	        <div class="input-boxes">
	          <div>
	            <label for="input-email">UW EMAIL (TEVENS UW GEBRUIKERSNAAM!)</label>
	            <input id="input-email" class="validate" name="email" type="text" value="<?php echo set_value('email', 'EMAIL');?>" onfocus="if (this.value=='EMAIL')this.value=''" onblur="if (this.value=='')this.value='EMAIL'" />
	          </div>

	          <div>
	            <label for="input-password">UW WACHTWOORD</label>
	            <input type="text" id="input-fake-password" value="WACHTWOORD" />
	            <input type="password"  class="validate" id="input-password" name="password" style="display:none" />
	          </div>
	          <div>
	            <label for="input-password2">HERHAAL UW WACHTWOORD</label>
	            <input id="input-fake-password2" type="text" value="HERHAAL UW WACHTWOORD"/>
	            <input id="input-password2"  class="validate" type="password" name="password2" style="display:none"/>
	          </div>
	        </div>

	        <div class="policies">
	          <div>
	          <?php
	          echo form_checkbox('double_opt_in', '1', set_checkbox('double_opt_in', '1', FALSE), 'id="input-double-opt-in"');
	          ?>
			    <label for="input-double-opt-in">Ja, ik ontvang graag de nieuwsbrief en blijf op de hoogte van de nieuwste voorstellingen en acties</label>
	          </div>
	
	          <div>
	          <?php
		          echo form_checkbox('policy', 'accept', set_checkbox('policy', 'accept'), 'id="input-policy"'); 
	          ?>
	          <label for="input-policy">Ja, ik ga akkoord met de <a target="_blank" href="/faq/questions/about#algemene voorwaarden">algemene voorwaarden</a>.</label>
	          </div>
						<?php 
						//echo form_error('policy');
						?>
	        </div>

	        <div class="submit-wrapper">
	          <input type="submit" class="call-to-action" name="register" value="REGISTREREN">
	        </div>

	
	        <div class="account-form-block-bottom"></div>
<?php
	echo form_close();
?>
	      </div>
	
	    </div>
		<div id="account-form-right">
		  <h3>Kies uw onderwerp</h3>
		  <div class="account-form-block">
		    <?php
		      echo layout_get_subjects();
		    ?>
		  </div>
		</div>
	  </div>
	</div>

<?php
echo layout_footer();