<?php
$this->load->helper('layout');
$this->load->model('Performance_model');

echo layout_header('home', array(), array('account_form'));
?>

	<div class="wrapper">
		<?php echo layout_search(); ?>
		<h1 class="page-title">MIJN ON-STAGE.TV</h1>
    </div>
    <div id="main">
      <div id="main-content">
	<div id="account-introduction">
	<p>
	Beste klant,
	</p>
	<p>
	Hieronder kunt u uw tegoed bekijken, voordat u permanent uw account verwijderd.
<?php
//'
?>
	</p>
	</div>
	<div id="account-left">
<?php
	if($credits):
?>
	
	  <h3>Tegoeden</h3>

<?php
	endif;
	foreach($credit_performances as $credit_performance):
?>

	  <div class="account-block">
	
	    <div class="credit-head">
	      <a href="/performance/video/<? echo $credit_performance['id'] ?>"><img height="83" width="134" class="credit-image" src="<?php echo $credit_performance['thumbnail_url']; ?>" /></a>
	      <div class="credit-head-description">
	        <p>
	        NOG BESCHIKBAAR: 
	        </p>
	        <p class="hours">
	<?php
	echo ceil($credit_performance['hours_left']);
	?> UUR</br>
				<a href="/performance/video/<? echo $credit_performance['id'] ?>">Nu bekijken</a>
	        </p>
	      </div>
	    </div>
	    <div class="credit-body">
	      <table id="performance-attributes-table">
	        <tr>
	          <td class="performance-attribute">Uitvoerende</td>
	          <td><?php echo $credit_performance['performer']; ?></td>
	        </tr>
	        <tr>
	          <td class="performance-attribute">Titel</td>
	          <td><?php echo $credit_performance['title']; ?></td>
	        </tr>
	        <tr>
	          <td class="performance-attribute">Genre</td>
	          <td><?php echo $credit_performance['genre']; ?></td>
	        </tr>
	        <tr>
	          <td class="performance-attribute">Speelduur</td>
	          <td><?php echo $credit_performance['duration']; ?> minuten</td>
	        </tr>
	        <tr>
	          <td class="performance-attribute">Jaar</td>
	          <td><?php echo $credit_performance['year']; ?></td>
	        </tr>
	        <tr>
	          <td class="performance-attribute">Regie</td>
	          <td><?php echo $credit_performance['regission']; ?></td>
	        </tr>
	        <tr>
	          <td class="performance-attribute">Productie</td>
	          <td><?php echo $credit_performance['production']; ?></td>
	        </tr>
	        <tr>
	          <td class="performance-attribute">Tekst</td>
	          <td><?php echo $credit_performance['texts']; ?></td>
	        </tr>
	        <tr>
	          <td class="performance-attribute">Muziek</td>
	          <td><?php echo $credit_performance['music']; ?></td>
	        </tr>	      
		  </table>
	    </div>
	  </div>
	  
<?php 
			  endforeach;
?>			  
	  <h3>Account Verwijderen</h3>
	  <div class="account-block account-form-block">
<?php
	form_hidden('action', 'delete');
	echo form_open('/user/remove/delete');
?>			        
		<div class="submit-wrapper">
	        <input type="submit" class="call-to-action" value="Ja, ik wil mijn account verwijderen">
	    </div>
			
		</br>
	<?php
	echo form_close();
	?>
	  </div>
	  	  <h3>ON-STAGE.SOCIAL</h3>
	  <div class="account-block">
	<?php
		echo layout_facebook(true, '535', '200', 'whitesmoke');
	?>
	  </div>
	</div>
	<!-- account left -->
	<div id="account-right">
	  <h3>Kies uw onderwerp</h3>
	  <div class="account-block">
	    <?php
		echo layout_get_subjects();
		?>
	  </div>
	  <h3>RECENT BEKEKEN VOORSTELLINGEN</h3>
	  <div class="account-block">
		<ul class="vertical-performances-list">
		<?php
		foreach($recent_performances as $performance) :
		?>
		<li>
		<a href="/performance/trailer/<?php echo $performance['id']; ?>"><img src="<?php echo $performance['thumbnail_url']; ?>" height="140" width="225" /></a>
		<div class="carousel-item-title"><a href="/performance/trailer/<?php echo $performance['id']; ?>"><?php echo $performance['title']; ?></a></div>
		<div class="carousel-item-category"><?php echo $performance['genre']; ?></div>
		</li>
		<?php
		endforeach;
		?>
		</ul>
	  </div>
	</div>

	  </div>
	  <!-- main-content -->
	</div>
	<!-- main -->

<?php
echo layout_footer();