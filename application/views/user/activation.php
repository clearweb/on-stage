<?php
$this->load->helper('layout');

echo layout_header('home', array(), array('account_form'));
?>

	<div class="wrapper">
		<?php echo layout_search(); ?>
		<h1 class="page-title">Gelukt!</h1>
    </div>
    <div id="main">
      <div id="main-content">
	  	<div id="account-form-left">
	      <div class="account-form-block">
	        <p>
		      Uw account is succesvol geactiveerd. Heel veel plezier met de voorstelling!
	        </p>
	        <p>
	          Met vriendelijke groet,
	        </p>
	        <p>
	          Het on-stage.tv team.
	        </p>
	        <div class="action-wrapper">
	          <a class="call-to-action" href="/performance/">Afronden</a>
	        </div>
	        <div class="account-form-block-bottom"></div>
	      </div>
	    </div>
		<div id="account-form-right">
		  <h3>Kies uw onderwerp</h3>
		  <div class="account-form-block">
		    <?php
		      echo layout_get_subjects();
		    ?>
		  </div>
		</div>
	  </div>
	</div>

<?php
echo layout_footer();