<?php
$this->load->helper('layout');

echo layout_header('home', array(), array('account_form'));
?>

	<div class="wrapper">
		<?php echo layout_search(); ?>
		<h1 class="page-title">Wachtwoord vergeten?</h1>
    </div>
    <div id="main">
      <div id="main-content">
	  	<div id="account-form-left">
	      <div class="account-form-block">
<?php
	echo form_open('user/reset_password');
?>
	        <p>
		      Vul hieronder je e-mailadres in en klik op 'versturen'.
		      <br />
		      Je ontvangt van ons een e-mail met een link waarmee je een nieuw wachtwoord kunt instellen.
	        </p>
	        <div class="input-boxes">

	          <div>
	            <label for="input-email">UW EMAIL (TEVENS UW GEBRUIKERSNAAM!)</label>
	            <input id="input-email" name="email" class="validate" type="text" value="<?php echo set_value('email', 'EMAIL');?>" onfocus="if (this.value=='EMAIL')this.value=''" onblur="if (this.value=='')this.value='EMAIL'" />
	          </div>

	        </div>

	        <div class="submit-wrapper">
	          <input type="submit" class="call-to-action" value="Versturen" />
	        </div>
	        <div class="account-form-block-bottom"></div>
<?php
	echo form_close();
?>
	      </div>
	    </div>
		<div id="account-form-right">
		  <h3>Kies uw onderwerp</h3>
		  <div class="account-form-block">
		    <?php
		      echo layout_get_subjects();
		    ?>
		  </div>
		</div>
	  </div>
	</div>

<?php
echo layout_footer();