<?php
$this->load->helper('layout');
$this->load->model('Performance_model');
$this->load->model('Newsletter_model');

echo layout_header('home', array(), array('account_form'));
?>

	<div class="wrapper">
		<?php echo layout_search(); ?>
		<h1 class="page-title">MIJN ON-STAGE.TV</h1>
    </div>
    <div id="main">
      <div id="main-content">
		<div id="account-introduction">
			<p>
				Beste klant,
			</p>
			<p>
				Hieronder kunt u uw tegoed bekijken, uw persoonlijke gegevens wijzigen, wachtwoord veranderen en je recent bekeken video's nog eens nakijken.
<?php
//'
?>
			</p>
		</div>
		<div id="account-left">
	<?php
	if($credits):
?>
	  <h3>Tegoeden</h3>

	<?php
	endif;
	foreach($credit_performances as $credit_performance):
	?>

	  <div class="account-block">
	
	    <div class="credit-head">
	      <a href="/performance/video/<? echo $credit_performance['id'] ?>"><img height="83" width="134" class="credit-image" src="<?php echo $credit_performance['thumbnail_url']; ?>" /></a>
	      <div class="credit-head-description">
	        <p>
	        NOG BESCHIKBAAR: 
	        </p>
	        <p class="hours">
	<?php
	echo ceil($credit_performance['hours_left']);
	?> UUR</br>
				<a href="/performance/video/<? echo $credit_performance['id'] ?>">Nu bekijken</a>
	        </p>
	      </div>
	    </div>
	    <div class="credit-body">
	      <table id="performance-attributes-table">
	        <tr>
	          <td class="performance-attribute">Uitvoerende</td>
	          <td><?php echo $credit_performance['performer']; ?></td>
	        </tr>
	        <tr>
	          <td class="performance-attribute">Titel</td>
	          <td><?php echo $credit_performance['title']; ?></td>
	        </tr>
	        <tr>
	          <td class="performance-attribute">Genre</td>
	          <td><?php echo $credit_performance['genre']; ?></td>
	        </tr>
	        <tr>
	          <td class="performance-attribute">Speelduur</td>
	          <td><?php echo $credit_performance['duration']; ?> minuten</td>
	        </tr>
	        <tr>
	          <td class="performance-attribute">Jaar</td>
	          <td><?php echo $credit_performance['year']; ?></td>
	        </tr>
	        <tr>
	          <td class="performance-attribute">Regie</td>
	          <td><?php echo $credit_performance['regission']; ?></td>
	        </tr>
	        <tr>
	          <td class="performance-attribute">Productie</td>
	          <td><?php echo $credit_performance['production']; ?></td>
	        </tr>
	        <tr>
	          <td class="performance-attribute">Tekst</td>
	          <td><?php echo $credit_performance['texts']; ?></td>
	        </tr>
	        <tr>
	          <td class="performance-attribute">Muziek</td>
	          <td><?php echo $credit_performance['music']; ?></td>
	        </tr>	      
		  </table>
	    </div>
	  </div>
	  
<?php 
			  endforeach;
?>			  
	  <h3>Persoonlijke gegevens</h3>
	  <div class="account-block account-form-block">
	<?php
	if ($changed)
	{
		echo '<div class="pass-change-box"></div>';
	}
	echo form_open();
	?>
		<div class="input-boxes">
	      <div>
	        <label for="input-email">UW EMAIL (TEVENS UW GEBRUIKERSNAAM!)</label>
	        <input id="input-email" name="email" type="text" value="<?php echo set_value('email', $this->User_model->getProperty('username'));?>" onfocus="if (this.value=='EMAIL')this.value=''" onblur="if (this.value=='')this.value='EMAIL'" />
			<input type="submit" value="Bijwerken" />
	      </div>
	      <div>
	        <label for="input-password">UW WACHTWOORD</label>
	        <input type="text" id="input-fake-password" value="WACHTWOORD WIJZIGEN" />
	        <input type="password" id="input-password" name="password" style="display:none" />
	      </div>
	
	      <div>
	        <label for="input-password2">HERHAAL UW WACHTWOORD</label>
	        <input id="input-fake-password2" type="text" value="HERHAAL UW WACHTWOORD"/>
	        <input id="input-password2" type="password" name="password2" style="display:none"/>
	      </div>
	    </div>

	    <div class="policies">
	      <div>
	        <?php			
				if( $this->Newsletter_model->lookup_newsletter($this->User_model->getProperty('username')) ){
					$optin = TRUE;
				}else{
					$optin = FALSE;
				}
				
				echo form_checkbox('double_opt_in', '1', set_checkbox('double_opt_in', '1', ((bool)$optin)), 'id="input-double-opt-in"');
	        ?>
			<label for="input-double-opt-in">Ja, ik ontvang graag de nieuwsbrief en blijf op de hoogte van de nieuwste voorstellingen en acties</label>
	      </div>
	    </div>
		
		</br>
		<div class="input-divider"></div>
		<div class="input-boxes">
	      <div>
	        <label for="input-password2">ACCOUNT VERWIJDEREN</label>
	        <a href="/user/remove">Account Verwijderen</a>
	      </div>
	    </div>
		<?php
		echo form_close();
		?>
	  </div>
		
	  <h3>ON-STAGE.SOCIAL</h3>
	  <div class="account-block">
		<?php
			echo layout_facebook(true, '535', '200', 'whitesmoke');
		?>
	  </div>
	 </div>
	<!-- account left -->
	<div id="account-right">
	  <h3>Kies uw onderwerp</h3>
	  <div class="account-block">
	    <?php
		echo layout_get_subjects();
		?>
	  </div>
	  <h3>RECENT BEKEKEN VOORSTELLINGEN</h3>
	  <div class="account-block">
		<ul class="vertical-performances-list">
		<?php
		foreach($recent_performances as $performance) :
		?>
		<li>
		<a href="/performance/trailer/<?php echo $performance['id']; ?>"><img src="<?php echo $performance['thumbnail_url']; ?>" height="140" width="225" /></a>
		<div class="carousel-item-title"><a href="/performance/trailer/<?php echo $performance['id']; ?>"><?php echo $performance['title']; ?></a></div>
		<div class="carousel-item-category"><?php echo $performance['genre']; ?></div>
		</li>
		<?php
		endforeach;
		?>
		</ul>
	  </div>
	</div>

	  </div>
	  <!-- main-content -->
	</div>
	<!-- main -->

<?php
echo layout_footer();