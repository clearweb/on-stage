<?php
$this->load->helper('layout');

echo layout_header('home', array(), array('account_form'));
?>

	<div class="wrapper">
	<?php echo layout_search(); ?>
		<h1 class="page-title">Onjuiste gebruikersnaam of wachtwoord</h1>
    </div>
    <div id="main">
      <div id="main-content">
	  	<div id="account-form-left">
	      <div class="account-form-block">
<?php
	echo form_open();
	if ( ! empty($referer))
	{
		echo form_hidden('referer', $referer);
	}
?>
	        <p>
			  U hebt een verkeerd gebruikersnaam of wachtwoord ingevuld.
	        </p>
			<p>
			Bent u uw <a href="/user/reset_password/">wachtwoord vergeten?</a>
			<br />
			Hebt u nog geen account? <a href="/user/register/">registreer gratis!</a>
	        </p>
	        <div class="input-boxes">
                  
	          <div>
	            <label for="input-email">UW EMAIL (TEVENS UW GEBRUIKERSNAAM!)</label>
				<input id="input-email" name="email" type="text" value="<?php echo set_value('email', 'EMAIL');?>" onfocus="if (this.value=='EMAIL')this.value=''" onblur="if (this.value=='')this.value='EMAIL'" />
	          </div>

			  <div>
	            <label for="input-password">UW WACHTWOORD</label>
	            <input type="text" id="input-fake-password" value="WACHTWOORD" />
	            <input type="password" id="input-password" name="password" style="display:none" />
	          </div>
	        </div>

	        <div class="submit-wrapper">
	          <input type="submit" class="call-to-action" value="Versturen" />
	        </div>
	        <div class="account-form-block-bottom"></div>
<?php
	echo form_close();
?>
	      </div>
	    </div>
		<div id="account-form-right">
		  <h3>Kies uw onderwerp</h3>
		  <div class="account-form-block">
		    <?php
		      echo layout_get_subjects();
		    ?>
		  </div>
		</div>
	  </div>
	</div>

<?php
echo layout_footer();