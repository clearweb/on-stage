<?php
$this->load->helper('layout');

echo layout_header('home', array(), array('account_form'));
?>

	<div class="wrapper">
	<?php echo layout_search(); ?>
		<h1 class="page-title">Uw wachtwoord veranderen</h1>
    </div>
    <div id="main">
      <div id="main-content">
	  	<div id="account-form-left">
	      <div class="account-form-block">
<?php
	echo form_open();
?>
	        <p>
			U kunt met het volgende formulier uw wachtwoord veranderen.
	        </p>
	        <div class="input-boxes">
                  
	          <div>
	            <label for="input-email">UW EMAIL (TEVENS UW GEBRUIKERSNAAM!)</label>
		    <?php 
		    echo $email;
					?>
	          </div>

		  <div>
	            <label for="input-password">UW WACHTWOORD</label>
	            <input type="text" id="input-fake-password" value="WACHTWOORD" />
	            <input type="password"  class="validate" id="input-password" name="password" style="display:none" />
	          </div>
	          <div>
	            <label for="input-password2">HERHAAL UW WACHTWOORD</label>
	            <input id="input-fake-password2" type="text" value="HERHAAL UW WACHTWOORD"/>
	            <input id="input-password2"  class="validate" type="password" name="password2" style="display:none"/>
	          </div>

	        </div>

	        <div class="submit-wrapper">
	          <input type="submit" class="call-to-action" value="Versturen" />
	        </div>
	        <div class="account-form-block-bottom"></div>
<?php
	echo form_close();
?>
	      </div>
	    </div>
		<div id="account-form-right">
		  <h3>Kies uw onderwerp</h3>
		  <div class="account-form-block">
		    <?php
		      echo layout_get_subjects();
		    ?>
		  </div>
		</div>
	  </div>
	</div>

<?php
echo layout_footer();