<?php
$this->load->helper('layout');
$this->load->model('Performance_model');

echo layout_header('home', array(), array('account_form'));
?>

	<div class="wrapper">
		<?php echo layout_search(); ?>
		<h1 class="page-title">MIJN ON-STAGE.TV</h1>
    </div>
    <div id="main">
      <div id="main-content">
	<div id="account-introduction">
	<p>
	Beste klant,
	</p>
	<p>
	Uw account is verwijderd. Bedankt dat u gebruik heeft gemaakt van onze service.
	</p>
	</div>
	
	<div id="account-left">
	  
	  <h3>Account Verwijderd !</h3>
	  <div class="account-block account-form-block">
		<p style="font-size: 16px; margin-top: -5px;">     
		Uw account is permanent verwijderd.</br>
		Als u wilt, kunt zich opnieuw registreren voor een nieuw account.</br>
		</br>
		Bedankt dat u gebruik heeft gemaakt van onze service.
		</p>
	  </div>
  
	</div>
	
	<!-- account left -->
	<div id="account-right">
	  <h3>Kies uw onderwerp</h3>
	  <div class="account-block">
	    <?php
		echo layout_get_subjects();
		?>
	  </div>
	</div>

	  </div>
	  <!-- main-content -->
	</div>
	<!-- main -->

<?php
echo layout_footer();