<?php
$this->load->helper('layout');
$this->load->helper('text');
$this->load->library('pagination');
echo layout_header('performances');

$config['base_url'] = '/performance/index/';
$config['total_rows'] = $total_hits;
$config['per_page'] = $items_per_page;

$config['display_pages'] = FALSE;
$config['display_pages_summary'] = TRUE;

$config['pages_summary_open'] = '<span>';
$config['pages_summary_close'] = '</span>';

$config['num_links'] = 1;
$config['use_page_numbers'] = TRUE;

$config['full_tag_open'] = '<div class="pagination">';
$config['full_tag_close'] = '</div>';
$config['last_link'] = FALSE;
$config['first_link'] = FALSE;
$config['next_link'] = '<img src="/images/tiny-play.png" />';
$config['prev_link'] = '<img src="/images/inverse-tiny-play.png" />';

$this->pagination->initialize($config); 
//$search = urldecode($search);
?>

    <div id="main">
      <div id="main-content">
  <?php echo layout_search($search); ?>

	<div id="main-content-meta">
<!--	  <h1 class="alignleft"> -->
	<?php
	if (empty($search))
		echo heading('Voorstellingen', 1, 'class="alignleft"');
	else
		echo heading('Zoeken naar voorstellingen met "'.$search.'"',1);
	?>
<!-- </h1> -->
<!--
	  <a class="meta-filter selected" href="javacript:void()">Toon alles</a>
	  <a class="meta-filter" href="javacript:void()">Meest populair</a>
	  <a class="meta-filter" href="javacript:void()">Meest recent</a>
-->
	<?php echo $this->pagination->create_links(); ?>
	</div>

  <?php echo layout_sidefilters();?>

  
	<div id="main-content-center">
	  <ul id="performance-grid">

	<?php
	foreach($performances as $performance):
	?>
	    <li>
	      <img src="<?php echo $performance['thumbnail_url']; ?>" width="134" height="83" alt="" />
	      <div class="carousel-item-title"><a><?php echo character_limiter($performance['title'], 16); ?></a></div>
	      <div class="carousel-item-category"><?php echo $performance['genre']; ?></div>
	      <div class="carousel-bottom">
	        <a href="/performance/trailer/<?php echo $performance['id']; ?>" class="watch-trailer">Bekijk trailer</a>
	        <a href="/performance/video/<?php echo $performance['id']; ?>" class="watch-now">Nu kijken</a>
	      </div>
	    </li>
	<?php
	endforeach;
	?>
	   <div id="performance-grid-bottom">
	      <?php echo $this->pagination->create_info(); ?>
	      <?php echo $this->pagination->create_links(); ?>
	   </div>
	  </ul>
	  
	  <div class="bottom-box-container">
	    <h3>Veilig betalen</h3>	
	    <div class="bottom-box">
	      <img id="pay-safely-payment-methods" src="/images/payment-methods.png" />
	      <img id="pay-safely-logo" src="/images/small-logo.png" />
	    </div>
	  </div>
	  
	  <div id="registrations-in-own-management" class="bottom-box-container">
	    <h3>&nbsp;</h3>
	    <div class="bottom-box">
	      <div id="registrations-in-own-management-right">
		
	      </div>
	    </div>
	  </div>
	  
	</div>
	
      </div>

      <div id="main-bottom-block">
	<div id="main-bottom-block-left">
	  <div class="separator-light"></div>
	  <h1>{text_bottom_title}</h1>
	  <div class="separator-dark"></div>
	  {text_bottom}
	  <div class="alignright">
	       <a href="/user/register" class="call-to-action">
		 Gratis aanmelden
	       </a>
	  </div>
	</div>
	<div>
	  <img src="/images/two-pcs.png" />
	</div>


     </div>
    </div>



<?php
echo layout_footer();