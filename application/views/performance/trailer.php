<?php
$this->load->helper('layout');
$this->load->helper('url');


// Facebook "like" button settings
$facebook['title']			= $this->Performance_model->getProperty('performer') .' - '. $this->Performance_model->getProperty('title');
$facebook['url']			= current_url();
$facebook['type']			= 'website';
$facebook['description']	= $this->Performance_model->getProperty('summary');
$facebook['image']			= $this->Performance_model->getProperty('thumbnail_url');

echo layout_header('performances', array(), array('jquery.jcarousel.min'), array($facebook) );
?>

  <div id="main">
    <div id="main-content">
      <?php echo layout_search(); ?>
	  
	  	<div id="trailer-left">
	<h1 class="performance-title"><?php echo $this->Performance_model->getProperty('performer'); ?> - <?php echo $this->Performance_model->getProperty('title'); ?></h1>
	<h2 class="performance-summary"><?php echo $this->Performance_model->getProperty('summary'); ?></h2>
	

	<?php echo $video->get_video_html(640, 357); ?>
	  
	  <div id="performance-description-box">
	    <div id="performance-description-box-title">PLOT</div>
	    <div id="performance-description">
	      <?php echo $this->Performance_model->getProperty('description'); ?>
	    </div>
	    <table id="performance-attributes-table">
	      <tr>
		<td class="performance-attribute">Uitvoerende</td>
	      <td><?php echo $this->Performance_model->getProperty('crew'); ?></td>
	      </tr>
	      <tr>
		<td class="performance-attribute">Titel</td>
	      <td><?php echo $this->Performance_model->getProperty('title'); ?></td>
	      </tr>
	      <tr>
		<td class="performance-attribute">Genre</td>
	      <td><?php echo $this->Performance_model->getProperty('genre'); ?></td>
	      </tr>
	      <tr>
		<td class="performance-attribute">Speelduur</td>
	      <td><?php echo $this->Performance_model->getProperty('duration'); ?> minuten</td>
	      </tr>
	      <tr>
		<td class="performance-attribute">Jaar</td>
	      <td><?php echo $this->Performance_model->getProperty('year'); ?></td>
	      </tr>
	      <tr>
		<td class="performance-attribute">Regie</td>
	      <td><?php echo $this->Performance_model->getProperty('regission'); ?></td>
	      </tr>
	      <tr>
		<td class="performance-attribute">Productie</td>
	      <td><?php echo $this->Performance_model->getProperty('production'); ?></td>
	      </tr>
	      <tr>
		<td class="performance-attribute">Tekst</td>
	      <td><?php echo $this->Performance_model->getProperty('texts'); ?></td>
	      </tr>
	      <tr>
		<td class="performance-attribute">Muziek</td>
	      <td><?php echo $this->Performance_model->getProperty('music'); ?></td>
	      </tr>	      
	    </table>
	    <a class="simple-call-to-action" href="/performance/video/<?php echo $this->Performance_model->get_id();?>">Huur voor &euro; <?php echo number_format($this->Performance_model->getProperty('rental_price'), 2, ',', '.'); ?></a>
	    <div id="description-separator"></div>
	    <div id="performance-description-pre-bottom">
	      <div id="performance-share">Delen met vrienden:</div>
		  <?php 
		  $link 	=  site_url('/performance/trailer/'.$this->Performance_model->get_id());
		 		 
		  echo layout_social($link, $this->config->item('twitter_account'), $this->config->item('twitter_tagg')); 
		  ?>
		<!--<div id="performance-view-count">Bekeken: 212 Keer</div> -->
	      <img id="payment-methods" src="/images/payment-methods.png" />
	    </div>
	    <div id="performance-description-bottom"></div>
	  </div>
	  <div id="back-to-overview-container">
	    <a href="/performance" id="back-to-overview" class="call-to-action">terug naar overzicht</a>
	  </div>
	  <div id="trailer-left-bottom">
	    <div id="trailer-how-it-works-wrapper">
	      <h3>Hoe werkt het?</h3>
	      <div id="trailer-how-it-works">
		<ul>
		
		<?php	foreach($faq_questions as $question): 	?>
	    <li><a href="/faq/questions/how-it-works#<?php echo $question['title'];?>" class="question"><?php echo $question['title'];?></a></li>
		<?php endforeach; ?>
		
		</ul>
		<img src="/images/small-logo.png" />
		
		<div id="trailer-how-it-works-bottom"></div>
		
	      </div>
	    </div>
	    <div id="trailer-social-media-wrapper">
	      <h3>On-stage.social</h3>
	      <div id="trailer-social-media">
<?php
   echo layout_facebook(true, '380', '200');
?>
		<div id="trailer-social-media-bottom"></div>
	      </div>
	    </div>
	  </div>
	</div>
		
	<div id="trailer-right">
	  <div id="trailer-action-block" class="trailer-component">
	    <a href="/performance/video/<?php echo $this->Performance_model->get_id();?>" id="hire-and-watch" class="simple-call-to-action"><span>Huur en Bekijk</span><img src="/images/white_play.png" /></a>
	    <div>of</div>
	    <a href="/user/register" id="register-free" class="simple-call-to-action">Registreer gratis</a>
	    <div id="trailer-action-block-bottom">Direct online of via tv</div>
	  </div>
	  <?php
	  if( $related_performances ):
	  ?>
	  <div class="trailer-component">
	    <h3>Bekijk ook</h3>
	    <ul id="trailer-related-block">
		<?php
		foreach($related_performances as $performance) :
		?>
		<li>
		<a href="/performance/trailer/<?php echo $performance['id']; ?>"><img src="<?php echo $performance['thumbnail_url']; ?>" height="83" width="134" /></a>
		<div class="carousel-item-title"><a href="/performance/trailer/<?php echo $performance['id']; ?>"><?php echo $performance['title']; ?></a></div>
		<div class="carousel-item-category"><?php echo $performance['genre']; ?></div>
		</li>
		<?php
		endforeach;
		//endif;
		?>
	      <div id="trailer-related-block-bottom"></div>
	    </ul>
	    <a onclick="load_more_related_trailers();" id="load-more-related" class="simple-call-to-action">Meer suggesties laden</a>
	  </div>
	  <?php
	  endif;
	  ?>
	</div>

    </div>
  </div>
<?php
echo layout_footer();