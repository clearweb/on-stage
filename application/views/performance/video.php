<?php
$this->load->helper('layout');
echo layout_header('performances', array('http://vjs.zencdn.net/c/video-js.css'), array('jquery.jcarousel.min', 'http://vjs.zencdn.net/c/video.js'));
?>

  <div id="main">
    <div id="main-content">
  <?php echo layout_search(); ?>
	
	<h1 class="performance-title"><?php echo $this->Performance_model->getProperty('performer'); ?> - <?php echo $this->Performance_model->getProperty('title'); ?></h1>
	<h2 class="performance-summary"><?php echo $this->Performance_model->getProperty('summary'); ?></h2>

<?php 
	echo $video->get_video_html(979, 548);
?>
	

	<div id="performance-description-box">
	  <div id="performance-description-box-left">
	    <div id="performance-description-box-title">PLOT</div>
	    <div id="performance-description">
	      <?php echo $this->Performance_model->getProperty('description'); ?>
	    </div>
	  </div>
	  <table id="performance-attributes-table">
	    <tr>
	      <td class="performance-attribute">Uitvoerende</td>
	      <td><?php echo $this->Performance_model->getProperty('performer'); ?></td>
	    </tr>
	    <tr>
	      <td class="performance-attribute">Titel</td>
	      <td><?php echo $this->Performance_model->getProperty('title'); ?></td>
	    </tr>
	    <tr>
	      <td class="performance-attribute">Genre</td>
	      <td><?php echo $this->Performance_model->getProperty('genre'); ?></td>
	    </tr>
	    <tr>
	      <td class="performance-attribute">Speelduur</td>
	      <td><?php echo $this->Performance_model->getProperty('duration'); ?> minuten</td>
	    </tr>
	    <tr>
	      <td class="performance-attribute">Jaar</td>
	      <td><?php echo $this->Performance_model->getProperty('year'); ?></td>
	    </tr>
	    <tr>
	      <td class="performance-attribute">Regie</td>
	      <td><?php echo $this->Performance_model->getProperty('regission'); ?></td>
	    </tr>
	    <tr>
	      <td class="performance-attribute">Productie</td>
	      <td><?php echo $this->Performance_model->getProperty('production'); ?></td>
	    </tr>
	    <tr>
	      <td class="performance-attribute">Tekst</td>
	      <td><?php echo $this->Performance_model->getProperty('texts'); ?></td>
	    </tr>
	    <tr>
	      <td class="performance-attribute">Muziek</td>
	      <td><?php echo $this->Performance_model->getProperty('music'); ?></td>
	    </tr>	      
	  </table>
	  <div id="description-separator"></div>
	  <div id="performance-description-pre-bottom">
	    <div id="performance-share">Delen met vrienden:</div>
 <?php 
		  $link 	= '/performance/trailer/'.$this->Performance_model->get_id();
		 		 
		  echo layout_social($link, $this->config->item('twitter_account'), $this->config->item('twitter_tagg')); 
 ?>
	    <!-- <div id="performance-view-count">Bekeken: 212 Keer</div> -->
	    <div id="playing-problems-container">
	      <a id="playing-problems" href="/faq/questions/how-it-works" class="simple-call-to-action">
		Problemen met afspelen?
	      </a>
	    </div>
	  </div>
	  <div id="performance-description-bottom"></div>
	</div>
	<div id="back-to-overview-container">
	  <a href="/performance" id="back-to-overview" class="call-to-action">terug naar overzicht</a>
	</div>

	<div id="also-viewed-carousel" class="carousel-container">
	  <h3>Kijkers bekeken ook</h3>
	  <ul  class="jcarousel">
		<?php
		foreach($related_performances as $performance) :
		?>
		<li>
		<a href="/performance/trailer/<?php echo $performance['id']; ?>"><img src="<?php echo $performance['thumbnail_url']; ?>" height="83" width="134" /></a>
		<div class="carousel-item-title"><a href="/performance/trailer/<?php echo $performance['id']; ?>"><?php echo $performance['title']; ?></a></div>
		<div class="carousel-item-category"><?php echo $performance['genre']; ?></div>
		</li>
		<?php
		endforeach;
		?>
	  </ul>
	  <div class="carousel-separator"></div>

	</div>
	
    </div>
  </div>
<?php
echo layout_footer();