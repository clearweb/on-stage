<?php
$this->load->helper('layout');
$this->load->helper('text');
echo layout_header('home', array('nivo-slider'), array('jquery.jcarousel.min', 'jquery.nivo.slider'));
?>
    <div class="slider-wrapper">
      <div id="slider" class="nivoSlider">
        <img src="/images/slider1.png" data-thumb="/images/slider1.png" alt="Altijd op de eerste rij. Online podia voor de kunsten zonder deuren. Pay per view, zonder verplichtingen" />
        <img src="/images/slider2.png" data-thumb="/images/slider2.png" alt="" />
      </div>
      <div id="slider-bar-container">
	<div id="slider-bar"></div>
      </div>
  <?php
  echo layout_search();
  ?>
    </div>

  
    <div id="main">
      <div id="main-upper-block">
	<div id="main-upper-block-left">
	<h1>{text_top_title}</h1>
	  <div class="separator-dark"></div>
	  {text_top}
	  <div id="register-now">
	    <a class="call-to-action" href="/user/register">Meld je gratis aan!</a>
	  </div>
	  <div class="separator-light"></div>
	</div>
	<img src="/images/3_media_devices.png" />
      </div>
      
      <div id="main-content">

  <?php
  echo layout_sidefilters();
?>


	<div id="main-content-center">
	  <div id="new-container" class="show-slider">
	    <div class="show-slider-top">
	      <h2 class="show-slider-title">Nieuw op ON-STAGE</h2>
<!--
	      <a href="javascript:void();" class="show-slider-filter">Toon alles</a>
	      <span class="show-slider-filter-seperator">|</span>
	      <a href="javascript:void();" class="show-slider-filter">Meest populair</a>
	      <span class="show-slider-filter-seperator">|</span>
	      <a href="javascript:void();" class="show-slider-filter">Nieuwste</a>
-->
	    </div>
	    <div class="small-slider-wrapper">
	      <div id="new-slider" class="nivoSlider small-slider">
	<?php foreach($new_performances as $new_performance): ?>
		    <img src="/performance/slider_image/<?php echo $new_performance['id'] ?>" alt="" title="#new-slider-caption-<?php echo $new_performance['id']; ?>"/>
	<?php endforeach; ?>
	      </div>
	<?php foreach($new_performances as $new_performance): ?>
	      <div id="new-slider-caption-<?php echo $new_performance['id'] ?>" class="nivo-html-caption">
		    <h1 class="small-slider-author" style="color:<?php echo $new_performance['title_color']?>"><?php echo character_limiter($new_performance['performer'], 20); ?></h1>
		    <h2 class="small-slider-title"><?php echo character_limiter($new_performance['title'],35); ?></h2>
		    <div class="small-slider-summary">
	        <?php echo word_limiter($new_performance['summary'], 35); ?>
		    </div>
		    <div class="small-slider-price-wrapper">
		      <span class="small-slider-price">&euro; <?php echo number_format($new_performance['rental_price'], 2, ',', '.'); ?></span>
		    </div>

		    <div class="small-slider-choices-wrapper">
		      <a href="/performance/trailer/<?php echo $new_performance['id'] ?>" class="small-slider-watch-trailer">Bekijk trailer</a>
		      <a href="/performance/video/<?php echo $new_performance['id'] ?>" class="small-slider-watch-now">Nu kijken</a>
		    </div>
	      </div>
      	<?php endforeach; ?>
	    </div>
	  </div>
	  <div id="trailers-container" class="show-slider carousel-container">
	    <div class="show-slider-top">
	      <h2 class="show-slider-title">Trailers</h2>
 <!--
	      <a href="javascript:void();" class="show-slider-filter">Toon alles</a>
	      <span class="show-slider-filter-seperator">|</span>
	      <a href="javascript:void();" class="show-slider-filter">Meest populair</a>
	      <span class="show-slider-filter-seperator">|</span>
	      <a href="javascript:void();" class="show-slider-filter">Nieuwste</a>
 -->
	    </div>
	    
	    <ul id="trailer-carousel" class="jcarousel">
			<?php foreach ($trailers as $trailer): ?>
	      <li>
			<a href="/performance/trailer/<?php echo $trailer['id']; ?>"><img src="<?php echo $trailer['thumbnail_url']; ?>" width="134" height="83" alt="" /></a>
        	<div class="carousel-item-title"><a href="/performance/trailer/<?php echo $trailer['id']; ?>"><?php echo character_limiter($trailer['title'], 14); ?></a></div>
			<div class="carousel-item-category"><?php echo $trailer['genre']; ?></div>
	      </li>
           <?php endforeach; ?>
	    </ul>
	    <div class="carousel-separator"></div>
	  </div>
	  
	  
	  <div id="popular-container" class="show-slider">
	    <div class="show-slider-top">
	      <h2 class="show-slider-title">Populair</h2>
<!--
	      <a href="javascript:void();" class="show-slider-filter">Toon alles</a>
	      <span class="show-slider-filter-seperator">|</span>
	      <a href="javascript:void();" class="show-slider-filter">Meest populair</a>
	      <span class="show-slider-filter-seperator">|</span>
	      <a href="javascript:void();" class="show-slider-filter">Nieuwste</a>
-->
	    </div>
	    <div class="small-slider-wrapper">
	      <div id="popular-slider" class="nivoSlider small-slider">
	<?php foreach($popular_performances as $popular_performance): ?>
		    <img src="/performance/slider_image/<?php echo $popular_performance['id'] ?>" alt="" title="#popular-slider-caption-<?php echo $popular_performance['id']; ?>"/>
	<?php endforeach; ?>
	      </div>
	<?php foreach($popular_performances as $popular_performance): ?>
	      <div id="popular-slider-caption-<?php echo $popular_performance['id'] ?>" class="nivo-html-caption">
		    <h1 class="small-slider-author" style="color:<?php echo $popular_performance['title_color']?>"><?php echo character_limiter($popular_performance['performer'], 20); ?></h1>
		    <h2 class="small-slider-title"><?php echo character_limiter($popular_performance['title'], 35); ?></h2>
		    <div class="small-slider-summary">
	        <?php echo word_limiter($popular_performance['summary'], 35); ?>
		    </div>
		    <div class="small-slider-price-wrapper">
		      <span class="small-slider-price">&euro; <?php echo number_format($popular_performance['rental_price'], 2, ',', '.'); ?></span>
		    </div>

		    <div class="small-slider-choices-wrapper">
		      <a href="/performance/trailer/<?php echo $popular_performance['id'] ?>" class="small-slider-watch-trailer">Bekijk trailer</a>
		      <a href="/performance/video/<?php echo $popular_performance['id'] ?>" class="small-slider-watch-now">Nu kijken</a>
		    </div>
	      </div>
      	<?php endforeach; ?>
	    </div>
	  </div>
	  
	  
	  <div id="artist-container" class="show-slider carousel-container">
	    <div class="show-slider-top">
	      <h2 class="show-slider-title">artiesten</h2>
 <!--
	      <a href="javascript:void();" class="show-slider-filter">Toon alles</a>
	      <span class="show-slider-filter-seperator">|</span>
	      <a href="javascript:void();" class="show-slider-filter">Meest populair</a>
	      <span class="show-slider-filter-seperator">|</span>
	      <a href="javascript:void();" class="show-slider-filter">Nieuwste</a>
  -->
	    </div>
	    <ul id="artist-carousel" class="jcarousel">
		<?php foreach($artists as $artist): ?>
	      <li>
			<a href="/performance/trailer/<?php echo $artist['id']; ?>"><img src="<?php echo $artist['thumbnail_url']; ?>" width="134" height="83" alt="" /></a>
			<div class="carousel-item-title"><a href="/performance/trailer/<?php echo $artist['id']; ?>"><?php echo character_limiter($artist['performer'], 12); ?></a></div>
			<div class="carousel-item-category"><?php echo $artist['genre']; ?></div>
	      </li>
		 <?php endforeach; ?>
	    </ul>
	    <div class="carousel-separator-light"></div>
	  </div>
	</div>
      </div>
      <div id="main-bottom-block">
	<div id="main-bottom-block-left">
	  <div class="separator-light"></div>
 	  <h1>{text_bottom_title}</h1>
	  <div class="separator-dark"></div>
	  {text_bottom}
	</div>
	<div id="discover-offers">
	  <a href="/performance" class="call-to-action">
	    Ontdek het aanbod
	  </a>
	</div>
      </div>
    </div> <!-- main -->
    <div id="pre-footer">
      <div id="pre-footer-wrapper">
		<div class="pre-footer-col">
		  <img src="/images/col-1.png" />
		  <h3>{text_prefooter1_title}</h3>
		  {text_prefooter1}
		</div>
		<div class="pre-footer-col">
		  <img src="/images/col-2.png" />
		  <h3>{text_prefooter2_title}</h3>
		  {text_prefooter2}
		</div>
		<div class="pre-footer-col">
		  <img src="/images/col-3.png" />
		  <h3>{text_prefooter3_title}</h3>
		  {text_prefooter3}
		</div>
      </div>
    </div>

<?php
echo layout_footer();