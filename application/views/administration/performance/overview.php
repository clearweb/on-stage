<?php
$this->load->helper('layout');
$this->load->library('grid');
echo layout_admin_header('performances');
?>

<div id="main">
  <div id="main-content">

<?php

$this->grid->set_title('Voorstelling beheren');
$this->grid->add_rows($performances);

$this->grid->filter_headers(array('id', 'description', 'summary', 'duration', 'texts', 'regission', 'production', 'music', 'image_url', 'title_color', 'thumbnail_url', 'video_url', 'video_id', 'rental_price', 'rental_period', 'available_from', 'filter_countries'));
$this->grid->set_header_names(array(
									'title' => 'Titel',
									'performer' => 'Artiest',
									'crew' => 'Uitvoerende',
									'genre' => 'Genre',
									'duration' => 'Speelduur',
									'year' => 'Jaar'
									)
							  );

$this->grid->add_operation('/performance/add', 'Toevoegen');

$this->grid->add_control_button();
$this->grid->add_control_button('/performance/delete/{id}', 'Verwijderen', array('confirmation-needed'));

$this->grid->render();
?>
	
  </div>
</div>

<?php
echo layout_admin_footer();
