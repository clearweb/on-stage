<?php
$this->load->helper('layout');
$this->load->library('table');
echo layout_admin_header('performances', array('colorpicker'), array('ckeditor/ckeditor', 'colorpicker'));
$this->load->helper('form');
?>

<div id="main">
  <div id="main-content">
	<h1>Voorstelling bewerken</h1>
<?php
echo form_open_multipart();

echo form_hidden('id', $this->Performance_model->id);
echo form_hidden('title_color', $this->Performance_model->getProperty('title_color', '#333333'));

$tmpl = array (
			   'table_open'  => '<table class="form-table">',
			   'heading_row_start' => '<tr>',
			   'heading_row_end' => '</tr>',
			   'heading_cell_start' => '<td>',
			   'heading_cell_end' => '</td>',
			   );
$this->table->set_template($tmpl);

$text_area = form_textarea( array(
								'name' => 'description',
								'id' => 'description',
								'class' => 'ckeditor',
								'value' => $this->Performance_model->getProperty('description')
								)
							);
$image_url = $this->Performance_model->getProperty('image_url', NULL);

if (empty($image_url))
{
	$image = '';
}
else 
{
	$image = '<img src="'.$this->Performance_model->getProperty('image_url').'" />';
}

?>

<div class="form-errors"><?php
echo validation_errors();
?></div>

<div id="performance-tabs">
    <ul>
        <li><a href="#tabs-1">Basis</a></li>
        <li><a href="#tabs-2">Instellingen</a></li>
        <li><a href="#tabs-3">Afbeelding</a></li>
    </ul>

	<div id="tabs-1">
<?php

$color_selector = '<div class="color-selector"><div style="background-color:'.$this->Performance_model->getProperty('title_color', '#333333').'"></div></div>';
$data = array(
			  array(form_label('Titel:', 'title'), form_input('title', $this->Performance_model->getProperty('title'), 'class="input-color-selector"').$color_selector),
			  array('', '<h1 class="color-selector-example small-slider-author">Voorbeeld Titel</h1>'),
			  array(form_label('Artiest:', 'performer'), form_input('performer', $this->Performance_model->getProperty('performer'))),
			  array(form_label('Uitvoerende:', 'crew'), form_input('crew', $this->Performance_model->getProperty('crew'))),
			  array(form_label('Genre:', 'genre'), form_input('genre', $this->Performance_model->getProperty('genre'))),
			  array(form_label('Speelduur:', 'duration'), form_input('duration', $this->Performance_model->getProperty('duration'))),
			  array(form_label('Jaar:', 'year'), form_input('year', $this->Performance_model->getProperty('year'))),
			  array(form_label('Regissie:', 'regission'), form_input('regission', $this->Performance_model->getProperty('regission'))),
			  array(form_label('Productie:', 'production'), form_input('production', $this->Performance_model->getProperty('production'))),
			  array(form_label('Teksten:', 'texts'), form_input('texts', $this->Performance_model->getProperty('texts'))),
			  array(form_label('Muziek:', 'music'), form_input('music', $this->Performance_model->getProperty('music'))),
			  array(form_label('Samenvatting:', 'summary'), form_textarea('summary', $this->Performance_model->getProperty('summary'))),
			  array(form_label('Beschrijving:', 'description'), $text_area),
			  );

echo $this->table->generate($data);
?>
</div>
<div id="tabs-2">
<?php

	preg_match( '/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/',$this->Performance_model->getProperty('available_from'), $date_attr);
	if( empty($date_attr[1]) ||  empty($date_attr[2]) ||  empty($date_attr[3]) ){
		$date_attr[1] = '00';
		$date_attr[2] = '00';
		$date_attr[3] = '00';
	}
	$available_from = "{$date_attr[3]}-{$date_attr[2]}-{$date_attr[1]}";


$filter_countries = $this->Performance_model->getProperty('filter_countries');

$data = array(
			  array(form_label('Beschikbaar vanaf:', 'available_from'), form_datepicker('available_from', $available_from)),
			  array(form_label('Huurperiode:', 'rental_period'), form_input('rental_period', $this->Performance_model->getProperty('rental_period'))),
			  array(form_label('Huurprijs:', 'rental_price'), form_input('rental_price', $this->Performance_model->getProperty('rental_price'))),
			  array(form_label('Geblokkeerde landen:', 'filter_countries'), form_multiselect('filter_countries[]', $countries, $filter_countries)),
			  );
echo $this->table->generate($data);
?>	
</div>

<div id="tabs-3">
<?php
$data = array(
			  array(form_label('Afbeelding:', 'image'), $image.form_upload('image')),
			  );
echo $this->table->generate($data);
?>	
</div>

<?php
	echo form_submit('submit', 'Verzenden');
?>
<a href="/performance/overview" id="back-to-overview" class="call-to-action">terug naar overzicht</a>

</div>
<?php
echo form_close();
	?>
  </div>
</div>

<?php
echo layout_admin_footer();