<?php
$this->load->helper('layout');
$this->load->library('table');
$this->load->helper('form');

echo layout_admin_header('dashboard');
?>

<div id="main">
  <div id="main-content">
	<h1>Inloggen</h1>
<?php
echo form_open();

$tmpl = array (
			   'table_open'  => '<table class="form-table login">',
			   'heading_row_start' => '<tr>',
			   'heading_row_end' => '</tr>',
			   'heading_cell_start' => '<td>',
			   'heading_cell_end' => '</td>',
			   );

$this->table->set_template($tmpl);

$data = array(
			  array( form_label('Gebruikersnaam:', 'username'), form_input('username', '')),
			  array(form_label('Wachtwoord:', 'password'), form_password('password', '')),
			  array('', form_submit('submit', 'Inloggen'))
			  );

if ($validation_error):
	?>
	<div class="form-errors">
	  <p>
		U heeft een onjuiste gebruikersnaam of een onjuist wachtwoord ingevoerd.
	  </p>
	</div>
	<?php
endif;

echo $this->table->generate($data);
echo form_close();
	?>
  </div>
</div>

<?php
echo layout_admin_footer();
