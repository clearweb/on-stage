<?php
$this->load->helper('layout');
$this->load->library('table');
$this->load->library('grid');
$this->load->helper('form');
echo layout_admin_header('dashboard', array('dashboard_print'), array('jquery.tablesorter', 'dashboard'));
?>

<div id="main">
  <div id="main-content">
			<div class="table-inputs-control">	
<?php
	if( empty($period['from']) ){
		if( empty($date_attr[1]) ||  empty($date_attr[2]) ||  empty($date_attr[3]) ){
			
			$date_attr[1] = '00';
			$date_attr[2] = '00';
			$date_attr[3] = '00';
			
			$date_attr[4] = date('Y', time());
			$date_attr[5] = date('m', time());
			$date_attr[6] = date('d', time());
		}
	
		//$period_from = "{$date_attr[3]}-{$date_attr[2]}-{$date_attr[1]}";
		$period_from = "";
		$period_till = "{$date_attr[6]}-{$date_attr[5]}-{$date_attr[4]}";
	}else{
		$period_from = $period['from'];
		$period_till = $period['till'];
	}
	
	echo form_open();
	echo form_label('Periode Van:', 'period_from'), form_datepicker('period_from', $period_from);	
	echo form_label('Periode Tot:', 'period_till'), form_datepicker('period_till', $period_till);
	echo form_submit('submit', 'Instellen');
	echo form_close();
?>
		</div>
	<div class="stats-table">
<?php	
	$this->grid->clear();
	$this->grid->set_title('Totaal');

	$this->grid->add_row($performance_stats_totals);

	$this->grid->filter_headers(array('id', 'summary',));
	$this->grid->set_header_names(array(
										'total_gebruikers' => 'Gebruikers',
										'total_voorstellingen' => 'Voorstellingen',
										'total_rents' => 'Aantal voorstellingen gehuurd',
										'total_views' => 'Aantal voorstellingen  bekeken',
										'total_newsletter' => 'Nieuwsbrief',
										'total_earnings' => 'Totale omzet',
										'gemiddeld' => 'Gemiddelde omzet',
										)
								  );
	$this->grid->render();
?>
	</div>
	<div class="stats-table">
<?php	
	$this->grid->clear();
	$this->grid->set_title('Voorstellingen');

	$this->grid->add_rows($performance_stats);

	$this->grid->filter_headers(array('id'));
	$this->grid->set_header_names(array(
										'title' => 'Voorstelling',
										'artist' => 'Artiest',
										'rents' => 'Aantal keer verhuurd',
										'views' => 'Aantal keer bekeken',
										'price' => 'Prijs',
										'earnings' => 'Totaal',
										)
								  );
	$this->grid->render();
?>		

	</div>
	<div class="stats-table">
<?php	
	$this->grid->clear();
	$this->grid->set_title('Gebruikers');

	$this->grid->add_rows($user_stats);

	$this->grid->filter_headers(array('id', 'summary',));
	$this->grid->set_header_names(array(
										'name' => 'Gebruiker',
										'rents' => 'Aantal voorstellingen gehuurd',
										'views' => 'Aantal voorstellingen  bekeken',
										'newsletter' => 'Nieuwsbrief',
										)
								  );
	$this->grid->render();
?>
	</div>
  </div>
</div>

<?php
echo layout_admin_footer();
