    <div id="top">
      <div id="top-wrap">
	<div id="logo-container">
	  <a href="/">
	    <img id="logo" src="/images/logo.png" />
	  </a>
	</div>
	<div id="top-right">
	  <div id="webuser-block">
		 <?php
		 $this->load->helper('admin_user');
		 if (admin_user_logged_in()) :
		 ?>
		 HALLO BEHEERDER | 
		 <a id="logout" href="/administration/logout">UITLOGGEN</a>
		 <?php
		 else:
		 ?>
		 <a id="login" href="/administration/login">LOGIN</a>
		 <?php
		 endif;
		 ?>
		 
	  </div>
	  <div id="menu">
	    <ul>
	      <li 
                  <?php if ($current_menu == 'dashboard'):?>
                  class="current-menu-item"
                  <?php endif; ?>
              >
		<a href="/administration">Dashboard</a>
	      </li>
	      <li 
                  <?php if ($current_menu == 'texts'):?>
                  class="current-menu-item"
                  <?php endif; ?>
              >
		<a href="/text/overview">Teksten</a>
	      </li>
	      <li 
                  <?php if ($current_menu == 'faqs'):?>
                  class="current-menu-item"
                  <?php endif; ?>
              >
	      <a href="/faq/overview">Faqs</a>
	      </li>
	      <li 
                  <?php if ($current_menu == 'performances'):?>
                  class="current-menu-item"
                  <?php endif; ?>
              >
		<a href="/performance/overview">Voorstellingen</a>
	      </li>
	      <li 
                  <?php if ($current_menu == 'videos'):?>
                  class="current-menu-item"
                  <?php endif; ?>
              >
			 <a href="/video/overview">Video's</a>
	      </li>
	      <li 
                  <?php if ($current_menu == 'users'):?>
                  class="current-menu-item"
                  <?php endif; ?>
              >
			 <a href="/user/overview">Gebruikers</a>
	      </li>
		  <li 
                  <?php if ($current_menu == 'newsletter'):?>
                  class="current-menu-item"
                  <?php endif; ?>
              >
			 <a href="/newsletter/overview">Nieuwsbrief</a>
	      </li>
	    </ul>
	  </div>
	</div>
      </div>
    </div>
   <!-- top -->