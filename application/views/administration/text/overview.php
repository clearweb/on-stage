<?php
$this->load->library('grid');
$this->load->helper('layout');
echo layout_admin_header('texts');
?>

<div id="main">
  <div id="main-content">
	
<?php

$this->grid->set_title('Teksten');

$this->grid->add_rows($texts);

$this->grid->filter_headers(array('id', 'page_url', 'text'));
$this->grid->set_header_names(array(
									'page' => 'Pagina',
									'title' => 'Titel',
									'position' => 'Positie',
									)
							  );

$this->grid->add_control_button('{page_url}', 'Bekijk pagina');
$this->grid->add_control_button();

$this->grid->render();
?>
  </div>
</div>

<?php
echo layout_admin_footer();