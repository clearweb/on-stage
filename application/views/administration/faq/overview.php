<?php
$this->load->helper('layout');
$this->load->library('grid');
echo layout_admin_header('faqs');

?>

<div id="main">
  <div id="main-content">
	
<?php

$this->grid->set_title('faqs');
$this->grid->add_rows($faqs);

$this->grid->filter_headers(array('id', 'description'));
$this->grid->set_header_names(array(
									'title' => 'Titel',
									'name' => 'Code'
									)
							  );

$this->grid->add_control_button('/faq_question/overview/{id}', 'Vragen beheren');
$this->grid->add_control_button();

$this->grid->render();
?>
  </div>
</div>

<?php
echo layout_admin_footer();