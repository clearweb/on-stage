<?php
$this->load->helper('layout');
$this->load->library('table');
echo layout_admin_header('faqs', array(), array('ckeditor/ckeditor'));
$this->load->helper('form');
?>

<div id="main">
  <div id="main-content">
	<h1>Faq vraag bewerken</h1>
<?php
echo form_open();

echo form_hidden('id', $this->Faq_question_model->id);

?>
<div class="form-errors">
<?php
echo validation_errors();
?>
</div>

<?php


$tmpl = array (
			   'table_open'  => '<table class="form-table">',
			   'heading_row_start' => '<tr>',
			   'heading_row_end' => '</tr>',
			   'heading_cell_start' => '<td>',
			   'heading_cell_end' => '</td>',
			   );
$this->table->set_template($tmpl);

$description_area = form_textarea( array(
										 'name' => 'description',
										 'id' => 'description',
										 'class' => 'ckeditor',
										 'value' => $this->Faq_question_model->description
										 )
								   );
$data = array(
			  array( form_label('Titel:', 'title'), form_input('title', $this->Faq_question_model->title)),
			  array(form_label('Footer category:', 'category'), form_dropdown('category', $this->Faq_question_model->get_categories(), $this->Faq_question_model->category)),
			  array(form_label('Tekst:', 'description'), $description_area),
			  array('', form_submit('submit', 'Verzenden').'<a href="/faq_question/overview/'.$this->Faq_question_model->faq_id.'" id="back-to-overview" class="call-to-action">terug naar overzicht</a>'),
			  );

echo $this->table->generate($data);

echo form_close();
	?>
  </div>
</div>

<?php
echo layout_admin_footer();