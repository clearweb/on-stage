<?php
$this->load->helper('layout');
$this->load->library('grid');
echo layout_admin_header('faqs');

?>

<div id="main">
  <div id="main-content">
	
<?php
$this->grid->set_title('faq vragen');

$this->grid->add_rows($faq_questions);

$this->grid->filter_headers(array('id', 'faq_id', 'category', 'rank', 'description'));
$this->grid->set_header_names(array(
									'title' => 'Vraag'
									)
							  );
$this->grid->add_operation('/faq_question/add/'.$faq_id, 'Toevoegen');


$this->grid->set_sortable('/faq_question/sort');
$this->grid->add_control_button('/faq_question/edit/{id}');
$this->grid->add_control_button('/faq_question/delete/{id}', 'Verwijderen', array('confirmation-needed'));

$this->grid->render();
?>

<a href="/faq/overview" id="back-to-overview" class="call-to-action">terug naar overzicht</a>

  </div>
</div>

<?php
echo layout_admin_footer();