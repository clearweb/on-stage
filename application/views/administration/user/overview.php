<?php
$this->load->helper('layout');
$this->load->library('grid');
echo layout_admin_header('users');
?>

<div id="main">
  <div id="main-content">

<?php

foreach($users as $key=>$user)
{
	$users[$key]['active'       ] = $user['active'       ] == 1? 'Ja' : 'Nee';
	$users[$key]['double_opt_in'] = $user['double_opt_in'] == 1? 'Ja' : 'Nee';
}

$this->grid->set_title('Gebruikers beheren');
$this->grid->add_rows($users);

$this->grid->filter_headers(array('id', 'password', 'forgot_password_key', 'double_opt_in', 'forgot_password_key_expire'));
$this->grid->set_header_names(array(
									'username' => 'Gebruikersnaam',
									'active' => 'Actief'
									)
							  );

$this->grid->add_control_button('/user/login_as/{id}', 'Inloggen als');

$this->grid->render();
?>
	
  </div>
</div>

<?php
echo layout_admin_footer();
