<?php
$this->load->helper('layout');
$this->load->library('grid');
echo layout_admin_header('videos');
?>

<div id="main">
  <div id="main-content">
<?php

foreach($videos as $key => $video)
{
	if($videos[$key]['type']  == 'free') {
		$videos[$key]['type'] = 'Trailer';
	} elseif($videos[$key]['type']  == 'paid') {
		$videos[$key]['type'] = 'Betaalde video';
	}
}

$this->grid->set_title('Video\'s beheren');
$this->grid->add_rows($videos);

$this->grid->filter_headers(array('id', 'performance', 'thumbnail_url', 'sd_url', 'hd_url'));
$this->grid->set_header_names(array(
									'video_id' => 'Video ID',
									'name' => 'Code',
									'performance_name' => 'Voorstelling',
									'type' => 'Type'
									)
							  );

$this->grid->add_operation('/video/add', 'Toevoegen');
$this->grid->add_control_button();
$this->grid->add_control_button('/video/delete/{id}', 'Verwijderen', array('confirmation-needed'));

$this->grid->render();
?>
  </div>
</div>

<?php
echo layout_admin_footer();
