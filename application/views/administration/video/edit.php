<?php
$this->load->helper('layout');
$this->load->library('table');

$video_html = '';

if($this->Video_model->loaded) {
	$video_html = $this->Video_model->get_video_html(550, 367);
}

$performance_options = array();
foreach($performances as $performance) {
	$performance_options[$performance['id']] = $performance['title'];
}

echo layout_admin_header('videos');
$this->load->helper('form');
?>

<div id="main">
  <div id="main-content">
	<h1>Video bewerken</h1>
<?php
echo form_open();

echo form_hidden('id', $this->Video_model->id);

$tmpl = array (
			   'table_open'  => '<table class="form-table">',
			   'heading_row_start' => '<tr>',
			   'heading_row_end' => '</tr>',
			   'heading_cell_start' => '<td>',
			   'heading_cell_end' => '</td>',
			   );
$this->table->set_template($tmpl);

?>

<div class="form-errors">
<?php
echo validation_errors();
?>
</div>

<?php
$data = array(
			  array( form_label('Video:', 'video'), $video_html.form_jquery_upload('video', '/video/blueimp')),
			  array( form_label('Type:', 'type'), form_dropdown('type', array('free'=>'Trailer', 'paid'=>'Betaalde video'), $this->Video_model->getProperty('type'))),
			  array( form_label('Voorstelling:', 'performance'), form_dropdown('performance', $performance_options, $this->Video_model->getProperty('performance')) ),
			  array('', form_submit('submit', 'Verzenden').'<a href="/video/overview" id="back-to-overview" class="call-to-action">terug naar overzicht</a>'),
			  );

echo $this->table->generate($data);

echo form_close();
	?>
  </div>
</div>

<?php
echo layout_admin_footer();