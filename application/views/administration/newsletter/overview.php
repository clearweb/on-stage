<?php
$this->load->library('grid');
$this->load->helper('layout');
echo layout_admin_header('newsletter');
?>

<div id="main">
  <div id="main-content">
  
<?php

$this->grid->set_title('Nieuwsbrief');

$this->grid->add_rows($newsletter);

$this->grid->filter_headers(array('id', 'userid', 'double_opt_in'));
$this->grid->set_header_names(array(
									'email' => 'E-mail',
									)
							  );
$this->grid->render();

?>

  </div>
</div>

<?php
echo layout_admin_footer();