<?php
function build_lettergrid($letters, $grid_name = '', $selected = '', $all_link_uri = '') 
{
?>
		 <div class="letter-selection">
		 <div class="letter-selection-grid">
		 <?php
		foreach($letters as $letter => $links_in_letter )
		{
			$classes = 'letter-selection-letter';
			$href = 'href="#'.$grid_name.'-content-'.$letter.'"';
			
			if (count($links_in_letter) == 0)
			{
				$classes .= ' empty';
				$href = '';
			}
			echo '<a '.$href.' class="'.$classes.'">'.strtoupper($letter).'</a>';
		}
		 ?>
		 </div>
		 <div class="letter-selection-content-container">
		 <?php
		foreach($letters as $letter => $links_in_letter ):
			$class = 'letter-selection-content';
			foreach($links_in_letter as $link)
			{
				if ($selected == $link['name'])
					$class .= ' open-initially';
			}
		?>
		 <div id="<?php echo $grid_name ?>-content-<?php echo $letter;?>" class="<?php echo $class;?>">
		 <a <?php echo (($selected)?'':' class="important" '); ?> href="<?php echo $all_link_uri; ?>">Alle</a>
		 <?php

			foreach($links_in_letter as $link)
			{
				$class = (isset($selected) && $selected == $link['name'])?' class="important" ':'';
				echo '<a '.$class.'href="'.$link['url'].'">'.$link['name'].'</a>';
			}
		 ?>
		 </div>
		 <?php
		endforeach;
		 ?>
	      </div>
	     </div>	
<?php
}
?>

	<div id="main-content-left">
	  <div id="main-content-left-accordion">
	    <h3><a class="accordion-choice" href="#performer">artiest</a></h3>
	    <div>
<?php
	build_lettergrid($performers, 'performer', $selected_performer, $all_performers_uri);
?>
	    </div>
	    <h3><a class="accordion-choice" href="#performance">voorstellingen</a></h3>
	    <div>
<?php
	build_lettergrid($performance, 'performance', $selected_performance, $all_performances_uri);
?>
	    </div>
	    <h3><a class="accordion-choice" href="#genre">genre</a></h3>
	    <div>
		 <a <?php echo (($selected_genre)?'':' class="important" ');?> href="<?php echo $all_genres_uri; ?>">Alle genres</a>

		 <?php foreach($genres as $genre): ?>
		 <a <?php if (isset($selected_genre) && $selected_genre == $genre['name']) echo ' class="important open-initially" ';?> href="<?php echo $genre['url']; ?>">
		 <?php echo $genre['name']; ?>
		 </a>
		 <?php endforeach; ?>
	    </div>
	  </div>
	  
	  
	  
	  
	  
	  <div class="left-component">
	    <h3 class="component-title">Contact ons</h3>
	    <p>
	      info@on-stage.tv
	    </p>
	  </div>
	  
	  <div class="left-component">
	    <h3 class="component-title">Nieuwsbrief</h3>
	    <?php echo form_open('/newsletter/add'); ?>
		<input id="newsletter" type="text" name="email" value="e-mail adres" onClick="select()"/>
		<input id="mail-submit" type="submit" value="" />
		<?php echo form_close();?>
	  </div>
	  
	  <div class="left-component">
	    <h3 class="component-title">social media</h3>
		<?php echo layout_facebook(true, '175', '300'); //faces,width, height, border-color?>
		<p>
<!--
	<a class="twitter-timeline" lang="NL" width="100" href="https://twitter.com/onstagetvtweet" data-widget-id="310849749865611265">Tweets van @onstagetvtweet</a>
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
--!>

	<script>
		function replaceURLWithHTMLLinks(text) {
			var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
			return text.replace(exp,"<a href='$1'>$1</a>"); 
		}

		$.getJSON("https://api.twitter.com/1/statuses/user_timeline/onstagetvtweet.json?count=5&include_rts=1&callback=?", function(data) {
				$.each(data, function(key, tweet) {
						tweet_p = $('<p>')
							.addClass('tweet')
							.html(replaceURLWithHTMLLinks(tweet.text));
						$('#twitter').append(tweet_p);
					})
			});
	</script>
	<! -- Start of Twitter Feed -->
	<p align="left">
		<div class="tweetbox" style="float:left;">
			<div class="twitterlink-container"><span><a href="https://twitter.com/onstagetvtweet" target="_blank" class="twitterlink"><img src="/images/twitter-bird-white.png"/>@onstagetvtweet</a></span></div>
			<p align="left">&#160</p>
			<div id="twitter"></div>
		</div>		
	</p>
	<! -- End of Twitter Feed -->

	    </p>
	  </div>

	</div>