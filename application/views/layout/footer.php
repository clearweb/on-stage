    <div id="footer">
      <div id="footer-wrapper">
      <div id="footer-col-container">
	<div class="footer-col">
	  <p class="footer-col-title">
	    Over ON-STAGE.TV
	  </p>
		 <?php
		 foreach($about as $footer_link):
		 ?>
		 <a href="/faq/questions/<?php echo $footer_link['faq_title']; ?>#<?php echo $footer_link['title']; ?>">
		 <?php echo $footer_link['title']; ?>
		 </a>
		 <?php
		 endforeach;
		 ?>
	</div>
	<div class="footer-col">
	  <p class="footer-col-title">
	    Kijken en betalen
	  </p>
		 <?php
		 foreach($watch_and_pay as $footer_link):
		 ?>
		 <a href="/faq/questions/<?php echo $footer_link['faq_title']; ?>#<?php echo $footer_link['title']; ?>">
		 <?php echo $footer_link['title']; ?>
		 </a>
		 <?php
		 endforeach;
		 ?>
	</div>
	<div class="footer-col">
	  <p class="footer-col-title">
	    Support
	  </p>
		 <?php
		 foreach($support as $footer_link):
		 ?>
		 <a href="/faq/questions/<?php echo $footer_link['faq_title']; ?>#<?php echo $footer_link['title']; ?>">
		 <?php echo $footer_link['title']; ?>
		 </a>
		 <?php
		 endforeach;
		 ?>
	</div>
	<div class="footer-col">
	  <p class="footer-col-title">
	    Overig
	  </p>
		 <?php
		 foreach($other as $footer_link):
		 ?>
		 <a href="/faq/questions/<?php echo $footer_link['faq_title']; ?>#<?php echo $footer_link['title']; ?>">
		 <?php echo $footer_link['title']; ?>
		 </a>
		 <?php
		 endforeach;
		 ?>
	</div>
      </div>
      <div id="footer-copyright">
	<p>
	  STENDHAL PRODUCTIONS, ALL RIGHTS RESERVED &copy; 2012 - SITE BY <a href="http://clearweb.nl">CLEARWEB</a> / <a href="http://think-brand.nl">THINK.BRAND</a>
	</p>
      </div>
      </div>
      <div id="underfooter"></div>
    </div>