<div id="search-container">
	<div id="search-wrapper">
<?php
$this->load->helper('form');
echo form_open('/performance/search', array('method'=>'get'));
echo form_hidden('controller', $controller);
echo form_hidden('action', $action);
echo form_hidden('first_param', $first_param);
echo form_hidden('assoc_params', json_encode($assoc_params));

if (empty($search_term))
{
	$search_value = 'zoek op voorstelling, artiest...';
	$class = '';
}
else
{
	$search_value = $search_term;
	$class = 'class="filled"';
}
echo form_input('search', $search_value, 'onfocus="if (this.value==\'zoek op voorstelling, artiest...\') this.value=\'\';" onblur="if (this.value==\'\') this.value=\'zoek op voorstelling, artiest...\'" onchange="$(this).removeClass(\'filled\')"'.$class );
echo form_submit('submit','');
echo form_close();
?>
	</div>
</div>