<?php
$this->load->helper('form');
$this->load->helper('user');
?>
<div id="fb-root"></div>
<script>
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
    <div id="top">
      <div id="top-wrap">
	<div id="logo-container">
	  <a href="/">
	    <img id="logo" src="/images/logo.png" />
	  </a>
	</div>
	<div id="top-right">
	  <div id="webuser-block">
  <?php
  if ( user_logged_in()):
?>
	  <a id="my-onstage" href="/user/account/">MIJN ON-STAGE</a>
		   |
	  <a id="logout" href="/user/logout/">LOGOUT</a>
<?php
  else:
?>
	    <a id="register" href="/user/register">REGISTREREN</a> | <a id="login-toggler">LOGIN</a>
	    <div id="login-container">
       <?php
       echo form_open('/user/login/');
       ?>
	      <div>Login</div>
	      <div>
			<input type="text" value="Email" name="email" onfocus="if (this.value=='Email') this.value= ''" onblur="if (this.value=='') this.value= 'Email'"/>
	      </div>
	      <div>
			<input id="password-field-fake" type="text" value="Wachtwoord"/>
			<input id="password-field" name="password" type="password" value=""/>
			  <p><a href="/user/reset_password/">wachtwoord vergeten?</a></p>
	      </div>
	      <div>
		<input type="submit" value="inloggen" />
	      </div>
       <?php
       echo form_close();
       ?>
	    </div>
<?php
	    endif;
?>
	    <img id="cart" src="/images/cart.png" />
	  </div>
	  <div id="menu">
	    <ul>
	      <li 
                  <?php if ($current_menu == 'home'):?>
                  class="current-menu-item"
                  <?php endif; ?>
              >
		<a href="/">HOME</a>
	      </li>
	      <li 
                  <?php if ($current_menu == 'performances'):?>
                  class="current-menu-item"
                  <?php endif; ?>
              >
	      <a href="/performance">Voorstellingen</a>
	      </li>
	      <li 
                  <?php if ($current_menu == 'how-it-works'):?>
                  class="current-menu-item"
                  <?php endif; ?>
              >
		<a href="/faq/questions/how-it-works">hoe werkt het</a>
	      </li>
	      <li 
                  <?php if ($current_menu == 'about'):?>
                  class="current-menu-item"
                  <?php endif; ?>
              >
		<a href="/faq/questions/about">over on-stage.tv</a>
	      </li>
	    </ul>
	  </div>
	</div>
      </div>
    </div>
   <!-- top -->