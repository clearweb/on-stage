<?php
$this->load->helper('layout');

echo layout_header('home', array());
?>

	<div class="wrapper">
	<?php echo layout_search(); ?>
		<h1 class="page-title">Nieuwsbrief</h1>
    </div>
    <div id="main">
      <div id="main-content">
	  	<div id="account-form-left">
	      <div class="account-form-block">
			<div class="info-text">
	        <p >
			Bedankt voor het aanmelden voor de nieuwsbrief.<br />
			U bent geregistreerd met<span class="email"> <?php echo $email; ?> </span>als e-mailadres.
			<br />
	        </p>
			</div>
	        <div class="account-form-block-bottom"></div>
	      </div>
	    </div>
		<div id="account-form-right">
		  <h3>Kies uw onderwerp</h3>
		  <div class="account-form-block">
		    <?php
		      echo layout_get_subjects();
		    ?>
		  </div>
		</div>
	  </div>
	</div>

<?php
echo layout_footer();