<?php
$this->load->helper('layout');

echo layout_header('home', array(), array('account_form'));
?>

	<div class="wrapper">
	<?php echo layout_search(); ?>
		<h1 class="page-title">Nieuwsbrief</h1>
    </div>
    <div id="main">
      <div id="main-content">
	  	<div id="account-form-left">
	      <div class="account-form-block">
		  <div class="info-text">
<?php
	echo form_open('/newsletter/add');
?>
	        <p >
			<?php echo validation_errors();?>
	        </p>
			</div>
			<div class="input-boxes">
	          <div>
	            <label for="input-email">UW EMAIL</label>
				<input id="input-email" name="email" type="text" value="" onfocus="if (this.value=='EMAIL')this.value=''" onblur="if (this.value=='')this.value='EMAIL'" />
	          </div>
	          <div class="submit-wrapper">
	            <input type="submit" class="call-to-action" value="Versturen" />
	          </div>
			</div>
	        <div class="account-form-block-bottom"></div>
<?php
	echo form_close();
?>
	      </div>
	    </div>
		<div id="account-form-right">
		  <h3>Kies uw onderwerp</h3>
		  <div class="account-form-block">
		    <?php
		      echo layout_get_subjects();
		    ?>
		  </div>
		</div>
	  </div>
	</div>

<?php
echo layout_footer();