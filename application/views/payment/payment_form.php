<?php
$this->load->helper('layout');
$this->load->helper('form');

echo layout_header('home', array(), array('account_form'));
?>

	<div class="wrapper">
		<?php echo layout_search(); ?>
		<h1 class="page-title">BETALEN EN BEKIJKEN</h1>
    </div>
    <div id="main">
      <div id="main-content">
	        <p>
		      Hieronder vindt u een overzicht van uw keuze. Kies dan de gewenste betalingsmethode en geniet direct van uw voorstelling!
	        </p>
<?php
echo form_open();
?>
	  	<div id="payment-left">
	      <div class="account-block">
	        <h3>UW KEUZE</h3>
	        <img width="200" height="120" src="<?php echo $this->Performance_model->getProperty('thumbnail_url');?>" />
	        <table id="payment-performance-attributes-table">
	          <tr>
	            <td class="payment-performance-attribute">Uw keuze</td>
	            <td><?php echo $this->Performance_model->getProperty('title'); ?></td>
	          </tr>
	          <tr>
	            <td class="payment-performance-attribute">Prijs</td>
	            <td class="price">&euro; <?php echo number_format($this->Performance_model->getProperty('rental_price'), 2, ',', '.'); ?></td>
	          </tr>
	          <tr>
	            <td class="payment-performance-attribute">Beschikbaar</td>
				<td class="hour"><?php echo $this->Performance_model->getProperty('rental_period'); ?> uur</td>
	          </tr>
	          <tr>
	            <td class="payment-performance-attribute">Korting</td>
	            <td>Geen</td>
	          </tr>
	        </table>
	      </div>
	    </div>
		<div id="payment-right">
		  <div class="account-block">
		    <h3>ONZE BETALINGEN GAAN VIA ICEPAY</h3>
<p class="payment">
ICEPAY maakt het als Payment Service Provider mogelijk voor webwinkeliers om diverse online betaalmethodes, zoals iDEAL en PayPal, aan te bieden in hun webshop. Zo kunt u als consument binnen de webshop van uw online betalingen binnen een gebruiksvriendelijke en veilige betaalomgeving verrichten. ICEPAY is alleen verantwoordelijk voor de afhandeling van uw betaling en heeft geen invloed op de levering van uw online aankopen.
</p>
<?php
//echo form_dropdown('payment_method', $payment_methods);
?>
<!--
		  </div>
		  <div class="account-block last">
-->
	        <?php
		      echo form_checkbox('policy', 'accept', set_checkbox('policy', 'accept'), 'id="input-policy"');
	        ?>
	        <label for="input-policy">Ja, ik ga akkoord met de <a target="_blank" href="/faq/questions/about#algemene voorwaarden">algemene voorwaarden</a>.</label>

	        <div class="submit-wrapper">
	          <input type="submit" class="call-to-action" value="GA VERDER">
	        </div>

		  </div>
		</div>
<?php
echo form_close();
?>
	  </div>
	</div>
<?php
echo layout_footer();