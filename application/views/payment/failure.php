<?php
$this->load->helper('layout');

echo layout_header('home');
?>

	<div class="wrapper">
		<?php echo layout_search(); ?>
		<h1 class="page-title">Betaling nog niet voltooid</h1>
    </div>
    <div id="main">
      <div id="main-content">
	  	<div id="account-form-left">
	      <div class="account-form-block">
	        <p>
		      Uw betaling is nog niet afgerond.
	        </p>
	        <p>
	          Klik hieronder om de betaling alsnog af te ronden.
	        </p>
	        <div class="action-wrapper">
	          <a class="call-to-action" href="/payment/hire_performance/<?php echo $performance_id; ?>" >Afronden</a>
	        </div>
	        <div class="account-form-block-bottom"></div>
	      </div>
	    </div>
		<div id="account-form-right">
		  <h3>Kies uw onderwerp</h3>
		  <div class="account-form-block">
		    <?php
		      echo layout_get_subjects();
		    ?>
		  </div>
		</div>
	  </div>
	</div>

<?php
echo layout_footer();