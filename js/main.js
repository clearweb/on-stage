function load_more_related_trailers() {
    page = divcount() + 2;
	container_id = 'related_content_container_' + page;
	add_related_content_container(container_id);
	$.get('/performance/load_related_videos/' + page, {}, function(data) {
		$('#' + container_id).html(data);
	});
}
	
function divcount() {
	var mycount =  $('#trailer-related-block div.test-class').length;
	return mycount;
}

function remove_related_content_button()
{
	$('a#load-more-related')
		.remove();
}

function add_related_content_container(id) {
	$('<div></div>')
		.attr('id', id)
		.html('')
		.insertBefore($('#trailer-related-block-bottom')
		.hide()
		.slideDown('fast'));
}

function open_question(id){
	$('#faq-accordion').accordion("option", "active", id );
}

$(document).ready(function() {
    $('body, body *').click(
	function(event) {
	    if ($(this).is('#login-toggler, #login-container, #login-container *') || $(this).is('')) {
		$('#login-container').show();
		$('#login-container').position( {
		    
		    my:"center top",
		    at: "center bottom",
		    
		    of: "#login-toggler",
		    offset: "0 10"
		});
		event.stopPropagation();
	    } else {
		$('#login-container').hide();
	    }
	}
    );
	
	$.get('/performance/check_for_more_content/2', {}, function(data) {
		if(data=="false"){
			$('a#load-more-related')
				.remove();
		}
	});
		
    $('#password-field-fake').focus( function () {
	$('#password-field-fake').hide();
	$('#password-field').show();
	$('#password-field').focus();
    })

    $('#password-field').blur( function () {
	if ( $('#password-field').val() == '') {
	    $('#password-field').hide();
	    $('#password-field-fake').show();
	}
    });

    if ($.fn.nivoSlider) {
	
	$('#slider').nivoSlider(
	    {
		effect: 'slideInRight',
		directionNav: false,
		manualAdvance: true
	    }
	);
	
	$('.small-slider').nivoSlider(
	    {
		effect: 'slideInRight',
		controlNav: false,
		directionNav: true,
		manualAdvance: true
	    }
	);

    }

    $('#menu li').hover( function() {
	$(this).toggleClass('active-menu-item');
    });
    
    
    $('#faq-accordion').accordion(
	{
	    autoHeight: false,
	    collapsible: true,
	    navigation: true
	}
    );
    
    if ($.fn.jcarousel) {
    
	$('.show-slider .jcarousel').jcarousel();

	$('#also-viewed-carousel .jcarousel').jcarousel(
	    {
		visible: 4,
		scroll: 4
	    }
	);
    }
    
    $('.jcarousel-item > a > img')
	.hover(
	    function() {
		$('#carousel-item-overlay').remove();
		$(this)
		    .after(
			$('<div></div>')
			    .attr('id', 'carousel-item-overlay')
			    .addClass('carousel-item-overlay')
			    .append(
				$('<p></p>').
				    html('bekijk')
			    )
			    .append(
				$('<img></img>').
				    attr('src', '/images/white_play.png')
			    )
			
			);
		$('#carousel-item-overlay').width($(this).width());
		$('#carousel-item-overlay').height($(this).height());
		$('#carousel-item-overlay').offset($(this).offset());
		
		$(this).closest('a').
		    hover(
			function(){},
			function() {
			    $('#carousel-item-overlay').remove()
			}
		    );
	    },
	    function(event) {
		if ($(event.relatedTarget).attr('id') != 'carousel-item-overlay' && $(event.relatedTarget).not('#carousel-item-overlay *')) {
		    $('#carousel-item-overlay').remove();
		}
	    }
	);


    if ($.fn.ColorPicker) {    
	$('.color-selector-example').css('color', $('input[name="title_color"]').val());
	
	$('.color-selector').ColorPicker(
	    {
		color: $('input[name="title_color"]').val(),
		onShow: function (colpkr) {
		    $(colpkr).fadeIn(500);
		    return false;
		},
		onHide: function (colpkr) {
		    $(colpkr).fadeOut(500);
		    return false;
		},
		onChange: function (hsb, hex, rgb) {
		    $('.color-selector div').css('backgroundColor', '#' + hex);
		    $('.color-selector-example').css('color', '#' + hex);
		    $('input[name="title_color"]').val('#' + hex);
		}
	    }
	);
    }

	if ($(document).find('.pass-change-box').length > 0) {
		$('body')
			.append(
			$('<div>')
				.html('<p>Uw wachtwoord is succesvol gewijzigd.</p>')
				.hide()
				.attr('id', 'password-change-dialog')
				.addClass('password-change-dialog')
				.attr('title', 'Wachtwoord gewijzigd')
			);
		
		$('#password-change-dialog')
			.dialog(
			{
				resizable: false,
				closeText: 'x',
				height:220,
				width:400,
				modal: true,
				close: function() {$( this ).remove()},
				buttons: {
					Akkoord: function() {
						$( this ).dialog( "close" );
					}
				}
			});
	}
	
	$('a.confirmation-needed').click(function(e) {
	e.preventDefault();
	href = $(this).attr('href');
	$('body')
	    .append(
		$('<div>')
		    .html('<p>U staat op het punt om een actie te ondernemen die u niet terug kan draaien.</p>')
		    .hide()
		    .attr('id', 'confirm-dialog')
		    .addClass('confirm-dialog')
		    .attr('title', 'Weet u dit zeker?')
	    );
	
	$('#confirm-dialog')
	    .dialog(
		{
		    resizable: false,
		    closeText: 'x',
		    height:220,
		    width:400,
		    modal: true,
		    close: function() {$( this ).remove()},
		    buttons: {
			Annuleren: function() {
			    $( this ).dialog( "close" );
			},
			Voltooien: function() {
			    $( this ).dialog( "close" );
			    window.location = href;
			}
		    }
		}
	    )
    });

    $('#performance-tabs').tabs();
    
    $('#main-content-left-accordion').accordion(
	{
	    autoHeight: false,
	    collapsible: true,	
	    activate: function( event, ui ) {$('html, body').animate({scrollTop:0}, 'slow')},
	    //navigation: true
	}
    ).ready(
	function ()
	{
	    $('#main-content-left-accordion h3').each(
		function(index, e)
		{
		    if($(e).next().has('.open-initially').length > 0 && index > 0)
		    {
			$( '#main-content-left-accordion' ).accordion( "option", "active", index );
		    }
		}
	    )
	}
    );


	
    
    $('.letter-selection-content:not(.open-initially)').hide();
    
    $('.letter-selection-letter:not(.empty)').click(
	function(e) {
	    $(this).closest('.letter-selection').find('.letter-selection-content-container > .letter-selection-content').hide();
	    $($(this).attr('href')).show();
	    e.preventDefault();
	}
    )
});