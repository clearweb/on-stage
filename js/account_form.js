function check_valid(element)
{
    name = $(element).attr('name');
    data = $(element).closest('form').serialize();
    action = $(element).closest('form').attr('action');
    $.post(action + '?ajax=true&name='+name, data, function(response){
	if (response == 'valid')
	{
	    update_validity(element, true);
	} else {
	    update_validity(element, false, response);
	}
    });
    
}

function update_validity(element, valid, error)
{
    if (valid) {
	if ( $('#validation-success-' + $(element).attr('name')).length <= 0 )
	{
	    $(element).after(
		$('<div></div>')
		    .addClass('success')
		    .attr('id', 'validation-success-' + $(element).attr('name'))
	    );
	}
	$('#validation-success-' + $(element).attr('name')).show();
	$('#validation-error-' + $(element).attr('name')).hide();
    }
    else
    {
	if ( $('#validation-error-' + $(element).attr('name')).length <= 0 )
	{
	    $(element).after(
		$('<div></div>')
		    .addClass('error')
		    .attr('id', 'validation-error-' + $(element).attr('name'))
	    );
	}
	$('#validation-error-' + $(element).attr('name')).html(error);
	$('#validation-success-' + $(element).attr('name')).hide();
	$('#validation-error-' + $(element).attr('name')).show();
    }
}

$(document).ready(
    function() {
	$('input[type="submit"].call-to-action').hover(
	    function() {$(this).addClass('hover')}, function() {$(this).removeClass('hover')}
	);

	$('#input-fake-password').focus( function () {
	    $('#input-fake-password').hide();
	    $('#input-password').show();
	    $('#input-password').focus();
	});

	$('#input-password').blur( function () {
	    if ( $('#input-password').val() == '') {
		$('#input-password').hide();
		$('#input-fake-password').show();
	    }
	});

	$('#input-fake-password2').focus( function () {
	    $('#input-fake-password2').hide();
	    $('#input-password2').show();
	    $('#input-password2').focus();
	});

	$('#input-password2').blur( function () {
	    if ( $('#input-password2').val() == '') {
		$('#input-password2').hide();
		$('#input-fake-password2').show();
	    }
	});

	$('.account-form-block input.validate').each(function(){check_valid(this);});
	
	$('.account-form-block input.validate').change(function(){check_valid(this);});
    }
    
)